== Arbitray base adder


== Tag least significant digit as addend
0 add:cmd | add:shift 0 add:mark add:cmd
1 add:cmd | add:shift 1 add:mark add:cmd
2 add:cmd | add:shift 2 add:mark add:cmd
3 add:cmd | add:shift 3 add:mark add:cmd
4 add:cmd | add:shift 4 add:mark add:cmd
5 add:cmd | add:shift 5 add:mark add:cmd
6 add:cmd | add:shift 6 add:mark add:cmd
7 add:cmd | add:shift 7 add:mark add:cmd
8 add:cmd | add:shift 8 add:mark add:cmd
9 add:cmd | add:shift 9 add:mark add:cmd
A add:cmd | add:shift A add:mark add:cmd
B add:cmd | add:shift B add:mark add:cmd
C add:cmd | add:shift C add:mark add:cmd
D add:cmd | add:shift D add:mark add:cmd
E add:cmd | add:shift E add:mark add:cmd
F add:cmd | add:shift F add:mark add:cmd
G add:cmd | add:shift G add:mark add:cmd
H add:cmd | add:shift H add:mark add:cmd
I add:cmd | add:shift I add:mark add:cmd
J add:cmd | add:shift J add:mark add:cmd
K add:cmd | add:shift K add:mark add:cmd
L add:cmd | add:shift L add:mark add:cmd
M add:cmd | add:shift M add:mark add:cmd
N add:cmd | add:shift N add:mark add:cmd
O add:cmd | add:shift O add:mark add:cmd
P add:cmd | add:shift P add:mark add:cmd
Q add:cmd | add:shift Q add:mark add:cmd
R add:cmd | add:shift R add:mark add:cmd
S add:cmd | add:shift S add:mark add:cmd
T add:cmd | add:shift T add:mark add:cmd
U add:cmd | add:shift U add:mark add:cmd
V add:cmd | add:shift V add:mark add:cmd
W add:cmd | add:shift W add:mark add:cmd
X add:cmd | add:shift X add:mark add:cmd
Y add:cmd | add:shift Y add:mark add:cmd
Z add:cmd | add:shift Z add:mark add:cmd

== Shift current addend towards first number
add:done add:shift 0 add:mark|add:shift 0 add:mark add:done
add:done add:shift 1 add:mark|add:shift 1 add:mark add:done
add:done add:shift 2 add:mark|add:shift 2 add:mark add:done
add:done add:shift 3 add:mark|add:shift 3 add:mark add:done
add:done add:shift 4 add:mark|add:shift 4 add:mark add:done
add:done add:shift 5 add:mark|add:shift 5 add:mark add:done
add:done add:shift 6 add:mark|add:shift 6 add:mark add:done
add:done add:shift 7 add:mark|add:shift 7 add:mark add:done
add:done add:shift 8 add:mark|add:shift 8 add:mark add:done
add:done add:shift 9 add:mark|add:shift 9 add:mark add:done
add:done add:shift A add:mark|add:shift A add:mark add:done
add:done add:shift B add:mark|add:shift B add:mark add:done
add:done add:shift C add:mark|add:shift C add:mark add:done
add:done add:shift D add:mark|add:shift D add:mark add:done
add:done add:shift E add:mark|add:shift E add:mark add:done
add:done add:shift F add:mark|add:shift F add:mark add:done
add:done add:shift G add:mark|add:shift G add:mark add:done
add:done add:shift H add:mark|add:shift H add:mark add:done
add:done add:shift I add:mark|add:shift I add:mark add:done
add:done add:shift J add:mark|add:shift J add:mark add:done
add:done add:shift K add:mark|add:shift K add:mark add:done
add:done add:shift L add:mark|add:shift L add:mark add:done
add:done add:shift M add:mark|add:shift M add:mark add:done
add:done add:shift N add:mark|add:shift N add:mark add:done
add:done add:shift O add:mark|add:shift O add:mark add:done
add:done add:shift P add:mark|add:shift P add:mark add:done
add:done add:shift Q add:mark|add:shift Q add:mark add:done
add:done add:shift R add:mark|add:shift R add:mark add:done
add:done add:shift S add:mark|add:shift S add:mark add:done
add:done add:shift T add:mark|add:shift T add:mark add:done
add:done add:shift U add:mark|add:shift U add:mark add:done
add:done add:shift V add:mark|add:shift V add:mark add:done
add:done add:shift W add:mark|add:shift W add:mark add:done
add:done add:shift X add:mark|add:shift X add:mark add:done
add:done add:shift Y add:mark|add:shift Y add:mark add:done
add:done add:shift Z add:mark|add:shift Z add:mark add:done

0 add:shift 0 add:mark|add:shift 0 add:mark 0
1 add:shift 0 add:mark|add:shift 0 add:mark 1
2 add:shift 0 add:mark|add:shift 0 add:mark 2
3 add:shift 0 add:mark|add:shift 0 add:mark 3
4 add:shift 0 add:mark|add:shift 0 add:mark 4
5 add:shift 0 add:mark|add:shift 0 add:mark 5
6 add:shift 0 add:mark|add:shift 0 add:mark 6
7 add:shift 0 add:mark|add:shift 0 add:mark 7
8 add:shift 0 add:mark|add:shift 0 add:mark 8
9 add:shift 0 add:mark|add:shift 0 add:mark 9
A add:shift 0 add:mark|add:shift 0 add:mark A
B add:shift 0 add:mark|add:shift 0 add:mark B
C add:shift 0 add:mark|add:shift 0 add:mark C
D add:shift 0 add:mark|add:shift 0 add:mark D
E add:shift 0 add:mark|add:shift 0 add:mark E
F add:shift 0 add:mark|add:shift 0 add:mark F
G add:shift 0 add:mark|add:shift 0 add:mark G
H add:shift 0 add:mark|add:shift 0 add:mark H
I add:shift 0 add:mark|add:shift 0 add:mark I
J add:shift 0 add:mark|add:shift 0 add:mark J
K add:shift 0 add:mark|add:shift 0 add:mark K
L add:shift 0 add:mark|add:shift 0 add:mark L
M add:shift 0 add:mark|add:shift 0 add:mark M
N add:shift 0 add:mark|add:shift 0 add:mark N
O add:shift 0 add:mark|add:shift 0 add:mark O
P add:shift 0 add:mark|add:shift 0 add:mark P
Q add:shift 0 add:mark|add:shift 0 add:mark Q
R add:shift 0 add:mark|add:shift 0 add:mark R
S add:shift 0 add:mark|add:shift 0 add:mark S
T add:shift 0 add:mark|add:shift 0 add:mark T
U add:shift 0 add:mark|add:shift 0 add:mark U
V add:shift 0 add:mark|add:shift 0 add:mark V
W add:shift 0 add:mark|add:shift 0 add:mark W
X add:shift 0 add:mark|add:shift 0 add:mark X
Y add:shift 0 add:mark|add:shift 0 add:mark Y
Z add:shift 0 add:mark|add:shift 0 add:mark Z

0 add:shift 1 add:mark|add:shift 1 add:mark 0
1 add:shift 1 add:mark|add:shift 1 add:mark 1
2 add:shift 1 add:mark|add:shift 1 add:mark 2
3 add:shift 1 add:mark|add:shift 1 add:mark 3
4 add:shift 1 add:mark|add:shift 1 add:mark 4
5 add:shift 1 add:mark|add:shift 1 add:mark 5
6 add:shift 1 add:mark|add:shift 1 add:mark 6
7 add:shift 1 add:mark|add:shift 1 add:mark 7
8 add:shift 1 add:mark|add:shift 1 add:mark 8
9 add:shift 1 add:mark|add:shift 1 add:mark 9
A add:shift 1 add:mark|add:shift 1 add:mark A
B add:shift 1 add:mark|add:shift 1 add:mark B
C add:shift 1 add:mark|add:shift 1 add:mark C
D add:shift 1 add:mark|add:shift 1 add:mark D
E add:shift 1 add:mark|add:shift 1 add:mark E
F add:shift 1 add:mark|add:shift 1 add:mark F
G add:shift 1 add:mark|add:shift 1 add:mark G
H add:shift 1 add:mark|add:shift 1 add:mark H
I add:shift 1 add:mark|add:shift 1 add:mark I
J add:shift 1 add:mark|add:shift 1 add:mark J
K add:shift 1 add:mark|add:shift 1 add:mark K
L add:shift 1 add:mark|add:shift 1 add:mark L
M add:shift 1 add:mark|add:shift 1 add:mark M
N add:shift 1 add:mark|add:shift 1 add:mark N
O add:shift 1 add:mark|add:shift 1 add:mark O
P add:shift 1 add:mark|add:shift 1 add:mark P
Q add:shift 1 add:mark|add:shift 1 add:mark Q
R add:shift 1 add:mark|add:shift 1 add:mark R
S add:shift 1 add:mark|add:shift 1 add:mark S
T add:shift 1 add:mark|add:shift 1 add:mark T
U add:shift 1 add:mark|add:shift 1 add:mark U
V add:shift 1 add:mark|add:shift 1 add:mark V
W add:shift 1 add:mark|add:shift 1 add:mark W
X add:shift 1 add:mark|add:shift 1 add:mark X
Y add:shift 1 add:mark|add:shift 1 add:mark Y
Z add:shift 1 add:mark|add:shift 1 add:mark Z

0 add:shift 2 add:mark|add:shift 2 add:mark 0
1 add:shift 2 add:mark|add:shift 2 add:mark 1
2 add:shift 2 add:mark|add:shift 2 add:mark 2
3 add:shift 2 add:mark|add:shift 2 add:mark 3
4 add:shift 2 add:mark|add:shift 2 add:mark 4
5 add:shift 2 add:mark|add:shift 2 add:mark 5
6 add:shift 2 add:mark|add:shift 2 add:mark 6
7 add:shift 2 add:mark|add:shift 2 add:mark 7
8 add:shift 2 add:mark|add:shift 2 add:mark 8
9 add:shift 2 add:mark|add:shift 2 add:mark 9
A add:shift 2 add:mark|add:shift 2 add:mark A
B add:shift 2 add:mark|add:shift 2 add:mark B
C add:shift 2 add:mark|add:shift 2 add:mark C
D add:shift 2 add:mark|add:shift 2 add:mark D
E add:shift 2 add:mark|add:shift 2 add:mark E
F add:shift 2 add:mark|add:shift 2 add:mark F
G add:shift 2 add:mark|add:shift 2 add:mark G
H add:shift 2 add:mark|add:shift 2 add:mark H
I add:shift 2 add:mark|add:shift 2 add:mark I
J add:shift 2 add:mark|add:shift 2 add:mark J
K add:shift 2 add:mark|add:shift 2 add:mark K
L add:shift 2 add:mark|add:shift 2 add:mark L
M add:shift 2 add:mark|add:shift 2 add:mark M
N add:shift 2 add:mark|add:shift 2 add:mark N
O add:shift 2 add:mark|add:shift 2 add:mark O
P add:shift 2 add:mark|add:shift 2 add:mark P
Q add:shift 2 add:mark|add:shift 2 add:mark Q
R add:shift 2 add:mark|add:shift 2 add:mark R
S add:shift 2 add:mark|add:shift 2 add:mark S
T add:shift 2 add:mark|add:shift 2 add:mark T
U add:shift 2 add:mark|add:shift 2 add:mark U
V add:shift 2 add:mark|add:shift 2 add:mark V
W add:shift 2 add:mark|add:shift 2 add:mark W
X add:shift 2 add:mark|add:shift 2 add:mark X
Y add:shift 2 add:mark|add:shift 2 add:mark Y
Z add:shift 2 add:mark|add:shift 2 add:mark Z

0 add:shift 3 add:mark|add:shift 3 add:mark 0
1 add:shift 3 add:mark|add:shift 3 add:mark 1
2 add:shift 3 add:mark|add:shift 3 add:mark 2
3 add:shift 3 add:mark|add:shift 3 add:mark 3
4 add:shift 3 add:mark|add:shift 3 add:mark 4
5 add:shift 3 add:mark|add:shift 3 add:mark 5
6 add:shift 3 add:mark|add:shift 3 add:mark 6
7 add:shift 3 add:mark|add:shift 3 add:mark 7
8 add:shift 3 add:mark|add:shift 3 add:mark 8
9 add:shift 3 add:mark|add:shift 3 add:mark 9
A add:shift 3 add:mark|add:shift 3 add:mark A
B add:shift 3 add:mark|add:shift 3 add:mark B
C add:shift 3 add:mark|add:shift 3 add:mark C
D add:shift 3 add:mark|add:shift 3 add:mark D
E add:shift 3 add:mark|add:shift 3 add:mark E
F add:shift 3 add:mark|add:shift 3 add:mark F
G add:shift 3 add:mark|add:shift 3 add:mark G
H add:shift 3 add:mark|add:shift 3 add:mark H
I add:shift 3 add:mark|add:shift 3 add:mark I
J add:shift 3 add:mark|add:shift 3 add:mark J
K add:shift 3 add:mark|add:shift 3 add:mark K
L add:shift 3 add:mark|add:shift 3 add:mark L
M add:shift 3 add:mark|add:shift 3 add:mark M
N add:shift 3 add:mark|add:shift 3 add:mark N
O add:shift 3 add:mark|add:shift 3 add:mark O
P add:shift 3 add:mark|add:shift 3 add:mark P
Q add:shift 3 add:mark|add:shift 3 add:mark Q
R add:shift 3 add:mark|add:shift 3 add:mark R
S add:shift 3 add:mark|add:shift 3 add:mark S
T add:shift 3 add:mark|add:shift 3 add:mark T
U add:shift 3 add:mark|add:shift 3 add:mark U
V add:shift 3 add:mark|add:shift 3 add:mark V
W add:shift 3 add:mark|add:shift 3 add:mark W
X add:shift 3 add:mark|add:shift 3 add:mark X
Y add:shift 3 add:mark|add:shift 3 add:mark Y
Z add:shift 3 add:mark|add:shift 3 add:mark Z

0 add:shift 4 add:mark|add:shift 4 add:mark 0
1 add:shift 4 add:mark|add:shift 4 add:mark 1
2 add:shift 4 add:mark|add:shift 4 add:mark 2
3 add:shift 4 add:mark|add:shift 4 add:mark 3
4 add:shift 4 add:mark|add:shift 4 add:mark 4
5 add:shift 4 add:mark|add:shift 4 add:mark 5
6 add:shift 4 add:mark|add:shift 4 add:mark 6
7 add:shift 4 add:mark|add:shift 4 add:mark 7
8 add:shift 4 add:mark|add:shift 4 add:mark 8
9 add:shift 4 add:mark|add:shift 4 add:mark 9
A add:shift 4 add:mark|add:shift 4 add:mark A
B add:shift 4 add:mark|add:shift 4 add:mark B
C add:shift 4 add:mark|add:shift 4 add:mark C
D add:shift 4 add:mark|add:shift 4 add:mark D
E add:shift 4 add:mark|add:shift 4 add:mark E
F add:shift 4 add:mark|add:shift 4 add:mark F
G add:shift 4 add:mark|add:shift 4 add:mark G
H add:shift 4 add:mark|add:shift 4 add:mark H
I add:shift 4 add:mark|add:shift 4 add:mark I
J add:shift 4 add:mark|add:shift 4 add:mark J
K add:shift 4 add:mark|add:shift 4 add:mark K
L add:shift 4 add:mark|add:shift 4 add:mark L
M add:shift 4 add:mark|add:shift 4 add:mark M
N add:shift 4 add:mark|add:shift 4 add:mark N
O add:shift 4 add:mark|add:shift 4 add:mark O
P add:shift 4 add:mark|add:shift 4 add:mark P
Q add:shift 4 add:mark|add:shift 4 add:mark Q
R add:shift 4 add:mark|add:shift 4 add:mark R
S add:shift 4 add:mark|add:shift 4 add:mark S
T add:shift 4 add:mark|add:shift 4 add:mark T
U add:shift 4 add:mark|add:shift 4 add:mark U
V add:shift 4 add:mark|add:shift 4 add:mark V
W add:shift 4 add:mark|add:shift 4 add:mark W
X add:shift 4 add:mark|add:shift 4 add:mark X
Y add:shift 4 add:mark|add:shift 4 add:mark Y
Z add:shift 4 add:mark|add:shift 4 add:mark Z

0 add:shift 5 add:mark|add:shift 5 add:mark 0
1 add:shift 5 add:mark|add:shift 5 add:mark 1
2 add:shift 5 add:mark|add:shift 5 add:mark 2
3 add:shift 5 add:mark|add:shift 5 add:mark 3
4 add:shift 5 add:mark|add:shift 5 add:mark 4
5 add:shift 5 add:mark|add:shift 5 add:mark 5
6 add:shift 5 add:mark|add:shift 5 add:mark 6
7 add:shift 5 add:mark|add:shift 5 add:mark 7
8 add:shift 5 add:mark|add:shift 5 add:mark 8
9 add:shift 5 add:mark|add:shift 5 add:mark 9
A add:shift 5 add:mark|add:shift 5 add:mark A
B add:shift 5 add:mark|add:shift 5 add:mark B
C add:shift 5 add:mark|add:shift 5 add:mark C
D add:shift 5 add:mark|add:shift 5 add:mark D
E add:shift 5 add:mark|add:shift 5 add:mark E
F add:shift 5 add:mark|add:shift 5 add:mark F
G add:shift 5 add:mark|add:shift 5 add:mark G
H add:shift 5 add:mark|add:shift 5 add:mark H
I add:shift 5 add:mark|add:shift 5 add:mark I
J add:shift 5 add:mark|add:shift 5 add:mark J
K add:shift 5 add:mark|add:shift 5 add:mark K
L add:shift 5 add:mark|add:shift 5 add:mark L
M add:shift 5 add:mark|add:shift 5 add:mark M
N add:shift 5 add:mark|add:shift 5 add:mark N
O add:shift 5 add:mark|add:shift 5 add:mark O
P add:shift 5 add:mark|add:shift 5 add:mark P
Q add:shift 5 add:mark|add:shift 5 add:mark Q
R add:shift 5 add:mark|add:shift 5 add:mark R
S add:shift 5 add:mark|add:shift 5 add:mark S
T add:shift 5 add:mark|add:shift 5 add:mark T
U add:shift 5 add:mark|add:shift 5 add:mark U
V add:shift 5 add:mark|add:shift 5 add:mark V
W add:shift 5 add:mark|add:shift 5 add:mark W
X add:shift 5 add:mark|add:shift 5 add:mark X
Y add:shift 5 add:mark|add:shift 5 add:mark Y
Z add:shift 5 add:mark|add:shift 5 add:mark Z

0 add:shift 6 add:mark|add:shift 6 add:mark 0
1 add:shift 6 add:mark|add:shift 6 add:mark 1
2 add:shift 6 add:mark|add:shift 6 add:mark 2
3 add:shift 6 add:mark|add:shift 6 add:mark 3
4 add:shift 6 add:mark|add:shift 6 add:mark 4
5 add:shift 6 add:mark|add:shift 6 add:mark 5
6 add:shift 6 add:mark|add:shift 6 add:mark 6
7 add:shift 6 add:mark|add:shift 6 add:mark 7
8 add:shift 6 add:mark|add:shift 6 add:mark 8
9 add:shift 6 add:mark|add:shift 6 add:mark 9
A add:shift 6 add:mark|add:shift 6 add:mark A
B add:shift 6 add:mark|add:shift 6 add:mark B
C add:shift 6 add:mark|add:shift 6 add:mark C
D add:shift 6 add:mark|add:shift 6 add:mark D
E add:shift 6 add:mark|add:shift 6 add:mark E
F add:shift 6 add:mark|add:shift 6 add:mark F
G add:shift 6 add:mark|add:shift 6 add:mark G
H add:shift 6 add:mark|add:shift 6 add:mark H
I add:shift 6 add:mark|add:shift 6 add:mark I
J add:shift 6 add:mark|add:shift 6 add:mark J
K add:shift 6 add:mark|add:shift 6 add:mark K
L add:shift 6 add:mark|add:shift 6 add:mark L
M add:shift 6 add:mark|add:shift 6 add:mark M
N add:shift 6 add:mark|add:shift 6 add:mark N
O add:shift 6 add:mark|add:shift 6 add:mark O
P add:shift 6 add:mark|add:shift 6 add:mark P
Q add:shift 6 add:mark|add:shift 6 add:mark Q
R add:shift 6 add:mark|add:shift 6 add:mark R
S add:shift 6 add:mark|add:shift 6 add:mark S
T add:shift 6 add:mark|add:shift 6 add:mark T
U add:shift 6 add:mark|add:shift 6 add:mark U
V add:shift 6 add:mark|add:shift 6 add:mark V
W add:shift 6 add:mark|add:shift 6 add:mark W
X add:shift 6 add:mark|add:shift 6 add:mark X
Y add:shift 6 add:mark|add:shift 6 add:mark Y
Z add:shift 6 add:mark|add:shift 6 add:mark Z

0 add:shift 7 add:mark|add:shift 7 add:mark 0
1 add:shift 7 add:mark|add:shift 7 add:mark 1
2 add:shift 7 add:mark|add:shift 7 add:mark 2
3 add:shift 7 add:mark|add:shift 7 add:mark 3
4 add:shift 7 add:mark|add:shift 7 add:mark 4
5 add:shift 7 add:mark|add:shift 7 add:mark 5
6 add:shift 7 add:mark|add:shift 7 add:mark 6
7 add:shift 7 add:mark|add:shift 7 add:mark 7
8 add:shift 7 add:mark|add:shift 7 add:mark 8
9 add:shift 7 add:mark|add:shift 7 add:mark 9
A add:shift 7 add:mark|add:shift 7 add:mark A
B add:shift 7 add:mark|add:shift 7 add:mark B
C add:shift 7 add:mark|add:shift 7 add:mark C
D add:shift 7 add:mark|add:shift 7 add:mark D
E add:shift 7 add:mark|add:shift 7 add:mark E
F add:shift 7 add:mark|add:shift 7 add:mark F
G add:shift 7 add:mark|add:shift 7 add:mark G
H add:shift 7 add:mark|add:shift 7 add:mark H
I add:shift 7 add:mark|add:shift 7 add:mark I
J add:shift 7 add:mark|add:shift 7 add:mark J
K add:shift 7 add:mark|add:shift 7 add:mark K
L add:shift 7 add:mark|add:shift 7 add:mark L
M add:shift 7 add:mark|add:shift 7 add:mark M
N add:shift 7 add:mark|add:shift 7 add:mark N
O add:shift 7 add:mark|add:shift 7 add:mark O
P add:shift 7 add:mark|add:shift 7 add:mark P
Q add:shift 7 add:mark|add:shift 7 add:mark Q
R add:shift 7 add:mark|add:shift 7 add:mark R
S add:shift 7 add:mark|add:shift 7 add:mark S
T add:shift 7 add:mark|add:shift 7 add:mark T
U add:shift 7 add:mark|add:shift 7 add:mark U
V add:shift 7 add:mark|add:shift 7 add:mark V
W add:shift 7 add:mark|add:shift 7 add:mark W
X add:shift 7 add:mark|add:shift 7 add:mark X
Y add:shift 7 add:mark|add:shift 7 add:mark Y
Z add:shift 7 add:mark|add:shift 7 add:mark Z

0 add:shift 8 add:mark|add:shift 8 add:mark 0
1 add:shift 8 add:mark|add:shift 8 add:mark 1
2 add:shift 8 add:mark|add:shift 8 add:mark 2
3 add:shift 8 add:mark|add:shift 8 add:mark 3
4 add:shift 8 add:mark|add:shift 8 add:mark 4
5 add:shift 8 add:mark|add:shift 8 add:mark 5
6 add:shift 8 add:mark|add:shift 8 add:mark 6
7 add:shift 8 add:mark|add:shift 8 add:mark 7
8 add:shift 8 add:mark|add:shift 8 add:mark 8
9 add:shift 8 add:mark|add:shift 8 add:mark 9
A add:shift 8 add:mark|add:shift 8 add:mark A
B add:shift 8 add:mark|add:shift 8 add:mark B
C add:shift 8 add:mark|add:shift 8 add:mark C
D add:shift 8 add:mark|add:shift 8 add:mark D
E add:shift 8 add:mark|add:shift 8 add:mark E
F add:shift 8 add:mark|add:shift 8 add:mark F
G add:shift 8 add:mark|add:shift 8 add:mark G
H add:shift 8 add:mark|add:shift 8 add:mark H
I add:shift 8 add:mark|add:shift 8 add:mark I
J add:shift 8 add:mark|add:shift 8 add:mark J
K add:shift 8 add:mark|add:shift 8 add:mark K
L add:shift 8 add:mark|add:shift 8 add:mark L
M add:shift 8 add:mark|add:shift 8 add:mark M
N add:shift 8 add:mark|add:shift 8 add:mark N
O add:shift 8 add:mark|add:shift 8 add:mark O
P add:shift 8 add:mark|add:shift 8 add:mark P
Q add:shift 8 add:mark|add:shift 8 add:mark Q
R add:shift 8 add:mark|add:shift 8 add:mark R
S add:shift 8 add:mark|add:shift 8 add:mark S
T add:shift 8 add:mark|add:shift 8 add:mark T
U add:shift 8 add:mark|add:shift 8 add:mark U
V add:shift 8 add:mark|add:shift 8 add:mark V
W add:shift 8 add:mark|add:shift 8 add:mark W
X add:shift 8 add:mark|add:shift 8 add:mark X
Y add:shift 8 add:mark|add:shift 8 add:mark Y
Z add:shift 8 add:mark|add:shift 8 add:mark Z

0 add:shift 9 add:mark|add:shift 9 add:mark 0
1 add:shift 9 add:mark|add:shift 9 add:mark 1
2 add:shift 9 add:mark|add:shift 9 add:mark 2
3 add:shift 9 add:mark|add:shift 9 add:mark 3
4 add:shift 9 add:mark|add:shift 9 add:mark 4
5 add:shift 9 add:mark|add:shift 9 add:mark 5
6 add:shift 9 add:mark|add:shift 9 add:mark 6
7 add:shift 9 add:mark|add:shift 9 add:mark 7
8 add:shift 9 add:mark|add:shift 9 add:mark 8
9 add:shift 9 add:mark|add:shift 9 add:mark 9
A add:shift 9 add:mark|add:shift 9 add:mark A
B add:shift 9 add:mark|add:shift 9 add:mark B
C add:shift 9 add:mark|add:shift 9 add:mark C
D add:shift 9 add:mark|add:shift 9 add:mark D
E add:shift 9 add:mark|add:shift 9 add:mark E
F add:shift 9 add:mark|add:shift 9 add:mark F
G add:shift 9 add:mark|add:shift 9 add:mark G
H add:shift 9 add:mark|add:shift 9 add:mark H
I add:shift 9 add:mark|add:shift 9 add:mark I
J add:shift 9 add:mark|add:shift 9 add:mark J
K add:shift 9 add:mark|add:shift 9 add:mark K
L add:shift 9 add:mark|add:shift 9 add:mark L
M add:shift 9 add:mark|add:shift 9 add:mark M
N add:shift 9 add:mark|add:shift 9 add:mark N
O add:shift 9 add:mark|add:shift 9 add:mark O
P add:shift 9 add:mark|add:shift 9 add:mark P
Q add:shift 9 add:mark|add:shift 9 add:mark Q
R add:shift 9 add:mark|add:shift 9 add:mark R
S add:shift 9 add:mark|add:shift 9 add:mark S
T add:shift 9 add:mark|add:shift 9 add:mark T
U add:shift 9 add:mark|add:shift 9 add:mark U
V add:shift 9 add:mark|add:shift 9 add:mark V
W add:shift 9 add:mark|add:shift 9 add:mark W
X add:shift 9 add:mark|add:shift 9 add:mark X
Y add:shift 9 add:mark|add:shift 9 add:mark Y
Z add:shift 9 add:mark|add:shift 9 add:mark Z

0 add:shift A add:mark|add:shift A add:mark 0
1 add:shift A add:mark|add:shift A add:mark 1
2 add:shift A add:mark|add:shift A add:mark 2
3 add:shift A add:mark|add:shift A add:mark 3
4 add:shift A add:mark|add:shift A add:mark 4
5 add:shift A add:mark|add:shift A add:mark 5
6 add:shift A add:mark|add:shift A add:mark 6
7 add:shift A add:mark|add:shift A add:mark 7
8 add:shift A add:mark|add:shift A add:mark 8
9 add:shift A add:mark|add:shift A add:mark 9
A add:shift A add:mark|add:shift A add:mark A
B add:shift A add:mark|add:shift A add:mark B
C add:shift A add:mark|add:shift A add:mark C
D add:shift A add:mark|add:shift A add:mark D
E add:shift A add:mark|add:shift A add:mark E
F add:shift A add:mark|add:shift A add:mark F
G add:shift A add:mark|add:shift A add:mark G
H add:shift A add:mark|add:shift A add:mark H
I add:shift A add:mark|add:shift A add:mark I
J add:shift A add:mark|add:shift A add:mark J
K add:shift A add:mark|add:shift A add:mark K
L add:shift A add:mark|add:shift A add:mark L
M add:shift A add:mark|add:shift A add:mark M
N add:shift A add:mark|add:shift A add:mark N
O add:shift A add:mark|add:shift A add:mark O
P add:shift A add:mark|add:shift A add:mark P
Q add:shift A add:mark|add:shift A add:mark Q
R add:shift A add:mark|add:shift A add:mark R
S add:shift A add:mark|add:shift A add:mark S
T add:shift A add:mark|add:shift A add:mark T
U add:shift A add:mark|add:shift A add:mark U
V add:shift A add:mark|add:shift A add:mark V
W add:shift A add:mark|add:shift A add:mark W
X add:shift A add:mark|add:shift A add:mark X
Y add:shift A add:mark|add:shift A add:mark Y
Z add:shift A add:mark|add:shift A add:mark Z

0 add:shift B add:mark|add:shift B add:mark 0
1 add:shift B add:mark|add:shift B add:mark 1
2 add:shift B add:mark|add:shift B add:mark 2
3 add:shift B add:mark|add:shift B add:mark 3
4 add:shift B add:mark|add:shift B add:mark 4
5 add:shift B add:mark|add:shift B add:mark 5
6 add:shift B add:mark|add:shift B add:mark 6
7 add:shift B add:mark|add:shift B add:mark 7
8 add:shift B add:mark|add:shift B add:mark 8
9 add:shift B add:mark|add:shift B add:mark 9
A add:shift B add:mark|add:shift B add:mark A
B add:shift B add:mark|add:shift B add:mark B
C add:shift B add:mark|add:shift B add:mark C
D add:shift B add:mark|add:shift B add:mark D
E add:shift B add:mark|add:shift B add:mark E
F add:shift B add:mark|add:shift B add:mark F
G add:shift B add:mark|add:shift B add:mark G
H add:shift B add:mark|add:shift B add:mark H
I add:shift B add:mark|add:shift B add:mark I
J add:shift B add:mark|add:shift B add:mark J
K add:shift B add:mark|add:shift B add:mark K
L add:shift B add:mark|add:shift B add:mark L
M add:shift B add:mark|add:shift B add:mark M
N add:shift B add:mark|add:shift B add:mark N
O add:shift B add:mark|add:shift B add:mark O
P add:shift B add:mark|add:shift B add:mark P
Q add:shift B add:mark|add:shift B add:mark Q
R add:shift B add:mark|add:shift B add:mark R
S add:shift B add:mark|add:shift B add:mark S
T add:shift B add:mark|add:shift B add:mark T
U add:shift B add:mark|add:shift B add:mark U
V add:shift B add:mark|add:shift B add:mark V
W add:shift B add:mark|add:shift B add:mark W
X add:shift B add:mark|add:shift B add:mark X
Y add:shift B add:mark|add:shift B add:mark Y
Z add:shift B add:mark|add:shift B add:mark Z

0 add:shift C add:mark|add:shift C add:mark 0
1 add:shift C add:mark|add:shift C add:mark 1
2 add:shift C add:mark|add:shift C add:mark 2
3 add:shift C add:mark|add:shift C add:mark 3
4 add:shift C add:mark|add:shift C add:mark 4
5 add:shift C add:mark|add:shift C add:mark 5
6 add:shift C add:mark|add:shift C add:mark 6
7 add:shift C add:mark|add:shift C add:mark 7
8 add:shift C add:mark|add:shift C add:mark 8
9 add:shift C add:mark|add:shift C add:mark 9
A add:shift C add:mark|add:shift C add:mark A
B add:shift C add:mark|add:shift C add:mark B
C add:shift C add:mark|add:shift C add:mark C
D add:shift C add:mark|add:shift C add:mark D
E add:shift C add:mark|add:shift C add:mark E
F add:shift C add:mark|add:shift C add:mark F
G add:shift C add:mark|add:shift C add:mark G
H add:shift C add:mark|add:shift C add:mark H
I add:shift C add:mark|add:shift C add:mark I
J add:shift C add:mark|add:shift C add:mark J
K add:shift C add:mark|add:shift C add:mark K
L add:shift C add:mark|add:shift C add:mark L
M add:shift C add:mark|add:shift C add:mark M
N add:shift C add:mark|add:shift C add:mark N
O add:shift C add:mark|add:shift C add:mark O
P add:shift C add:mark|add:shift C add:mark P
Q add:shift C add:mark|add:shift C add:mark Q
R add:shift C add:mark|add:shift C add:mark R
S add:shift C add:mark|add:shift C add:mark S
T add:shift C add:mark|add:shift C add:mark T
U add:shift C add:mark|add:shift C add:mark U
V add:shift C add:mark|add:shift C add:mark V
W add:shift C add:mark|add:shift C add:mark W
X add:shift C add:mark|add:shift C add:mark X
Y add:shift C add:mark|add:shift C add:mark Y
Z add:shift C add:mark|add:shift C add:mark Z

0 add:shift D add:mark|add:shift D add:mark 0
1 add:shift D add:mark|add:shift D add:mark 1
2 add:shift D add:mark|add:shift D add:mark 2
3 add:shift D add:mark|add:shift D add:mark 3
4 add:shift D add:mark|add:shift D add:mark 4
5 add:shift D add:mark|add:shift D add:mark 5
6 add:shift D add:mark|add:shift D add:mark 6
7 add:shift D add:mark|add:shift D add:mark 7
8 add:shift D add:mark|add:shift D add:mark 8
9 add:shift D add:mark|add:shift D add:mark 9
A add:shift D add:mark|add:shift D add:mark A
B add:shift D add:mark|add:shift D add:mark B
C add:shift D add:mark|add:shift D add:mark C
D add:shift D add:mark|add:shift D add:mark D
E add:shift D add:mark|add:shift D add:mark E
F add:shift D add:mark|add:shift D add:mark F
G add:shift D add:mark|add:shift D add:mark G
H add:shift D add:mark|add:shift D add:mark H
I add:shift D add:mark|add:shift D add:mark I
J add:shift D add:mark|add:shift D add:mark J
K add:shift D add:mark|add:shift D add:mark K
L add:shift D add:mark|add:shift D add:mark L
M add:shift D add:mark|add:shift D add:mark M
N add:shift D add:mark|add:shift D add:mark N
O add:shift D add:mark|add:shift D add:mark O
P add:shift D add:mark|add:shift D add:mark P
Q add:shift D add:mark|add:shift D add:mark Q
R add:shift D add:mark|add:shift D add:mark R
S add:shift D add:mark|add:shift D add:mark S
T add:shift D add:mark|add:shift D add:mark T
U add:shift D add:mark|add:shift D add:mark U
V add:shift D add:mark|add:shift D add:mark V
W add:shift D add:mark|add:shift D add:mark W
X add:shift D add:mark|add:shift D add:mark X
Y add:shift D add:mark|add:shift D add:mark Y
Z add:shift D add:mark|add:shift D add:mark Z

0 add:shift E add:mark|add:shift E add:mark 0
1 add:shift E add:mark|add:shift E add:mark 1
2 add:shift E add:mark|add:shift E add:mark 2
3 add:shift E add:mark|add:shift E add:mark 3
4 add:shift E add:mark|add:shift E add:mark 4
5 add:shift E add:mark|add:shift E add:mark 5
6 add:shift E add:mark|add:shift E add:mark 6
7 add:shift E add:mark|add:shift E add:mark 7
8 add:shift E add:mark|add:shift E add:mark 8
9 add:shift E add:mark|add:shift E add:mark 9
A add:shift E add:mark|add:shift E add:mark A
B add:shift E add:mark|add:shift E add:mark B
C add:shift E add:mark|add:shift E add:mark C
D add:shift E add:mark|add:shift E add:mark D
E add:shift E add:mark|add:shift E add:mark E
F add:shift E add:mark|add:shift E add:mark F
G add:shift E add:mark|add:shift E add:mark G
H add:shift E add:mark|add:shift E add:mark H
I add:shift E add:mark|add:shift E add:mark I
J add:shift E add:mark|add:shift E add:mark J
K add:shift E add:mark|add:shift E add:mark K
L add:shift E add:mark|add:shift E add:mark L
M add:shift E add:mark|add:shift E add:mark M
N add:shift E add:mark|add:shift E add:mark N
O add:shift E add:mark|add:shift E add:mark O
P add:shift E add:mark|add:shift E add:mark P
Q add:shift E add:mark|add:shift E add:mark Q
R add:shift E add:mark|add:shift E add:mark R
S add:shift E add:mark|add:shift E add:mark S
T add:shift E add:mark|add:shift E add:mark T
U add:shift E add:mark|add:shift E add:mark U
V add:shift E add:mark|add:shift E add:mark V
W add:shift E add:mark|add:shift E add:mark W
X add:shift E add:mark|add:shift E add:mark X
Y add:shift E add:mark|add:shift E add:mark Y
Z add:shift E add:mark|add:shift E add:mark Z

0 add:shift F add:mark|add:shift F add:mark 0
1 add:shift F add:mark|add:shift F add:mark 1
2 add:shift F add:mark|add:shift F add:mark 2
3 add:shift F add:mark|add:shift F add:mark 3
4 add:shift F add:mark|add:shift F add:mark 4
5 add:shift F add:mark|add:shift F add:mark 5
6 add:shift F add:mark|add:shift F add:mark 6
7 add:shift F add:mark|add:shift F add:mark 7
8 add:shift F add:mark|add:shift F add:mark 8
9 add:shift F add:mark|add:shift F add:mark 9
A add:shift F add:mark|add:shift F add:mark A
B add:shift F add:mark|add:shift F add:mark B
C add:shift F add:mark|add:shift F add:mark C
D add:shift F add:mark|add:shift F add:mark D
E add:shift F add:mark|add:shift F add:mark E
F add:shift F add:mark|add:shift F add:mark F
G add:shift F add:mark|add:shift F add:mark G
H add:shift F add:mark|add:shift F add:mark H
I add:shift F add:mark|add:shift F add:mark I
J add:shift F add:mark|add:shift F add:mark J
K add:shift F add:mark|add:shift F add:mark K
L add:shift F add:mark|add:shift F add:mark L
M add:shift F add:mark|add:shift F add:mark M
N add:shift F add:mark|add:shift F add:mark N
O add:shift F add:mark|add:shift F add:mark O
P add:shift F add:mark|add:shift F add:mark P
Q add:shift F add:mark|add:shift F add:mark Q
R add:shift F add:mark|add:shift F add:mark R
S add:shift F add:mark|add:shift F add:mark S
T add:shift F add:mark|add:shift F add:mark T
U add:shift F add:mark|add:shift F add:mark U
V add:shift F add:mark|add:shift F add:mark V
W add:shift F add:mark|add:shift F add:mark W
X add:shift F add:mark|add:shift F add:mark X
Y add:shift F add:mark|add:shift F add:mark Y
Z add:shift F add:mark|add:shift F add:mark Z

0 add:shift G add:mark|add:shift G add:mark 0
1 add:shift G add:mark|add:shift G add:mark 1
2 add:shift G add:mark|add:shift G add:mark 2
3 add:shift G add:mark|add:shift G add:mark 3
4 add:shift G add:mark|add:shift G add:mark 4
5 add:shift G add:mark|add:shift G add:mark 5
6 add:shift G add:mark|add:shift G add:mark 6
7 add:shift G add:mark|add:shift G add:mark 7
8 add:shift G add:mark|add:shift G add:mark 8
9 add:shift G add:mark|add:shift G add:mark 9
A add:shift G add:mark|add:shift G add:mark A
B add:shift G add:mark|add:shift G add:mark B
C add:shift G add:mark|add:shift G add:mark C
D add:shift G add:mark|add:shift G add:mark D
E add:shift G add:mark|add:shift G add:mark E
F add:shift G add:mark|add:shift G add:mark F
G add:shift G add:mark|add:shift G add:mark G
H add:shift G add:mark|add:shift G add:mark H
I add:shift G add:mark|add:shift G add:mark I
J add:shift G add:mark|add:shift G add:mark J
K add:shift G add:mark|add:shift G add:mark K
L add:shift G add:mark|add:shift G add:mark L
M add:shift G add:mark|add:shift G add:mark M
N add:shift G add:mark|add:shift G add:mark N
O add:shift G add:mark|add:shift G add:mark O
P add:shift G add:mark|add:shift G add:mark P
Q add:shift G add:mark|add:shift G add:mark Q
R add:shift G add:mark|add:shift G add:mark R
S add:shift G add:mark|add:shift G add:mark S
T add:shift G add:mark|add:shift G add:mark T
U add:shift G add:mark|add:shift G add:mark U
V add:shift G add:mark|add:shift G add:mark V
W add:shift G add:mark|add:shift G add:mark W
X add:shift G add:mark|add:shift G add:mark X
Y add:shift G add:mark|add:shift G add:mark Y
Z add:shift G add:mark|add:shift G add:mark Z

0 add:shift H add:mark|add:shift H add:mark 0
1 add:shift H add:mark|add:shift H add:mark 1
2 add:shift H add:mark|add:shift H add:mark 2
3 add:shift H add:mark|add:shift H add:mark 3
4 add:shift H add:mark|add:shift H add:mark 4
5 add:shift H add:mark|add:shift H add:mark 5
6 add:shift H add:mark|add:shift H add:mark 6
7 add:shift H add:mark|add:shift H add:mark 7
8 add:shift H add:mark|add:shift H add:mark 8
9 add:shift H add:mark|add:shift H add:mark 9
A add:shift H add:mark|add:shift H add:mark A
B add:shift H add:mark|add:shift H add:mark B
C add:shift H add:mark|add:shift H add:mark C
D add:shift H add:mark|add:shift H add:mark D
E add:shift H add:mark|add:shift H add:mark E
F add:shift H add:mark|add:shift H add:mark F
G add:shift H add:mark|add:shift H add:mark G
H add:shift H add:mark|add:shift H add:mark H
I add:shift H add:mark|add:shift H add:mark I
J add:shift H add:mark|add:shift H add:mark J
K add:shift H add:mark|add:shift H add:mark K
L add:shift H add:mark|add:shift H add:mark L
M add:shift H add:mark|add:shift H add:mark M
N add:shift H add:mark|add:shift H add:mark N
O add:shift H add:mark|add:shift H add:mark O
P add:shift H add:mark|add:shift H add:mark P
Q add:shift H add:mark|add:shift H add:mark Q
R add:shift H add:mark|add:shift H add:mark R
S add:shift H add:mark|add:shift H add:mark S
T add:shift H add:mark|add:shift H add:mark T
U add:shift H add:mark|add:shift H add:mark U
V add:shift H add:mark|add:shift H add:mark V
W add:shift H add:mark|add:shift H add:mark W
X add:shift H add:mark|add:shift H add:mark X
Y add:shift H add:mark|add:shift H add:mark Y
Z add:shift H add:mark|add:shift H add:mark Z

0 add:shift I add:mark|add:shift I add:mark 0
1 add:shift I add:mark|add:shift I add:mark 1
2 add:shift I add:mark|add:shift I add:mark 2
3 add:shift I add:mark|add:shift I add:mark 3
4 add:shift I add:mark|add:shift I add:mark 4
5 add:shift I add:mark|add:shift I add:mark 5
6 add:shift I add:mark|add:shift I add:mark 6
7 add:shift I add:mark|add:shift I add:mark 7
8 add:shift I add:mark|add:shift I add:mark 8
9 add:shift I add:mark|add:shift I add:mark 9
A add:shift I add:mark|add:shift I add:mark A
B add:shift I add:mark|add:shift I add:mark B
C add:shift I add:mark|add:shift I add:mark C
D add:shift I add:mark|add:shift I add:mark D
E add:shift I add:mark|add:shift I add:mark E
F add:shift I add:mark|add:shift I add:mark F
G add:shift I add:mark|add:shift I add:mark G
H add:shift I add:mark|add:shift I add:mark H
I add:shift I add:mark|add:shift I add:mark I
J add:shift I add:mark|add:shift I add:mark J
K add:shift I add:mark|add:shift I add:mark K
L add:shift I add:mark|add:shift I add:mark L
M add:shift I add:mark|add:shift I add:mark M
N add:shift I add:mark|add:shift I add:mark N
O add:shift I add:mark|add:shift I add:mark O
P add:shift I add:mark|add:shift I add:mark P
Q add:shift I add:mark|add:shift I add:mark Q
R add:shift I add:mark|add:shift I add:mark R
S add:shift I add:mark|add:shift I add:mark S
T add:shift I add:mark|add:shift I add:mark T
U add:shift I add:mark|add:shift I add:mark U
V add:shift I add:mark|add:shift I add:mark V
W add:shift I add:mark|add:shift I add:mark W
X add:shift I add:mark|add:shift I add:mark X
Y add:shift I add:mark|add:shift I add:mark Y
Z add:shift I add:mark|add:shift I add:mark Z

0 add:shift J add:mark|add:shift J add:mark 0
1 add:shift J add:mark|add:shift J add:mark 1
2 add:shift J add:mark|add:shift J add:mark 2
3 add:shift J add:mark|add:shift J add:mark 3
4 add:shift J add:mark|add:shift J add:mark 4
5 add:shift J add:mark|add:shift J add:mark 5
6 add:shift J add:mark|add:shift J add:mark 6
7 add:shift J add:mark|add:shift J add:mark 7
8 add:shift J add:mark|add:shift J add:mark 8
9 add:shift J add:mark|add:shift J add:mark 9
A add:shift J add:mark|add:shift J add:mark A
B add:shift J add:mark|add:shift J add:mark B
C add:shift J add:mark|add:shift J add:mark C
D add:shift J add:mark|add:shift J add:mark D
E add:shift J add:mark|add:shift J add:mark E
F add:shift J add:mark|add:shift J add:mark F
G add:shift J add:mark|add:shift J add:mark G
H add:shift J add:mark|add:shift J add:mark H
I add:shift J add:mark|add:shift J add:mark I
J add:shift J add:mark|add:shift J add:mark J
K add:shift J add:mark|add:shift J add:mark K
L add:shift J add:mark|add:shift J add:mark L
M add:shift J add:mark|add:shift J add:mark M
N add:shift J add:mark|add:shift J add:mark N
O add:shift J add:mark|add:shift J add:mark O
P add:shift J add:mark|add:shift J add:mark P
Q add:shift J add:mark|add:shift J add:mark Q
R add:shift J add:mark|add:shift J add:mark R
S add:shift J add:mark|add:shift J add:mark S
T add:shift J add:mark|add:shift J add:mark T
U add:shift J add:mark|add:shift J add:mark U
V add:shift J add:mark|add:shift J add:mark V
W add:shift J add:mark|add:shift J add:mark W
X add:shift J add:mark|add:shift J add:mark X
Y add:shift J add:mark|add:shift J add:mark Y
Z add:shift J add:mark|add:shift J add:mark Z

0 add:shift K add:mark|add:shift K add:mark 0
1 add:shift K add:mark|add:shift K add:mark 1
2 add:shift K add:mark|add:shift K add:mark 2
3 add:shift K add:mark|add:shift K add:mark 3
4 add:shift K add:mark|add:shift K add:mark 4
5 add:shift K add:mark|add:shift K add:mark 5
6 add:shift K add:mark|add:shift K add:mark 6
7 add:shift K add:mark|add:shift K add:mark 7
8 add:shift K add:mark|add:shift K add:mark 8
9 add:shift K add:mark|add:shift K add:mark 9
A add:shift K add:mark|add:shift K add:mark A
B add:shift K add:mark|add:shift K add:mark B
C add:shift K add:mark|add:shift K add:mark C
D add:shift K add:mark|add:shift K add:mark D
E add:shift K add:mark|add:shift K add:mark E
F add:shift K add:mark|add:shift K add:mark F
G add:shift K add:mark|add:shift K add:mark G
H add:shift K add:mark|add:shift K add:mark H
I add:shift K add:mark|add:shift K add:mark I
J add:shift K add:mark|add:shift K add:mark J
K add:shift K add:mark|add:shift K add:mark K
L add:shift K add:mark|add:shift K add:mark L
M add:shift K add:mark|add:shift K add:mark M
N add:shift K add:mark|add:shift K add:mark N
O add:shift K add:mark|add:shift K add:mark O
P add:shift K add:mark|add:shift K add:mark P
Q add:shift K add:mark|add:shift K add:mark Q
R add:shift K add:mark|add:shift K add:mark R
S add:shift K add:mark|add:shift K add:mark S
T add:shift K add:mark|add:shift K add:mark T
U add:shift K add:mark|add:shift K add:mark U
V add:shift K add:mark|add:shift K add:mark V
W add:shift K add:mark|add:shift K add:mark W
X add:shift K add:mark|add:shift K add:mark X
Y add:shift K add:mark|add:shift K add:mark Y
Z add:shift K add:mark|add:shift K add:mark Z

0 add:shift L add:mark|add:shift L add:mark 0
1 add:shift L add:mark|add:shift L add:mark 1
2 add:shift L add:mark|add:shift L add:mark 2
3 add:shift L add:mark|add:shift L add:mark 3
4 add:shift L add:mark|add:shift L add:mark 4
5 add:shift L add:mark|add:shift L add:mark 5
6 add:shift L add:mark|add:shift L add:mark 6
7 add:shift L add:mark|add:shift L add:mark 7
8 add:shift L add:mark|add:shift L add:mark 8
9 add:shift L add:mark|add:shift L add:mark 9
A add:shift L add:mark|add:shift L add:mark A
B add:shift L add:mark|add:shift L add:mark B
C add:shift L add:mark|add:shift L add:mark C
D add:shift L add:mark|add:shift L add:mark D
E add:shift L add:mark|add:shift L add:mark E
F add:shift L add:mark|add:shift L add:mark F
G add:shift L add:mark|add:shift L add:mark G
H add:shift L add:mark|add:shift L add:mark H
I add:shift L add:mark|add:shift L add:mark I
J add:shift L add:mark|add:shift L add:mark J
K add:shift L add:mark|add:shift L add:mark K
L add:shift L add:mark|add:shift L add:mark L
M add:shift L add:mark|add:shift L add:mark M
N add:shift L add:mark|add:shift L add:mark N
O add:shift L add:mark|add:shift L add:mark O
P add:shift L add:mark|add:shift L add:mark P
Q add:shift L add:mark|add:shift L add:mark Q
R add:shift L add:mark|add:shift L add:mark R
S add:shift L add:mark|add:shift L add:mark S
T add:shift L add:mark|add:shift L add:mark T
U add:shift L add:mark|add:shift L add:mark U
V add:shift L add:mark|add:shift L add:mark V
W add:shift L add:mark|add:shift L add:mark W
X add:shift L add:mark|add:shift L add:mark X
Y add:shift L add:mark|add:shift L add:mark Y
Z add:shift L add:mark|add:shift L add:mark Z

0 add:shift M add:mark|add:shift M add:mark 0
1 add:shift M add:mark|add:shift M add:mark 1
2 add:shift M add:mark|add:shift M add:mark 2
3 add:shift M add:mark|add:shift M add:mark 3
4 add:shift M add:mark|add:shift M add:mark 4
5 add:shift M add:mark|add:shift M add:mark 5
6 add:shift M add:mark|add:shift M add:mark 6
7 add:shift M add:mark|add:shift M add:mark 7
8 add:shift M add:mark|add:shift M add:mark 8
9 add:shift M add:mark|add:shift M add:mark 9
A add:shift M add:mark|add:shift M add:mark A
B add:shift M add:mark|add:shift M add:mark B
C add:shift M add:mark|add:shift M add:mark C
D add:shift M add:mark|add:shift M add:mark D
E add:shift M add:mark|add:shift M add:mark E
F add:shift M add:mark|add:shift M add:mark F
G add:shift M add:mark|add:shift M add:mark G
H add:shift M add:mark|add:shift M add:mark H
I add:shift M add:mark|add:shift M add:mark I
J add:shift M add:mark|add:shift M add:mark J
K add:shift M add:mark|add:shift M add:mark K
L add:shift M add:mark|add:shift M add:mark L
M add:shift M add:mark|add:shift M add:mark M
N add:shift M add:mark|add:shift M add:mark N
O add:shift M add:mark|add:shift M add:mark O
P add:shift M add:mark|add:shift M add:mark P
Q add:shift M add:mark|add:shift M add:mark Q
R add:shift M add:mark|add:shift M add:mark R
S add:shift M add:mark|add:shift M add:mark S
T add:shift M add:mark|add:shift M add:mark T
U add:shift M add:mark|add:shift M add:mark U
V add:shift M add:mark|add:shift M add:mark V
W add:shift M add:mark|add:shift M add:mark W
X add:shift M add:mark|add:shift M add:mark X
Y add:shift M add:mark|add:shift M add:mark Y
Z add:shift M add:mark|add:shift M add:mark Z

0 add:shift N add:mark|add:shift N add:mark 0
1 add:shift N add:mark|add:shift N add:mark 1
2 add:shift N add:mark|add:shift N add:mark 2
3 add:shift N add:mark|add:shift N add:mark 3
4 add:shift N add:mark|add:shift N add:mark 4
5 add:shift N add:mark|add:shift N add:mark 5
6 add:shift N add:mark|add:shift N add:mark 6
7 add:shift N add:mark|add:shift N add:mark 7
8 add:shift N add:mark|add:shift N add:mark 8
9 add:shift N add:mark|add:shift N add:mark 9
A add:shift N add:mark|add:shift N add:mark A
B add:shift N add:mark|add:shift N add:mark B
C add:shift N add:mark|add:shift N add:mark C
D add:shift N add:mark|add:shift N add:mark D
E add:shift N add:mark|add:shift N add:mark E
F add:shift N add:mark|add:shift N add:mark F
G add:shift N add:mark|add:shift N add:mark G
H add:shift N add:mark|add:shift N add:mark H
I add:shift N add:mark|add:shift N add:mark I
J add:shift N add:mark|add:shift N add:mark J
K add:shift N add:mark|add:shift N add:mark K
L add:shift N add:mark|add:shift N add:mark L
M add:shift N add:mark|add:shift N add:mark M
N add:shift N add:mark|add:shift N add:mark N
O add:shift N add:mark|add:shift N add:mark O
P add:shift N add:mark|add:shift N add:mark P
Q add:shift N add:mark|add:shift N add:mark Q
R add:shift N add:mark|add:shift N add:mark R
S add:shift N add:mark|add:shift N add:mark S
T add:shift N add:mark|add:shift N add:mark T
U add:shift N add:mark|add:shift N add:mark U
V add:shift N add:mark|add:shift N add:mark V
W add:shift N add:mark|add:shift N add:mark W
X add:shift N add:mark|add:shift N add:mark X
Y add:shift N add:mark|add:shift N add:mark Y
Z add:shift N add:mark|add:shift N add:mark Z

0 add:shift O add:mark|add:shift O add:mark 0
1 add:shift O add:mark|add:shift O add:mark 1
2 add:shift O add:mark|add:shift O add:mark 2
3 add:shift O add:mark|add:shift O add:mark 3
4 add:shift O add:mark|add:shift O add:mark 4
5 add:shift O add:mark|add:shift O add:mark 5
6 add:shift O add:mark|add:shift O add:mark 6
7 add:shift O add:mark|add:shift O add:mark 7
8 add:shift O add:mark|add:shift O add:mark 8
9 add:shift O add:mark|add:shift O add:mark 9
A add:shift O add:mark|add:shift O add:mark A
B add:shift O add:mark|add:shift O add:mark B
C add:shift O add:mark|add:shift O add:mark C
D add:shift O add:mark|add:shift O add:mark D
E add:shift O add:mark|add:shift O add:mark E
F add:shift O add:mark|add:shift O add:mark F
G add:shift O add:mark|add:shift O add:mark G
H add:shift O add:mark|add:shift O add:mark H
I add:shift O add:mark|add:shift O add:mark I
J add:shift O add:mark|add:shift O add:mark J
K add:shift O add:mark|add:shift O add:mark K
L add:shift O add:mark|add:shift O add:mark L
M add:shift O add:mark|add:shift O add:mark M
N add:shift O add:mark|add:shift O add:mark N
O add:shift O add:mark|add:shift O add:mark O
P add:shift O add:mark|add:shift O add:mark P
Q add:shift O add:mark|add:shift O add:mark Q
R add:shift O add:mark|add:shift O add:mark R
S add:shift O add:mark|add:shift O add:mark S
T add:shift O add:mark|add:shift O add:mark T
U add:shift O add:mark|add:shift O add:mark U
V add:shift O add:mark|add:shift O add:mark V
W add:shift O add:mark|add:shift O add:mark W
X add:shift O add:mark|add:shift O add:mark X
Y add:shift O add:mark|add:shift O add:mark Y
Z add:shift O add:mark|add:shift O add:mark Z

0 add:shift P add:mark|add:shift P add:mark 0
1 add:shift P add:mark|add:shift P add:mark 1
2 add:shift P add:mark|add:shift P add:mark 2
3 add:shift P add:mark|add:shift P add:mark 3
4 add:shift P add:mark|add:shift P add:mark 4
5 add:shift P add:mark|add:shift P add:mark 5
6 add:shift P add:mark|add:shift P add:mark 6
7 add:shift P add:mark|add:shift P add:mark 7
8 add:shift P add:mark|add:shift P add:mark 8
9 add:shift P add:mark|add:shift P add:mark 9
A add:shift P add:mark|add:shift P add:mark A
B add:shift P add:mark|add:shift P add:mark B
C add:shift P add:mark|add:shift P add:mark C
D add:shift P add:mark|add:shift P add:mark D
E add:shift P add:mark|add:shift P add:mark E
F add:shift P add:mark|add:shift P add:mark F
G add:shift P add:mark|add:shift P add:mark G
H add:shift P add:mark|add:shift P add:mark H
I add:shift P add:mark|add:shift P add:mark I
J add:shift P add:mark|add:shift P add:mark J
K add:shift P add:mark|add:shift P add:mark K
L add:shift P add:mark|add:shift P add:mark L
M add:shift P add:mark|add:shift P add:mark M
N add:shift P add:mark|add:shift P add:mark N
O add:shift P add:mark|add:shift P add:mark O
P add:shift P add:mark|add:shift P add:mark P
Q add:shift P add:mark|add:shift P add:mark Q
R add:shift P add:mark|add:shift P add:mark R
S add:shift P add:mark|add:shift P add:mark S
T add:shift P add:mark|add:shift P add:mark T
U add:shift P add:mark|add:shift P add:mark U
V add:shift P add:mark|add:shift P add:mark V
W add:shift P add:mark|add:shift P add:mark W
X add:shift P add:mark|add:shift P add:mark X
Y add:shift P add:mark|add:shift P add:mark Y
Z add:shift P add:mark|add:shift P add:mark Z

0 add:shift Q add:mark|add:shift Q add:mark 0
1 add:shift Q add:mark|add:shift Q add:mark 1
2 add:shift Q add:mark|add:shift Q add:mark 2
3 add:shift Q add:mark|add:shift Q add:mark 3
4 add:shift Q add:mark|add:shift Q add:mark 4
5 add:shift Q add:mark|add:shift Q add:mark 5
6 add:shift Q add:mark|add:shift Q add:mark 6
7 add:shift Q add:mark|add:shift Q add:mark 7
8 add:shift Q add:mark|add:shift Q add:mark 8
9 add:shift Q add:mark|add:shift Q add:mark 9
A add:shift Q add:mark|add:shift Q add:mark A
B add:shift Q add:mark|add:shift Q add:mark B
C add:shift Q add:mark|add:shift Q add:mark C
D add:shift Q add:mark|add:shift Q add:mark D
E add:shift Q add:mark|add:shift Q add:mark E
F add:shift Q add:mark|add:shift Q add:mark F
G add:shift Q add:mark|add:shift Q add:mark G
H add:shift Q add:mark|add:shift Q add:mark H
I add:shift Q add:mark|add:shift Q add:mark I
J add:shift Q add:mark|add:shift Q add:mark J
K add:shift Q add:mark|add:shift Q add:mark K
L add:shift Q add:mark|add:shift Q add:mark L
M add:shift Q add:mark|add:shift Q add:mark M
N add:shift Q add:mark|add:shift Q add:mark N
O add:shift Q add:mark|add:shift Q add:mark O
P add:shift Q add:mark|add:shift Q add:mark P
Q add:shift Q add:mark|add:shift Q add:mark Q
R add:shift Q add:mark|add:shift Q add:mark R
S add:shift Q add:mark|add:shift Q add:mark S
T add:shift Q add:mark|add:shift Q add:mark T
U add:shift Q add:mark|add:shift Q add:mark U
V add:shift Q add:mark|add:shift Q add:mark V
W add:shift Q add:mark|add:shift Q add:mark W
X add:shift Q add:mark|add:shift Q add:mark X
Y add:shift Q add:mark|add:shift Q add:mark Y
Z add:shift Q add:mark|add:shift Q add:mark Z

0 add:shift R add:mark|add:shift R add:mark 0
1 add:shift R add:mark|add:shift R add:mark 1
2 add:shift R add:mark|add:shift R add:mark 2
3 add:shift R add:mark|add:shift R add:mark 3
4 add:shift R add:mark|add:shift R add:mark 4
5 add:shift R add:mark|add:shift R add:mark 5
6 add:shift R add:mark|add:shift R add:mark 6
7 add:shift R add:mark|add:shift R add:mark 7
8 add:shift R add:mark|add:shift R add:mark 8
9 add:shift R add:mark|add:shift R add:mark 9
A add:shift R add:mark|add:shift R add:mark A
B add:shift R add:mark|add:shift R add:mark B
C add:shift R add:mark|add:shift R add:mark C
D add:shift R add:mark|add:shift R add:mark D
E add:shift R add:mark|add:shift R add:mark E
F add:shift R add:mark|add:shift R add:mark F
G add:shift R add:mark|add:shift R add:mark G
H add:shift R add:mark|add:shift R add:mark H
I add:shift R add:mark|add:shift R add:mark I
J add:shift R add:mark|add:shift R add:mark J
K add:shift R add:mark|add:shift R add:mark K
L add:shift R add:mark|add:shift R add:mark L
M add:shift R add:mark|add:shift R add:mark M
N add:shift R add:mark|add:shift R add:mark N
O add:shift R add:mark|add:shift R add:mark O
P add:shift R add:mark|add:shift R add:mark P
Q add:shift R add:mark|add:shift R add:mark Q
R add:shift R add:mark|add:shift R add:mark R
S add:shift R add:mark|add:shift R add:mark S
T add:shift R add:mark|add:shift R add:mark T
U add:shift R add:mark|add:shift R add:mark U
V add:shift R add:mark|add:shift R add:mark V
W add:shift R add:mark|add:shift R add:mark W
X add:shift R add:mark|add:shift R add:mark X
Y add:shift R add:mark|add:shift R add:mark Y
Z add:shift R add:mark|add:shift R add:mark Z

0 add:shift S add:mark|add:shift S add:mark 0
1 add:shift S add:mark|add:shift S add:mark 1
2 add:shift S add:mark|add:shift S add:mark 2
3 add:shift S add:mark|add:shift S add:mark 3
4 add:shift S add:mark|add:shift S add:mark 4
5 add:shift S add:mark|add:shift S add:mark 5
6 add:shift S add:mark|add:shift S add:mark 6
7 add:shift S add:mark|add:shift S add:mark 7
8 add:shift S add:mark|add:shift S add:mark 8
9 add:shift S add:mark|add:shift S add:mark 9
A add:shift S add:mark|add:shift S add:mark A
B add:shift S add:mark|add:shift S add:mark B
C add:shift S add:mark|add:shift S add:mark C
D add:shift S add:mark|add:shift S add:mark D
E add:shift S add:mark|add:shift S add:mark E
F add:shift S add:mark|add:shift S add:mark F
G add:shift S add:mark|add:shift S add:mark G
H add:shift S add:mark|add:shift S add:mark H
I add:shift S add:mark|add:shift S add:mark I
J add:shift S add:mark|add:shift S add:mark J
K add:shift S add:mark|add:shift S add:mark K
L add:shift S add:mark|add:shift S add:mark L
M add:shift S add:mark|add:shift S add:mark M
N add:shift S add:mark|add:shift S add:mark N
O add:shift S add:mark|add:shift S add:mark O
P add:shift S add:mark|add:shift S add:mark P
Q add:shift S add:mark|add:shift S add:mark Q
R add:shift S add:mark|add:shift S add:mark R
S add:shift S add:mark|add:shift S add:mark S
T add:shift S add:mark|add:shift S add:mark T
U add:shift S add:mark|add:shift S add:mark U
V add:shift S add:mark|add:shift S add:mark V
W add:shift S add:mark|add:shift S add:mark W
X add:shift S add:mark|add:shift S add:mark X
Y add:shift S add:mark|add:shift S add:mark Y
Z add:shift S add:mark|add:shift S add:mark Z

0 add:shift T add:mark|add:shift T add:mark 0
1 add:shift T add:mark|add:shift T add:mark 1
2 add:shift T add:mark|add:shift T add:mark 2
3 add:shift T add:mark|add:shift T add:mark 3
4 add:shift T add:mark|add:shift T add:mark 4
5 add:shift T add:mark|add:shift T add:mark 5
6 add:shift T add:mark|add:shift T add:mark 6
7 add:shift T add:mark|add:shift T add:mark 7
8 add:shift T add:mark|add:shift T add:mark 8
9 add:shift T add:mark|add:shift T add:mark 9
A add:shift T add:mark|add:shift T add:mark A
B add:shift T add:mark|add:shift T add:mark B
C add:shift T add:mark|add:shift T add:mark C
D add:shift T add:mark|add:shift T add:mark D
E add:shift T add:mark|add:shift T add:mark E
F add:shift T add:mark|add:shift T add:mark F
G add:shift T add:mark|add:shift T add:mark G
H add:shift T add:mark|add:shift T add:mark H
I add:shift T add:mark|add:shift T add:mark I
J add:shift T add:mark|add:shift T add:mark J
K add:shift T add:mark|add:shift T add:mark K
L add:shift T add:mark|add:shift T add:mark L
M add:shift T add:mark|add:shift T add:mark M
N add:shift T add:mark|add:shift T add:mark N
O add:shift T add:mark|add:shift T add:mark O
P add:shift T add:mark|add:shift T add:mark P
Q add:shift T add:mark|add:shift T add:mark Q
R add:shift T add:mark|add:shift T add:mark R
S add:shift T add:mark|add:shift T add:mark S
T add:shift T add:mark|add:shift T add:mark T
U add:shift T add:mark|add:shift T add:mark U
V add:shift T add:mark|add:shift T add:mark V
W add:shift T add:mark|add:shift T add:mark W
X add:shift T add:mark|add:shift T add:mark X
Y add:shift T add:mark|add:shift T add:mark Y
Z add:shift T add:mark|add:shift T add:mark Z

0 add:shift U add:mark|add:shift U add:mark 0
1 add:shift U add:mark|add:shift U add:mark 1
2 add:shift U add:mark|add:shift U add:mark 2
3 add:shift U add:mark|add:shift U add:mark 3
4 add:shift U add:mark|add:shift U add:mark 4
5 add:shift U add:mark|add:shift U add:mark 5
6 add:shift U add:mark|add:shift U add:mark 6
7 add:shift U add:mark|add:shift U add:mark 7
8 add:shift U add:mark|add:shift U add:mark 8
9 add:shift U add:mark|add:shift U add:mark 9
A add:shift U add:mark|add:shift U add:mark A
B add:shift U add:mark|add:shift U add:mark B
C add:shift U add:mark|add:shift U add:mark C
D add:shift U add:mark|add:shift U add:mark D
E add:shift U add:mark|add:shift U add:mark E
F add:shift U add:mark|add:shift U add:mark F
G add:shift U add:mark|add:shift U add:mark G
H add:shift U add:mark|add:shift U add:mark H
I add:shift U add:mark|add:shift U add:mark I
J add:shift U add:mark|add:shift U add:mark J
K add:shift U add:mark|add:shift U add:mark K
L add:shift U add:mark|add:shift U add:mark L
M add:shift U add:mark|add:shift U add:mark M
N add:shift U add:mark|add:shift U add:mark N
O add:shift U add:mark|add:shift U add:mark O
P add:shift U add:mark|add:shift U add:mark P
Q add:shift U add:mark|add:shift U add:mark Q
R add:shift U add:mark|add:shift U add:mark R
S add:shift U add:mark|add:shift U add:mark S
T add:shift U add:mark|add:shift U add:mark T
U add:shift U add:mark|add:shift U add:mark U
V add:shift U add:mark|add:shift U add:mark V
W add:shift U add:mark|add:shift U add:mark W
X add:shift U add:mark|add:shift U add:mark X
Y add:shift U add:mark|add:shift U add:mark Y
Z add:shift U add:mark|add:shift U add:mark Z

0 add:shift V add:mark|add:shift V add:mark 0
1 add:shift V add:mark|add:shift V add:mark 1
2 add:shift V add:mark|add:shift V add:mark 2
3 add:shift V add:mark|add:shift V add:mark 3
4 add:shift V add:mark|add:shift V add:mark 4
5 add:shift V add:mark|add:shift V add:mark 5
6 add:shift V add:mark|add:shift V add:mark 6
7 add:shift V add:mark|add:shift V add:mark 7
8 add:shift V add:mark|add:shift V add:mark 8
9 add:shift V add:mark|add:shift V add:mark 9
A add:shift V add:mark|add:shift V add:mark A
B add:shift V add:mark|add:shift V add:mark B
C add:shift V add:mark|add:shift V add:mark C
D add:shift V add:mark|add:shift V add:mark D
E add:shift V add:mark|add:shift V add:mark E
F add:shift V add:mark|add:shift V add:mark F
G add:shift V add:mark|add:shift V add:mark G
H add:shift V add:mark|add:shift V add:mark H
I add:shift V add:mark|add:shift V add:mark I
J add:shift V add:mark|add:shift V add:mark J
K add:shift V add:mark|add:shift V add:mark K
L add:shift V add:mark|add:shift V add:mark L
M add:shift V add:mark|add:shift V add:mark M
N add:shift V add:mark|add:shift V add:mark N
O add:shift V add:mark|add:shift V add:mark O
P add:shift V add:mark|add:shift V add:mark P
Q add:shift V add:mark|add:shift V add:mark Q
R add:shift V add:mark|add:shift V add:mark R
S add:shift V add:mark|add:shift V add:mark S
T add:shift V add:mark|add:shift V add:mark T
U add:shift V add:mark|add:shift V add:mark U
V add:shift V add:mark|add:shift V add:mark V
W add:shift V add:mark|add:shift V add:mark W
X add:shift V add:mark|add:shift V add:mark X
Y add:shift V add:mark|add:shift V add:mark Y
Z add:shift V add:mark|add:shift V add:mark Z

0 add:shift W add:mark|add:shift W add:mark 0
1 add:shift W add:mark|add:shift W add:mark 1
2 add:shift W add:mark|add:shift W add:mark 2
3 add:shift W add:mark|add:shift W add:mark 3
4 add:shift W add:mark|add:shift W add:mark 4
5 add:shift W add:mark|add:shift W add:mark 5
6 add:shift W add:mark|add:shift W add:mark 6
7 add:shift W add:mark|add:shift W add:mark 7
8 add:shift W add:mark|add:shift W add:mark 8
9 add:shift W add:mark|add:shift W add:mark 9
A add:shift W add:mark|add:shift W add:mark A
B add:shift W add:mark|add:shift W add:mark B
C add:shift W add:mark|add:shift W add:mark C
D add:shift W add:mark|add:shift W add:mark D
E add:shift W add:mark|add:shift W add:mark E
F add:shift W add:mark|add:shift W add:mark F
G add:shift W add:mark|add:shift W add:mark G
H add:shift W add:mark|add:shift W add:mark H
I add:shift W add:mark|add:shift W add:mark I
J add:shift W add:mark|add:shift W add:mark J
K add:shift W add:mark|add:shift W add:mark K
L add:shift W add:mark|add:shift W add:mark L
M add:shift W add:mark|add:shift W add:mark M
N add:shift W add:mark|add:shift W add:mark N
O add:shift W add:mark|add:shift W add:mark O
P add:shift W add:mark|add:shift W add:mark P
Q add:shift W add:mark|add:shift W add:mark Q
R add:shift W add:mark|add:shift W add:mark R
S add:shift W add:mark|add:shift W add:mark S
T add:shift W add:mark|add:shift W add:mark T
U add:shift W add:mark|add:shift W add:mark U
V add:shift W add:mark|add:shift W add:mark V
W add:shift W add:mark|add:shift W add:mark W
X add:shift W add:mark|add:shift W add:mark X
Y add:shift W add:mark|add:shift W add:mark Y
Z add:shift W add:mark|add:shift W add:mark Z

0 add:shift X add:mark|add:shift X add:mark 0
1 add:shift X add:mark|add:shift X add:mark 1
2 add:shift X add:mark|add:shift X add:mark 2
3 add:shift X add:mark|add:shift X add:mark 3
4 add:shift X add:mark|add:shift X add:mark 4
5 add:shift X add:mark|add:shift X add:mark 5
6 add:shift X add:mark|add:shift X add:mark 6
7 add:shift X add:mark|add:shift X add:mark 7
8 add:shift X add:mark|add:shift X add:mark 8
9 add:shift X add:mark|add:shift X add:mark 9
A add:shift X add:mark|add:shift X add:mark A
B add:shift X add:mark|add:shift X add:mark B
C add:shift X add:mark|add:shift X add:mark C
D add:shift X add:mark|add:shift X add:mark D
E add:shift X add:mark|add:shift X add:mark E
F add:shift X add:mark|add:shift X add:mark F
G add:shift X add:mark|add:shift X add:mark G
H add:shift X add:mark|add:shift X add:mark H
I add:shift X add:mark|add:shift X add:mark I
J add:shift X add:mark|add:shift X add:mark J
K add:shift X add:mark|add:shift X add:mark K
L add:shift X add:mark|add:shift X add:mark L
M add:shift X add:mark|add:shift X add:mark M
N add:shift X add:mark|add:shift X add:mark N
O add:shift X add:mark|add:shift X add:mark O
P add:shift X add:mark|add:shift X add:mark P
Q add:shift X add:mark|add:shift X add:mark Q
R add:shift X add:mark|add:shift X add:mark R
S add:shift X add:mark|add:shift X add:mark S
T add:shift X add:mark|add:shift X add:mark T
U add:shift X add:mark|add:shift X add:mark U
V add:shift X add:mark|add:shift X add:mark V
W add:shift X add:mark|add:shift X add:mark W
X add:shift X add:mark|add:shift X add:mark X
Y add:shift X add:mark|add:shift X add:mark Y
Z add:shift X add:mark|add:shift X add:mark Z

0 add:shift Y add:mark|add:shift Y add:mark 0
1 add:shift Y add:mark|add:shift Y add:mark 1
2 add:shift Y add:mark|add:shift Y add:mark 2
3 add:shift Y add:mark|add:shift Y add:mark 3
4 add:shift Y add:mark|add:shift Y add:mark 4
5 add:shift Y add:mark|add:shift Y add:mark 5
6 add:shift Y add:mark|add:shift Y add:mark 6
7 add:shift Y add:mark|add:shift Y add:mark 7
8 add:shift Y add:mark|add:shift Y add:mark 8
9 add:shift Y add:mark|add:shift Y add:mark 9
A add:shift Y add:mark|add:shift Y add:mark A
B add:shift Y add:mark|add:shift Y add:mark B
C add:shift Y add:mark|add:shift Y add:mark C
D add:shift Y add:mark|add:shift Y add:mark D
E add:shift Y add:mark|add:shift Y add:mark E
F add:shift Y add:mark|add:shift Y add:mark F
G add:shift Y add:mark|add:shift Y add:mark G
H add:shift Y add:mark|add:shift Y add:mark H
I add:shift Y add:mark|add:shift Y add:mark I
J add:shift Y add:mark|add:shift Y add:mark J
K add:shift Y add:mark|add:shift Y add:mark K
L add:shift Y add:mark|add:shift Y add:mark L
M add:shift Y add:mark|add:shift Y add:mark M
N add:shift Y add:mark|add:shift Y add:mark N
O add:shift Y add:mark|add:shift Y add:mark O
P add:shift Y add:mark|add:shift Y add:mark P
Q add:shift Y add:mark|add:shift Y add:mark Q
R add:shift Y add:mark|add:shift Y add:mark R
S add:shift Y add:mark|add:shift Y add:mark S
T add:shift Y add:mark|add:shift Y add:mark T
U add:shift Y add:mark|add:shift Y add:mark U
V add:shift Y add:mark|add:shift Y add:mark V
W add:shift Y add:mark|add:shift Y add:mark W
X add:shift Y add:mark|add:shift Y add:mark X
Y add:shift Y add:mark|add:shift Y add:mark Y
Z add:shift Y add:mark|add:shift Y add:mark Z

0 add:shift Z add:mark|add:shift Z add:mark 0
1 add:shift Z add:mark|add:shift Z add:mark 1
2 add:shift Z add:mark|add:shift Z add:mark 2
3 add:shift Z add:mark|add:shift Z add:mark 3
4 add:shift Z add:mark|add:shift Z add:mark 4
5 add:shift Z add:mark|add:shift Z add:mark 5
6 add:shift Z add:mark|add:shift Z add:mark 6
7 add:shift Z add:mark|add:shift Z add:mark 7
8 add:shift Z add:mark|add:shift Z add:mark 8
9 add:shift Z add:mark|add:shift Z add:mark 9
A add:shift Z add:mark|add:shift Z add:mark A
B add:shift Z add:mark|add:shift Z add:mark B
C add:shift Z add:mark|add:shift Z add:mark C
D add:shift Z add:mark|add:shift Z add:mark D
E add:shift Z add:mark|add:shift Z add:mark E
F add:shift Z add:mark|add:shift Z add:mark F
G add:shift Z add:mark|add:shift Z add:mark G
H add:shift Z add:mark|add:shift Z add:mark H
I add:shift Z add:mark|add:shift Z add:mark I
J add:shift Z add:mark|add:shift Z add:mark J
K add:shift Z add:mark|add:shift Z add:mark K
L add:shift Z add:mark|add:shift Z add:mark L
M add:shift Z add:mark|add:shift Z add:mark M
N add:shift Z add:mark|add:shift Z add:mark N
O add:shift Z add:mark|add:shift Z add:mark O
P add:shift Z add:mark|add:shift Z add:mark P
Q add:shift Z add:mark|add:shift Z add:mark Q
R add:shift Z add:mark|add:shift Z add:mark R
S add:shift Z add:mark|add:shift Z add:mark S
T add:shift Z add:mark|add:shift Z add:mark T
U add:shift Z add:mark|add:shift Z add:mark U
V add:shift Z add:mark|add:shift Z add:mark V
W add:shift Z add:mark|add:shift Z add:mark W
X add:shift Z add:mark|add:shift Z add:mark X
Y add:shift Z add:mark|add:shift Z add:mark Y
Z add:shift Z add:mark|add:shift Z add:mark Z


== Add sig figures
add:start add:current add:shift 0 add:mark|add:start 0 add:current
add:start add:current add:shift 1 add:mark|add:start 1 add:current
add:start add:current add:shift 2 add:mark|add:start 2 add:current
add:start add:current add:shift 3 add:mark|add:start 3 add:current
add:start add:current add:shift 4 add:mark|add:start 4 add:current
add:start add:current add:shift 5 add:mark|add:start 5 add:current
add:start add:current add:shift 6 add:mark|add:start 6 add:current
add:start add:current add:shift 7 add:mark|add:start 7 add:current
add:start add:current add:shift 8 add:mark|add:start 8 add:current
add:start add:current add:shift 9 add:mark|add:start 9 add:current
add:start add:current add:shift A add:mark|add:start A add:current
add:start add:current add:shift B add:mark|add:start B add:current
add:start add:current add:shift C add:mark|add:start C add:current
add:start add:current add:shift D add:mark|add:start D add:current
add:start add:current add:shift E add:mark|add:start E add:current
add:start add:current add:shift F add:mark|add:start F add:current
add:start add:current add:shift G add:mark|add:start G add:current
add:start add:current add:shift H add:mark|add:start H add:current
add:start add:current add:shift I add:mark|add:start I add:current
add:start add:current add:shift J add:mark|add:start J add:current
add:start add:current add:shift K add:mark|add:start K add:current
add:start add:current add:shift L add:mark|add:start L add:current
add:start add:current add:shift M add:mark|add:start M add:current
add:start add:current add:shift N add:mark|add:start N add:current
add:start add:current add:shift O add:mark|add:start O add:current
add:start add:current add:shift P add:mark|add:start P add:current
add:start add:current add:shift Q add:mark|add:start Q add:current
add:start add:current add:shift R add:mark|add:start R add:current
add:start add:current add:shift S add:mark|add:start S add:current
add:start add:current add:shift T add:mark|add:start T add:current
add:start add:current add:shift U add:mark|add:start U add:current
add:start add:current add:shift V add:mark|add:start V add:current
add:start add:current add:shift W add:mark|add:start W add:current
add:start add:current add:shift X add:mark|add:start X add:current
add:start add:current add:shift Y add:mark|add:start Y add:current
add:start add:current add:shift Z add:mark|add:start Z add:current

== Clear completed tags
0 add:done add:cmd|add:cmd 0
1 add:done add:cmd|add:cmd 1
2 add:done add:cmd|add:cmd 2
3 add:done add:cmd|add:cmd 3
4 add:done add:cmd|add:cmd 4
5 add:done add:cmd|add:cmd 5
6 add:done add:cmd|add:cmd 6
7 add:done add:cmd|add:cmd 7
8 add:done add:cmd|add:cmd 8
9 add:done add:cmd|add:cmd 9
A add:done add:cmd|add:cmd A
B add:done add:cmd|add:cmd B
C add:done add:cmd|add:cmd C
D add:done add:cmd|add:cmd D
E add:done add:cmd|add:cmd E
F add:done add:cmd|add:cmd F
G add:done add:cmd|add:cmd G
H add:done add:cmd|add:cmd H
I add:done add:cmd|add:cmd I
J add:done add:cmd|add:cmd J
K add:done add:cmd|add:cmd K
L add:done add:cmd|add:cmd L
M add:done add:cmd|add:cmd M
N add:done add:cmd|add:cmd N
O add:done add:cmd|add:cmd O
P add:done add:cmd|add:cmd P
Q add:done add:cmd|add:cmd Q
R add:done add:cmd|add:cmd R
S add:done add:cmd|add:cmd S
T add:done add:cmd|add:cmd T
U add:done add:cmd|add:cmd U
V add:done add:cmd|add:cmd V
W add:done add:cmd|add:cmd W
X add:done add:cmd|add:cmd X
Y add:done add:cmd|add:cmd Y
Z add:done add:cmd|add:cmd Z

add:current add:cmd|

== Apply addend
0 add:current add:shift 0 add:mark|add:current 0 add:done
1 add:current add:shift 0 add:mark|add:current 1 add:done
2 add:current add:shift 0 add:mark|add:current 2 add:done
3 add:current add:shift 0 add:mark|add:current 3 add:done
4 add:current add:shift 0 add:mark|add:current 4 add:done
5 add:current add:shift 0 add:mark|add:current 5 add:done
6 add:current add:shift 0 add:mark|add:current 6 add:done
7 add:current add:shift 0 add:mark|add:current 7 add:done
8 add:current add:shift 0 add:mark|add:current 8 add:done
9 add:current add:shift 0 add:mark|add:current 9 add:done
A add:current add:shift 0 add:mark|add:current A add:done
B add:current add:shift 0 add:mark|add:current B add:done
C add:current add:shift 0 add:mark|add:current C add:done
D add:current add:shift 0 add:mark|add:current D add:done
E add:current add:shift 0 add:mark|add:current E add:done
F add:current add:shift 0 add:mark|add:current F add:done
G add:current add:shift 0 add:mark|add:current G add:done
H add:current add:shift 0 add:mark|add:current H add:done
I add:current add:shift 0 add:mark|add:current I add:done
J add:current add:shift 0 add:mark|add:current J add:done
K add:current add:shift 0 add:mark|add:current K add:done
L add:current add:shift 0 add:mark|add:current L add:done
M add:current add:shift 0 add:mark|add:current M add:done
N add:current add:shift 0 add:mark|add:current N add:done
O add:current add:shift 0 add:mark|add:current O add:done
P add:current add:shift 0 add:mark|add:current P add:done
Q add:current add:shift 0 add:mark|add:current Q add:done
R add:current add:shift 0 add:mark|add:current R add:done
S add:current add:shift 0 add:mark|add:current S add:done
T add:current add:shift 0 add:mark|add:current T add:done
U add:current add:shift 0 add:mark|add:current U add:done
V add:current add:shift 0 add:mark|add:current V add:done
W add:current add:shift 0 add:mark|add:current W add:done
X add:current add:shift 0 add:mark|add:current X add:done
Y add:current add:shift 0 add:mark|add:current Y add:done
Z add:current add:shift 0 add:mark|add:current Z add:done

0 add:current add:shift 1 add:mark|add:current 1 add:done
1 add:current add:shift 1 add:mark|add:current 2 add:done
2 add:current add:shift 1 add:mark|add:current 3 add:done
3 add:current add:shift 1 add:mark|add:current 4 add:done
4 add:current add:shift 1 add:mark|add:current 5 add:done
5 add:current add:shift 1 add:mark|add:current 6 add:done
6 add:current add:shift 1 add:mark|add:current 7 add:done
7 add:current add:shift 1 add:mark|add:current 8 add:done
8 add:current add:shift 1 add:mark|add:current 9 add:done
9 add:current add:shift 1 add:mark|add:current A add:done
A add:current add:shift 1 add:mark|add:current B add:done
B add:current add:shift 1 add:mark|add:current C add:done
C add:current add:shift 1 add:mark|add:current D add:done
D add:current add:shift 1 add:mark|add:current E add:done
E add:current add:shift 1 add:mark|add:current F add:done
F add:current add:shift 1 add:mark|add:current G add:done
G add:current add:shift 1 add:mark|add:current H add:done
H add:current add:shift 1 add:mark|add:current I add:done
I add:current add:shift 1 add:mark|add:current J add:done
J add:current add:shift 1 add:mark|add:current K add:done
K add:current add:shift 1 add:mark|add:current L add:done
L add:current add:shift 1 add:mark|add:current M add:done
M add:current add:shift 1 add:mark|add:current N add:done
N add:current add:shift 1 add:mark|add:current O add:done
O add:current add:shift 1 add:mark|add:current P add:done
P add:current add:shift 1 add:mark|add:current Q add:done
Q add:current add:shift 1 add:mark|add:current R add:done
R add:current add:shift 1 add:mark|add:current S add:done
S add:current add:shift 1 add:mark|add:current T add:done
T add:current add:shift 1 add:mark|add:current U add:done
U add:current add:shift 1 add:mark|add:current V add:done
V add:current add:shift 1 add:mark|add:current W add:done
W add:current add:shift 1 add:mark|add:current X add:done
X add:current add:shift 1 add:mark|add:current Y add:done
Y add:current add:shift 1 add:mark|add:current Z add:done

0 add:current add:shift 2 add:mark|add:current 2 add:done
1 add:current add:shift 2 add:mark|add:current 3 add:done
2 add:current add:shift 2 add:mark|add:current 4 add:done
3 add:current add:shift 2 add:mark|add:current 5 add:done
4 add:current add:shift 2 add:mark|add:current 6 add:done
5 add:current add:shift 2 add:mark|add:current 7 add:done
6 add:current add:shift 2 add:mark|add:current 8 add:done
7 add:current add:shift 2 add:mark|add:current 9 add:done
8 add:current add:shift 2 add:mark|add:current A add:done
9 add:current add:shift 2 add:mark|add:current B add:done
A add:current add:shift 2 add:mark|add:current C add:done
B add:current add:shift 2 add:mark|add:current D add:done
C add:current add:shift 2 add:mark|add:current E add:done
D add:current add:shift 2 add:mark|add:current F add:done
E add:current add:shift 2 add:mark|add:current G add:done
F add:current add:shift 2 add:mark|add:current H add:done
G add:current add:shift 2 add:mark|add:current I add:done
H add:current add:shift 2 add:mark|add:current J add:done
I add:current add:shift 2 add:mark|add:current K add:done
J add:current add:shift 2 add:mark|add:current L add:done
K add:current add:shift 2 add:mark|add:current M add:done
L add:current add:shift 2 add:mark|add:current N add:done
M add:current add:shift 2 add:mark|add:current O add:done
N add:current add:shift 2 add:mark|add:current P add:done
O add:current add:shift 2 add:mark|add:current Q add:done
P add:current add:shift 2 add:mark|add:current R add:done
Q add:current add:shift 2 add:mark|add:current S add:done
R add:current add:shift 2 add:mark|add:current T add:done
S add:current add:shift 2 add:mark|add:current U add:done
T add:current add:shift 2 add:mark|add:current V add:done
U add:current add:shift 2 add:mark|add:current W add:done
V add:current add:shift 2 add:mark|add:current X add:done
W add:current add:shift 2 add:mark|add:current Y add:done
X add:current add:shift 2 add:mark|add:current Z add:done

0 add:current add:shift 3 add:mark|add:current 3 add:done
1 add:current add:shift 3 add:mark|add:current 4 add:done
2 add:current add:shift 3 add:mark|add:current 5 add:done
3 add:current add:shift 3 add:mark|add:current 6 add:done
4 add:current add:shift 3 add:mark|add:current 7 add:done
5 add:current add:shift 3 add:mark|add:current 8 add:done
6 add:current add:shift 3 add:mark|add:current 9 add:done
7 add:current add:shift 3 add:mark|add:current A add:done
8 add:current add:shift 3 add:mark|add:current B add:done
9 add:current add:shift 3 add:mark|add:current C add:done
A add:current add:shift 3 add:mark|add:current D add:done
B add:current add:shift 3 add:mark|add:current E add:done
C add:current add:shift 3 add:mark|add:current F add:done
D add:current add:shift 3 add:mark|add:current G add:done
E add:current add:shift 3 add:mark|add:current H add:done
F add:current add:shift 3 add:mark|add:current I add:done
G add:current add:shift 3 add:mark|add:current J add:done
H add:current add:shift 3 add:mark|add:current K add:done
I add:current add:shift 3 add:mark|add:current L add:done
J add:current add:shift 3 add:mark|add:current M add:done
K add:current add:shift 3 add:mark|add:current N add:done
L add:current add:shift 3 add:mark|add:current O add:done
M add:current add:shift 3 add:mark|add:current P add:done
N add:current add:shift 3 add:mark|add:current Q add:done
O add:current add:shift 3 add:mark|add:current R add:done
P add:current add:shift 3 add:mark|add:current S add:done
Q add:current add:shift 3 add:mark|add:current T add:done
R add:current add:shift 3 add:mark|add:current U add:done
S add:current add:shift 3 add:mark|add:current V add:done
T add:current add:shift 3 add:mark|add:current W add:done
U add:current add:shift 3 add:mark|add:current X add:done
V add:current add:shift 3 add:mark|add:current Y add:done
W add:current add:shift 3 add:mark|add:current Z add:done

0 add:current add:shift 4 add:mark|add:current 4 add:done
1 add:current add:shift 4 add:mark|add:current 5 add:done
2 add:current add:shift 4 add:mark|add:current 6 add:done
3 add:current add:shift 4 add:mark|add:current 7 add:done
4 add:current add:shift 4 add:mark|add:current 8 add:done
5 add:current add:shift 4 add:mark|add:current 9 add:done
6 add:current add:shift 4 add:mark|add:current A add:done
7 add:current add:shift 4 add:mark|add:current B add:done
8 add:current add:shift 4 add:mark|add:current C add:done
9 add:current add:shift 4 add:mark|add:current D add:done
A add:current add:shift 4 add:mark|add:current E add:done
B add:current add:shift 4 add:mark|add:current F add:done
C add:current add:shift 4 add:mark|add:current G add:done
D add:current add:shift 4 add:mark|add:current H add:done
E add:current add:shift 4 add:mark|add:current I add:done
F add:current add:shift 4 add:mark|add:current J add:done
G add:current add:shift 4 add:mark|add:current K add:done
H add:current add:shift 4 add:mark|add:current L add:done
I add:current add:shift 4 add:mark|add:current M add:done
J add:current add:shift 4 add:mark|add:current N add:done
K add:current add:shift 4 add:mark|add:current O add:done
L add:current add:shift 4 add:mark|add:current P add:done
M add:current add:shift 4 add:mark|add:current Q add:done
N add:current add:shift 4 add:mark|add:current R add:done
O add:current add:shift 4 add:mark|add:current S add:done
P add:current add:shift 4 add:mark|add:current T add:done
Q add:current add:shift 4 add:mark|add:current U add:done
R add:current add:shift 4 add:mark|add:current V add:done
S add:current add:shift 4 add:mark|add:current W add:done
T add:current add:shift 4 add:mark|add:current X add:done
U add:current add:shift 4 add:mark|add:current Y add:done
V add:current add:shift 4 add:mark|add:current Z add:done

0 add:current add:shift 5 add:mark|add:current 5 add:done
1 add:current add:shift 5 add:mark|add:current 6 add:done
2 add:current add:shift 5 add:mark|add:current 7 add:done
3 add:current add:shift 5 add:mark|add:current 8 add:done
4 add:current add:shift 5 add:mark|add:current 9 add:done
5 add:current add:shift 5 add:mark|add:current A add:done
6 add:current add:shift 5 add:mark|add:current B add:done
7 add:current add:shift 5 add:mark|add:current C add:done
8 add:current add:shift 5 add:mark|add:current D add:done
9 add:current add:shift 5 add:mark|add:current E add:done
A add:current add:shift 5 add:mark|add:current F add:done
B add:current add:shift 5 add:mark|add:current G add:done
C add:current add:shift 5 add:mark|add:current H add:done
D add:current add:shift 5 add:mark|add:current I add:done
E add:current add:shift 5 add:mark|add:current J add:done
F add:current add:shift 5 add:mark|add:current K add:done
G add:current add:shift 5 add:mark|add:current L add:done
H add:current add:shift 5 add:mark|add:current M add:done
I add:current add:shift 5 add:mark|add:current N add:done
J add:current add:shift 5 add:mark|add:current O add:done
K add:current add:shift 5 add:mark|add:current P add:done
L add:current add:shift 5 add:mark|add:current Q add:done
M add:current add:shift 5 add:mark|add:current R add:done
N add:current add:shift 5 add:mark|add:current S add:done
O add:current add:shift 5 add:mark|add:current T add:done
P add:current add:shift 5 add:mark|add:current U add:done
Q add:current add:shift 5 add:mark|add:current V add:done
R add:current add:shift 5 add:mark|add:current W add:done
S add:current add:shift 5 add:mark|add:current X add:done
T add:current add:shift 5 add:mark|add:current Y add:done
U add:current add:shift 5 add:mark|add:current Z add:done

0 add:current add:shift 6 add:mark|add:current 6 add:done
1 add:current add:shift 6 add:mark|add:current 7 add:done
2 add:current add:shift 6 add:mark|add:current 8 add:done
3 add:current add:shift 6 add:mark|add:current 9 add:done
4 add:current add:shift 6 add:mark|add:current A add:done
5 add:current add:shift 6 add:mark|add:current B add:done
6 add:current add:shift 6 add:mark|add:current C add:done
7 add:current add:shift 6 add:mark|add:current D add:done
8 add:current add:shift 6 add:mark|add:current E add:done
9 add:current add:shift 6 add:mark|add:current F add:done
A add:current add:shift 6 add:mark|add:current G add:done
B add:current add:shift 6 add:mark|add:current H add:done
C add:current add:shift 6 add:mark|add:current I add:done
D add:current add:shift 6 add:mark|add:current J add:done
E add:current add:shift 6 add:mark|add:current K add:done
F add:current add:shift 6 add:mark|add:current L add:done
G add:current add:shift 6 add:mark|add:current M add:done
H add:current add:shift 6 add:mark|add:current N add:done
I add:current add:shift 6 add:mark|add:current O add:done
J add:current add:shift 6 add:mark|add:current P add:done
K add:current add:shift 6 add:mark|add:current Q add:done
L add:current add:shift 6 add:mark|add:current R add:done
M add:current add:shift 6 add:mark|add:current S add:done
N add:current add:shift 6 add:mark|add:current T add:done
O add:current add:shift 6 add:mark|add:current U add:done
P add:current add:shift 6 add:mark|add:current V add:done
Q add:current add:shift 6 add:mark|add:current W add:done
R add:current add:shift 6 add:mark|add:current X add:done
S add:current add:shift 6 add:mark|add:current Y add:done
T add:current add:shift 6 add:mark|add:current Z add:done

0 add:current add:shift 7 add:mark|add:current 7 add:done
1 add:current add:shift 7 add:mark|add:current 8 add:done
2 add:current add:shift 7 add:mark|add:current 9 add:done
3 add:current add:shift 7 add:mark|add:current A add:done
4 add:current add:shift 7 add:mark|add:current B add:done
5 add:current add:shift 7 add:mark|add:current C add:done
6 add:current add:shift 7 add:mark|add:current D add:done
7 add:current add:shift 7 add:mark|add:current E add:done
8 add:current add:shift 7 add:mark|add:current F add:done
9 add:current add:shift 7 add:mark|add:current G add:done
A add:current add:shift 7 add:mark|add:current H add:done
B add:current add:shift 7 add:mark|add:current I add:done
C add:current add:shift 7 add:mark|add:current J add:done
D add:current add:shift 7 add:mark|add:current K add:done
E add:current add:shift 7 add:mark|add:current L add:done
F add:current add:shift 7 add:mark|add:current M add:done
G add:current add:shift 7 add:mark|add:current N add:done
H add:current add:shift 7 add:mark|add:current O add:done
I add:current add:shift 7 add:mark|add:current P add:done
J add:current add:shift 7 add:mark|add:current Q add:done
K add:current add:shift 7 add:mark|add:current R add:done
L add:current add:shift 7 add:mark|add:current S add:done
M add:current add:shift 7 add:mark|add:current T add:done
N add:current add:shift 7 add:mark|add:current U add:done
O add:current add:shift 7 add:mark|add:current V add:done
P add:current add:shift 7 add:mark|add:current W add:done
Q add:current add:shift 7 add:mark|add:current X add:done
R add:current add:shift 7 add:mark|add:current Y add:done
S add:current add:shift 7 add:mark|add:current Z add:done

0 add:current add:shift 8 add:mark|add:current 8 add:done
1 add:current add:shift 8 add:mark|add:current 9 add:done
2 add:current add:shift 8 add:mark|add:current A add:done
3 add:current add:shift 8 add:mark|add:current B add:done
4 add:current add:shift 8 add:mark|add:current C add:done
5 add:current add:shift 8 add:mark|add:current D add:done
6 add:current add:shift 8 add:mark|add:current E add:done
7 add:current add:shift 8 add:mark|add:current F add:done
8 add:current add:shift 8 add:mark|add:current G add:done
9 add:current add:shift 8 add:mark|add:current H add:done
A add:current add:shift 8 add:mark|add:current I add:done
B add:current add:shift 8 add:mark|add:current J add:done
C add:current add:shift 8 add:mark|add:current K add:done
D add:current add:shift 8 add:mark|add:current L add:done
E add:current add:shift 8 add:mark|add:current M add:done
F add:current add:shift 8 add:mark|add:current N add:done
G add:current add:shift 8 add:mark|add:current O add:done
H add:current add:shift 8 add:mark|add:current P add:done
I add:current add:shift 8 add:mark|add:current Q add:done
J add:current add:shift 8 add:mark|add:current R add:done
K add:current add:shift 8 add:mark|add:current S add:done
L add:current add:shift 8 add:mark|add:current T add:done
M add:current add:shift 8 add:mark|add:current U add:done
N add:current add:shift 8 add:mark|add:current V add:done
O add:current add:shift 8 add:mark|add:current W add:done
P add:current add:shift 8 add:mark|add:current X add:done
Q add:current add:shift 8 add:mark|add:current Y add:done
R add:current add:shift 8 add:mark|add:current Z add:done

0 add:current add:shift 9 add:mark|add:current 9 add:done
1 add:current add:shift 9 add:mark|add:current A add:done
2 add:current add:shift 9 add:mark|add:current B add:done
3 add:current add:shift 9 add:mark|add:current C add:done
4 add:current add:shift 9 add:mark|add:current D add:done
5 add:current add:shift 9 add:mark|add:current E add:done
6 add:current add:shift 9 add:mark|add:current F add:done
7 add:current add:shift 9 add:mark|add:current G add:done
8 add:current add:shift 9 add:mark|add:current H add:done
9 add:current add:shift 9 add:mark|add:current I add:done
A add:current add:shift 9 add:mark|add:current J add:done
B add:current add:shift 9 add:mark|add:current K add:done
C add:current add:shift 9 add:mark|add:current L add:done
D add:current add:shift 9 add:mark|add:current M add:done
E add:current add:shift 9 add:mark|add:current N add:done
F add:current add:shift 9 add:mark|add:current O add:done
G add:current add:shift 9 add:mark|add:current P add:done
H add:current add:shift 9 add:mark|add:current Q add:done
I add:current add:shift 9 add:mark|add:current R add:done
J add:current add:shift 9 add:mark|add:current S add:done
K add:current add:shift 9 add:mark|add:current T add:done
L add:current add:shift 9 add:mark|add:current U add:done
M add:current add:shift 9 add:mark|add:current V add:done
N add:current add:shift 9 add:mark|add:current W add:done
O add:current add:shift 9 add:mark|add:current X add:done
P add:current add:shift 9 add:mark|add:current Y add:done
Q add:current add:shift 9 add:mark|add:current Z add:done

0 add:current add:shift A add:mark|add:current A add:done
1 add:current add:shift A add:mark|add:current B add:done
2 add:current add:shift A add:mark|add:current C add:done
3 add:current add:shift A add:mark|add:current D add:done
4 add:current add:shift A add:mark|add:current E add:done
5 add:current add:shift A add:mark|add:current F add:done
6 add:current add:shift A add:mark|add:current G add:done
7 add:current add:shift A add:mark|add:current H add:done
8 add:current add:shift A add:mark|add:current I add:done
9 add:current add:shift A add:mark|add:current J add:done
A add:current add:shift A add:mark|add:current K add:done
B add:current add:shift A add:mark|add:current L add:done
C add:current add:shift A add:mark|add:current M add:done
D add:current add:shift A add:mark|add:current N add:done
E add:current add:shift A add:mark|add:current O add:done
F add:current add:shift A add:mark|add:current P add:done
G add:current add:shift A add:mark|add:current Q add:done
H add:current add:shift A add:mark|add:current R add:done
I add:current add:shift A add:mark|add:current S add:done
J add:current add:shift A add:mark|add:current T add:done
K add:current add:shift A add:mark|add:current U add:done
L add:current add:shift A add:mark|add:current V add:done
M add:current add:shift A add:mark|add:current W add:done
N add:current add:shift A add:mark|add:current X add:done
O add:current add:shift A add:mark|add:current Y add:done
P add:current add:shift A add:mark|add:current Z add:done

0 add:current add:shift B add:mark|add:current B add:done
1 add:current add:shift B add:mark|add:current C add:done
2 add:current add:shift B add:mark|add:current D add:done
3 add:current add:shift B add:mark|add:current E add:done
4 add:current add:shift B add:mark|add:current F add:done
5 add:current add:shift B add:mark|add:current G add:done
6 add:current add:shift B add:mark|add:current H add:done
7 add:current add:shift B add:mark|add:current I add:done
8 add:current add:shift B add:mark|add:current J add:done
9 add:current add:shift B add:mark|add:current K add:done
A add:current add:shift B add:mark|add:current L add:done
B add:current add:shift B add:mark|add:current M add:done
C add:current add:shift B add:mark|add:current N add:done
D add:current add:shift B add:mark|add:current O add:done
E add:current add:shift B add:mark|add:current P add:done
F add:current add:shift B add:mark|add:current Q add:done
G add:current add:shift B add:mark|add:current R add:done
H add:current add:shift B add:mark|add:current S add:done
I add:current add:shift B add:mark|add:current T add:done
J add:current add:shift B add:mark|add:current U add:done
K add:current add:shift B add:mark|add:current V add:done
L add:current add:shift B add:mark|add:current W add:done
M add:current add:shift B add:mark|add:current X add:done
N add:current add:shift B add:mark|add:current Y add:done
O add:current add:shift B add:mark|add:current Z add:done

0 add:current add:shift C add:mark|add:current C add:done
1 add:current add:shift C add:mark|add:current D add:done
2 add:current add:shift C add:mark|add:current E add:done
3 add:current add:shift C add:mark|add:current F add:done
4 add:current add:shift C add:mark|add:current G add:done
5 add:current add:shift C add:mark|add:current H add:done
6 add:current add:shift C add:mark|add:current I add:done
7 add:current add:shift C add:mark|add:current J add:done
8 add:current add:shift C add:mark|add:current K add:done
9 add:current add:shift C add:mark|add:current L add:done
A add:current add:shift C add:mark|add:current M add:done
B add:current add:shift C add:mark|add:current N add:done
C add:current add:shift C add:mark|add:current O add:done
D add:current add:shift C add:mark|add:current P add:done
E add:current add:shift C add:mark|add:current Q add:done
F add:current add:shift C add:mark|add:current R add:done
G add:current add:shift C add:mark|add:current S add:done
H add:current add:shift C add:mark|add:current T add:done
I add:current add:shift C add:mark|add:current U add:done
J add:current add:shift C add:mark|add:current V add:done
K add:current add:shift C add:mark|add:current W add:done
L add:current add:shift C add:mark|add:current X add:done
M add:current add:shift C add:mark|add:current Y add:done
N add:current add:shift C add:mark|add:current Z add:done

0 add:current add:shift D add:mark|add:current D add:done
1 add:current add:shift D add:mark|add:current E add:done
2 add:current add:shift D add:mark|add:current F add:done
3 add:current add:shift D add:mark|add:current G add:done
4 add:current add:shift D add:mark|add:current H add:done
5 add:current add:shift D add:mark|add:current I add:done
6 add:current add:shift D add:mark|add:current J add:done
7 add:current add:shift D add:mark|add:current K add:done
8 add:current add:shift D add:mark|add:current L add:done
9 add:current add:shift D add:mark|add:current M add:done
A add:current add:shift D add:mark|add:current N add:done
B add:current add:shift D add:mark|add:current O add:done
C add:current add:shift D add:mark|add:current P add:done
D add:current add:shift D add:mark|add:current Q add:done
E add:current add:shift D add:mark|add:current R add:done
F add:current add:shift D add:mark|add:current S add:done
G add:current add:shift D add:mark|add:current T add:done
H add:current add:shift D add:mark|add:current U add:done
I add:current add:shift D add:mark|add:current V add:done
J add:current add:shift D add:mark|add:current W add:done
K add:current add:shift D add:mark|add:current X add:done
L add:current add:shift D add:mark|add:current Y add:done
M add:current add:shift D add:mark|add:current Z add:done

0 add:current add:shift E add:mark|add:current E add:done
1 add:current add:shift E add:mark|add:current F add:done
2 add:current add:shift E add:mark|add:current G add:done
3 add:current add:shift E add:mark|add:current H add:done
4 add:current add:shift E add:mark|add:current I add:done
5 add:current add:shift E add:mark|add:current J add:done
6 add:current add:shift E add:mark|add:current K add:done
7 add:current add:shift E add:mark|add:current L add:done
8 add:current add:shift E add:mark|add:current M add:done
9 add:current add:shift E add:mark|add:current N add:done
A add:current add:shift E add:mark|add:current O add:done
B add:current add:shift E add:mark|add:current P add:done
C add:current add:shift E add:mark|add:current Q add:done
D add:current add:shift E add:mark|add:current R add:done
E add:current add:shift E add:mark|add:current S add:done
F add:current add:shift E add:mark|add:current T add:done
G add:current add:shift E add:mark|add:current U add:done
H add:current add:shift E add:mark|add:current V add:done
I add:current add:shift E add:mark|add:current W add:done
J add:current add:shift E add:mark|add:current X add:done
K add:current add:shift E add:mark|add:current Y add:done
L add:current add:shift E add:mark|add:current Z add:done

0 add:current add:shift F add:mark|add:current F add:done
1 add:current add:shift F add:mark|add:current G add:done
2 add:current add:shift F add:mark|add:current H add:done
3 add:current add:shift F add:mark|add:current I add:done
4 add:current add:shift F add:mark|add:current J add:done
5 add:current add:shift F add:mark|add:current K add:done
6 add:current add:shift F add:mark|add:current L add:done
7 add:current add:shift F add:mark|add:current M add:done
8 add:current add:shift F add:mark|add:current N add:done
9 add:current add:shift F add:mark|add:current O add:done
A add:current add:shift F add:mark|add:current P add:done
B add:current add:shift F add:mark|add:current Q add:done
C add:current add:shift F add:mark|add:current R add:done
D add:current add:shift F add:mark|add:current S add:done
E add:current add:shift F add:mark|add:current T add:done
F add:current add:shift F add:mark|add:current U add:done
G add:current add:shift F add:mark|add:current V add:done
H add:current add:shift F add:mark|add:current W add:done
I add:current add:shift F add:mark|add:current X add:done
J add:current add:shift F add:mark|add:current Y add:done
K add:current add:shift F add:mark|add:current Z add:done

0 add:current add:shift G add:mark|add:current G add:done
1 add:current add:shift G add:mark|add:current H add:done
2 add:current add:shift G add:mark|add:current I add:done
3 add:current add:shift G add:mark|add:current J add:done
4 add:current add:shift G add:mark|add:current K add:done
5 add:current add:shift G add:mark|add:current L add:done
6 add:current add:shift G add:mark|add:current M add:done
7 add:current add:shift G add:mark|add:current N add:done
8 add:current add:shift G add:mark|add:current O add:done
9 add:current add:shift G add:mark|add:current P add:done
A add:current add:shift G add:mark|add:current Q add:done
B add:current add:shift G add:mark|add:current R add:done
C add:current add:shift G add:mark|add:current S add:done
D add:current add:shift G add:mark|add:current T add:done
E add:current add:shift G add:mark|add:current U add:done
F add:current add:shift G add:mark|add:current V add:done
G add:current add:shift G add:mark|add:current W add:done
H add:current add:shift G add:mark|add:current X add:done
I add:current add:shift G add:mark|add:current Y add:done
J add:current add:shift G add:mark|add:current Z add:done

0 add:current add:shift H add:mark|add:current H add:done
1 add:current add:shift H add:mark|add:current I add:done
2 add:current add:shift H add:mark|add:current J add:done
3 add:current add:shift H add:mark|add:current K add:done
4 add:current add:shift H add:mark|add:current L add:done
5 add:current add:shift H add:mark|add:current M add:done
6 add:current add:shift H add:mark|add:current N add:done
7 add:current add:shift H add:mark|add:current O add:done
8 add:current add:shift H add:mark|add:current P add:done
9 add:current add:shift H add:mark|add:current Q add:done
A add:current add:shift H add:mark|add:current R add:done
B add:current add:shift H add:mark|add:current S add:done
C add:current add:shift H add:mark|add:current T add:done
D add:current add:shift H add:mark|add:current U add:done
E add:current add:shift H add:mark|add:current V add:done
F add:current add:shift H add:mark|add:current W add:done
G add:current add:shift H add:mark|add:current X add:done
H add:current add:shift H add:mark|add:current Y add:done
I add:current add:shift H add:mark|add:current Z add:done

0 add:current add:shift I add:mark|add:current I add:done
1 add:current add:shift I add:mark|add:current J add:done
2 add:current add:shift I add:mark|add:current K add:done
3 add:current add:shift I add:mark|add:current L add:done
4 add:current add:shift I add:mark|add:current M add:done
5 add:current add:shift I add:mark|add:current N add:done
6 add:current add:shift I add:mark|add:current O add:done
7 add:current add:shift I add:mark|add:current P add:done
8 add:current add:shift I add:mark|add:current Q add:done
9 add:current add:shift I add:mark|add:current R add:done
A add:current add:shift I add:mark|add:current S add:done
B add:current add:shift I add:mark|add:current T add:done
C add:current add:shift I add:mark|add:current U add:done
D add:current add:shift I add:mark|add:current V add:done
E add:current add:shift I add:mark|add:current W add:done
F add:current add:shift I add:mark|add:current X add:done
G add:current add:shift I add:mark|add:current Y add:done
H add:current add:shift I add:mark|add:current Z add:done

0 add:current add:shift J add:mark|add:current J add:done
1 add:current add:shift J add:mark|add:current K add:done
2 add:current add:shift J add:mark|add:current L add:done
3 add:current add:shift J add:mark|add:current M add:done
4 add:current add:shift J add:mark|add:current N add:done
5 add:current add:shift J add:mark|add:current O add:done
6 add:current add:shift J add:mark|add:current P add:done
7 add:current add:shift J add:mark|add:current Q add:done
8 add:current add:shift J add:mark|add:current R add:done
9 add:current add:shift J add:mark|add:current S add:done
A add:current add:shift J add:mark|add:current T add:done
B add:current add:shift J add:mark|add:current U add:done
C add:current add:shift J add:mark|add:current V add:done
D add:current add:shift J add:mark|add:current W add:done
E add:current add:shift J add:mark|add:current X add:done
F add:current add:shift J add:mark|add:current Y add:done
G add:current add:shift J add:mark|add:current Z add:done

0 add:current add:shift K add:mark|add:current K add:done
1 add:current add:shift K add:mark|add:current L add:done
2 add:current add:shift K add:mark|add:current M add:done
3 add:current add:shift K add:mark|add:current N add:done
4 add:current add:shift K add:mark|add:current O add:done
5 add:current add:shift K add:mark|add:current P add:done
6 add:current add:shift K add:mark|add:current Q add:done
7 add:current add:shift K add:mark|add:current R add:done
8 add:current add:shift K add:mark|add:current S add:done
9 add:current add:shift K add:mark|add:current T add:done
A add:current add:shift K add:mark|add:current U add:done
B add:current add:shift K add:mark|add:current V add:done
C add:current add:shift K add:mark|add:current W add:done
D add:current add:shift K add:mark|add:current X add:done
E add:current add:shift K add:mark|add:current Y add:done
F add:current add:shift K add:mark|add:current Z add:done

0 add:current add:shift L add:mark|add:current L add:done
1 add:current add:shift L add:mark|add:current M add:done
2 add:current add:shift L add:mark|add:current N add:done
3 add:current add:shift L add:mark|add:current O add:done
4 add:current add:shift L add:mark|add:current P add:done
5 add:current add:shift L add:mark|add:current Q add:done
6 add:current add:shift L add:mark|add:current R add:done
7 add:current add:shift L add:mark|add:current S add:done
8 add:current add:shift L add:mark|add:current T add:done
9 add:current add:shift L add:mark|add:current U add:done
A add:current add:shift L add:mark|add:current V add:done
B add:current add:shift L add:mark|add:current W add:done
C add:current add:shift L add:mark|add:current X add:done
D add:current add:shift L add:mark|add:current Y add:done
E add:current add:shift L add:mark|add:current Z add:done

0 add:current add:shift M add:mark|add:current M add:done
1 add:current add:shift M add:mark|add:current N add:done
2 add:current add:shift M add:mark|add:current O add:done
3 add:current add:shift M add:mark|add:current P add:done
4 add:current add:shift M add:mark|add:current Q add:done
5 add:current add:shift M add:mark|add:current R add:done
6 add:current add:shift M add:mark|add:current S add:done
7 add:current add:shift M add:mark|add:current T add:done
8 add:current add:shift M add:mark|add:current U add:done
9 add:current add:shift M add:mark|add:current V add:done
A add:current add:shift M add:mark|add:current W add:done
B add:current add:shift M add:mark|add:current X add:done
C add:current add:shift M add:mark|add:current Y add:done
D add:current add:shift M add:mark|add:current Z add:done

0 add:current add:shift N add:mark|add:current N add:done
1 add:current add:shift N add:mark|add:current O add:done
2 add:current add:shift N add:mark|add:current P add:done
3 add:current add:shift N add:mark|add:current Q add:done
4 add:current add:shift N add:mark|add:current R add:done
5 add:current add:shift N add:mark|add:current S add:done
6 add:current add:shift N add:mark|add:current T add:done
7 add:current add:shift N add:mark|add:current U add:done
8 add:current add:shift N add:mark|add:current V add:done
9 add:current add:shift N add:mark|add:current W add:done
A add:current add:shift N add:mark|add:current X add:done
B add:current add:shift N add:mark|add:current Y add:done
C add:current add:shift N add:mark|add:current Z add:done

0 add:current add:shift O add:mark|add:current O add:done
1 add:current add:shift O add:mark|add:current P add:done
2 add:current add:shift O add:mark|add:current Q add:done
3 add:current add:shift O add:mark|add:current R add:done
4 add:current add:shift O add:mark|add:current S add:done
5 add:current add:shift O add:mark|add:current T add:done
6 add:current add:shift O add:mark|add:current U add:done
7 add:current add:shift O add:mark|add:current V add:done
8 add:current add:shift O add:mark|add:current W add:done
9 add:current add:shift O add:mark|add:current X add:done
A add:current add:shift O add:mark|add:current Y add:done
B add:current add:shift O add:mark|add:current Z add:done

0 add:current add:shift P add:mark|add:current P add:done
1 add:current add:shift P add:mark|add:current Q add:done
2 add:current add:shift P add:mark|add:current R add:done
3 add:current add:shift P add:mark|add:current S add:done
4 add:current add:shift P add:mark|add:current T add:done
5 add:current add:shift P add:mark|add:current U add:done
6 add:current add:shift P add:mark|add:current V add:done
7 add:current add:shift P add:mark|add:current W add:done
8 add:current add:shift P add:mark|add:current X add:done
9 add:current add:shift P add:mark|add:current Y add:done
A add:current add:shift P add:mark|add:current Z add:done

0 add:current add:shift Q add:mark|add:current Q add:done
1 add:current add:shift Q add:mark|add:current R add:done
2 add:current add:shift Q add:mark|add:current S add:done
3 add:current add:shift Q add:mark|add:current T add:done
4 add:current add:shift Q add:mark|add:current U add:done
5 add:current add:shift Q add:mark|add:current V add:done
6 add:current add:shift Q add:mark|add:current W add:done
7 add:current add:shift Q add:mark|add:current X add:done
8 add:current add:shift Q add:mark|add:current Y add:done
9 add:current add:shift Q add:mark|add:current Z add:done

0 add:current add:shift R add:mark|add:current R add:done
1 add:current add:shift R add:mark|add:current S add:done
2 add:current add:shift R add:mark|add:current T add:done
3 add:current add:shift R add:mark|add:current U add:done
4 add:current add:shift R add:mark|add:current V add:done
5 add:current add:shift R add:mark|add:current W add:done
6 add:current add:shift R add:mark|add:current X add:done
7 add:current add:shift R add:mark|add:current Y add:done
8 add:current add:shift R add:mark|add:current Z add:done

0 add:current add:shift S add:mark|add:current S add:done
1 add:current add:shift S add:mark|add:current T add:done
2 add:current add:shift S add:mark|add:current U add:done
3 add:current add:shift S add:mark|add:current V add:done
4 add:current add:shift S add:mark|add:current W add:done
5 add:current add:shift S add:mark|add:current X add:done
6 add:current add:shift S add:mark|add:current Y add:done
7 add:current add:shift S add:mark|add:current Z add:done

0 add:current add:shift T add:mark|add:current T add:done
1 add:current add:shift T add:mark|add:current U add:done
2 add:current add:shift T add:mark|add:current V add:done
3 add:current add:shift T add:mark|add:current W add:done
4 add:current add:shift T add:mark|add:current X add:done
5 add:current add:shift T add:mark|add:current Y add:done
6 add:current add:shift T add:mark|add:current Z add:done

0 add:current add:shift U add:mark|add:current U add:done
1 add:current add:shift U add:mark|add:current V add:done
2 add:current add:shift U add:mark|add:current W add:done
3 add:current add:shift U add:mark|add:current X add:done
4 add:current add:shift U add:mark|add:current Y add:done
5 add:current add:shift U add:mark|add:current Z add:done

0 add:current add:shift V add:mark|add:current V add:done
1 add:current add:shift V add:mark|add:current W add:done
2 add:current add:shift V add:mark|add:current X add:done
3 add:current add:shift V add:mark|add:current Y add:done
4 add:current add:shift V add:mark|add:current Z add:done

0 add:current add:shift W add:mark|add:current W add:done
1 add:current add:shift W add:mark|add:current X add:done
2 add:current add:shift W add:mark|add:current Y add:done
3 add:current add:shift W add:mark|add:current Z add:done

0 add:current add:shift X add:mark|add:current X add:done
1 add:current add:shift X add:mark|add:current Y add:done
2 add:current add:shift X add:mark|add:current Z add:done

0 add:current add:shift Y add:mark|add:current Y add:done
1 add:current add:shift Y add:mark|add:current Z add:done

0 add:current add:shift Z add:mark|add:current Z add:done


Z add:current add:shift 1 add:mark|1 add:mark add:current 0 add:done

Y add:current add:shift 2 add:mark|1 add:mark add:current 0 add:done
Z add:current add:shift 2 add:mark|1 add:mark add:current 1 add:done

X add:current add:shift 3 add:mark|1 add:mark add:current 0 add:done
Y add:current add:shift 3 add:mark|1 add:mark add:current 1 add:done
Z add:current add:shift 3 add:mark|1 add:mark add:current 2 add:done

W add:current add:shift 4 add:mark|1 add:mark add:current 0 add:done
X add:current add:shift 4 add:mark|1 add:mark add:current 1 add:done
Y add:current add:shift 4 add:mark|1 add:mark add:current 2 add:done
Z add:current add:shift 4 add:mark|1 add:mark add:current 3 add:done

V add:current add:shift 5 add:mark|1 add:mark add:current 0 add:done
W add:current add:shift 5 add:mark|1 add:mark add:current 1 add:done
X add:current add:shift 5 add:mark|1 add:mark add:current 2 add:done
Y add:current add:shift 5 add:mark|1 add:mark add:current 3 add:done
Z add:current add:shift 5 add:mark|1 add:mark add:current 4 add:done

U add:current add:shift 6 add:mark|1 add:mark add:current 0 add:done
V add:current add:shift 6 add:mark|1 add:mark add:current 1 add:done
W add:current add:shift 6 add:mark|1 add:mark add:current 2 add:done
X add:current add:shift 6 add:mark|1 add:mark add:current 3 add:done
Y add:current add:shift 6 add:mark|1 add:mark add:current 4 add:done
Z add:current add:shift 6 add:mark|1 add:mark add:current 5 add:done

T add:current add:shift 7 add:mark|1 add:mark add:current 0 add:done
U add:current add:shift 7 add:mark|1 add:mark add:current 1 add:done
V add:current add:shift 7 add:mark|1 add:mark add:current 2 add:done
W add:current add:shift 7 add:mark|1 add:mark add:current 3 add:done
X add:current add:shift 7 add:mark|1 add:mark add:current 4 add:done
Y add:current add:shift 7 add:mark|1 add:mark add:current 5 add:done
Z add:current add:shift 7 add:mark|1 add:mark add:current 6 add:done

S add:current add:shift 8 add:mark|1 add:mark add:current 0 add:done
T add:current add:shift 8 add:mark|1 add:mark add:current 1 add:done
U add:current add:shift 8 add:mark|1 add:mark add:current 2 add:done
V add:current add:shift 8 add:mark|1 add:mark add:current 3 add:done
W add:current add:shift 8 add:mark|1 add:mark add:current 4 add:done
X add:current add:shift 8 add:mark|1 add:mark add:current 5 add:done
Y add:current add:shift 8 add:mark|1 add:mark add:current 6 add:done
Z add:current add:shift 8 add:mark|1 add:mark add:current 7 add:done

R add:current add:shift 9 add:mark|1 add:mark add:current 0 add:done
S add:current add:shift 9 add:mark|1 add:mark add:current 1 add:done
T add:current add:shift 9 add:mark|1 add:mark add:current 2 add:done
U add:current add:shift 9 add:mark|1 add:mark add:current 3 add:done
V add:current add:shift 9 add:mark|1 add:mark add:current 4 add:done
W add:current add:shift 9 add:mark|1 add:mark add:current 5 add:done
X add:current add:shift 9 add:mark|1 add:mark add:current 6 add:done
Y add:current add:shift 9 add:mark|1 add:mark add:current 7 add:done
Z add:current add:shift 9 add:mark|1 add:mark add:current 8 add:done

Q add:current add:shift A add:mark|1 add:mark add:current 0 add:done
R add:current add:shift A add:mark|1 add:mark add:current 1 add:done
S add:current add:shift A add:mark|1 add:mark add:current 2 add:done
T add:current add:shift A add:mark|1 add:mark add:current 3 add:done
U add:current add:shift A add:mark|1 add:mark add:current 4 add:done
V add:current add:shift A add:mark|1 add:mark add:current 5 add:done
W add:current add:shift A add:mark|1 add:mark add:current 6 add:done
X add:current add:shift A add:mark|1 add:mark add:current 7 add:done
Y add:current add:shift A add:mark|1 add:mark add:current 8 add:done
Z add:current add:shift A add:mark|1 add:mark add:current 9 add:done

P add:current add:shift B add:mark|1 add:mark add:current 0 add:done
Q add:current add:shift B add:mark|1 add:mark add:current 1 add:done
R add:current add:shift B add:mark|1 add:mark add:current 2 add:done
S add:current add:shift B add:mark|1 add:mark add:current 3 add:done
T add:current add:shift B add:mark|1 add:mark add:current 4 add:done
U add:current add:shift B add:mark|1 add:mark add:current 5 add:done
V add:current add:shift B add:mark|1 add:mark add:current 6 add:done
W add:current add:shift B add:mark|1 add:mark add:current 7 add:done
X add:current add:shift B add:mark|1 add:mark add:current 8 add:done
Y add:current add:shift B add:mark|1 add:mark add:current 9 add:done
Z add:current add:shift B add:mark|1 add:mark add:current A add:done

O add:current add:shift C add:mark|1 add:mark add:current 0 add:done
P add:current add:shift C add:mark|1 add:mark add:current 1 add:done
Q add:current add:shift C add:mark|1 add:mark add:current 2 add:done
R add:current add:shift C add:mark|1 add:mark add:current 3 add:done
S add:current add:shift C add:mark|1 add:mark add:current 4 add:done
T add:current add:shift C add:mark|1 add:mark add:current 5 add:done
U add:current add:shift C add:mark|1 add:mark add:current 6 add:done
V add:current add:shift C add:mark|1 add:mark add:current 7 add:done
W add:current add:shift C add:mark|1 add:mark add:current 8 add:done
X add:current add:shift C add:mark|1 add:mark add:current 9 add:done
Y add:current add:shift C add:mark|1 add:mark add:current A add:done
Z add:current add:shift C add:mark|1 add:mark add:current B add:done

N add:current add:shift D add:mark|1 add:mark add:current 0 add:done
O add:current add:shift D add:mark|1 add:mark add:current 1 add:done
P add:current add:shift D add:mark|1 add:mark add:current 2 add:done
Q add:current add:shift D add:mark|1 add:mark add:current 3 add:done
R add:current add:shift D add:mark|1 add:mark add:current 4 add:done
S add:current add:shift D add:mark|1 add:mark add:current 5 add:done
T add:current add:shift D add:mark|1 add:mark add:current 6 add:done
U add:current add:shift D add:mark|1 add:mark add:current 7 add:done
V add:current add:shift D add:mark|1 add:mark add:current 8 add:done
W add:current add:shift D add:mark|1 add:mark add:current 9 add:done
X add:current add:shift D add:mark|1 add:mark add:current A add:done
Y add:current add:shift D add:mark|1 add:mark add:current B add:done
Z add:current add:shift D add:mark|1 add:mark add:current C add:done

M add:current add:shift E add:mark|1 add:mark add:current 0 add:done
N add:current add:shift E add:mark|1 add:mark add:current 1 add:done
O add:current add:shift E add:mark|1 add:mark add:current 2 add:done
P add:current add:shift E add:mark|1 add:mark add:current 3 add:done
Q add:current add:shift E add:mark|1 add:mark add:current 4 add:done
R add:current add:shift E add:mark|1 add:mark add:current 5 add:done
S add:current add:shift E add:mark|1 add:mark add:current 6 add:done
T add:current add:shift E add:mark|1 add:mark add:current 7 add:done
U add:current add:shift E add:mark|1 add:mark add:current 8 add:done
V add:current add:shift E add:mark|1 add:mark add:current 9 add:done
W add:current add:shift E add:mark|1 add:mark add:current A add:done
X add:current add:shift E add:mark|1 add:mark add:current B add:done
Y add:current add:shift E add:mark|1 add:mark add:current C add:done
Z add:current add:shift E add:mark|1 add:mark add:current D add:done

L add:current add:shift F add:mark|1 add:mark add:current 0 add:done
M add:current add:shift F add:mark|1 add:mark add:current 1 add:done
N add:current add:shift F add:mark|1 add:mark add:current 2 add:done
O add:current add:shift F add:mark|1 add:mark add:current 3 add:done
P add:current add:shift F add:mark|1 add:mark add:current 4 add:done
Q add:current add:shift F add:mark|1 add:mark add:current 5 add:done
R add:current add:shift F add:mark|1 add:mark add:current 6 add:done
S add:current add:shift F add:mark|1 add:mark add:current 7 add:done
T add:current add:shift F add:mark|1 add:mark add:current 8 add:done
U add:current add:shift F add:mark|1 add:mark add:current 9 add:done
V add:current add:shift F add:mark|1 add:mark add:current A add:done
W add:current add:shift F add:mark|1 add:mark add:current B add:done
X add:current add:shift F add:mark|1 add:mark add:current C add:done
Y add:current add:shift F add:mark|1 add:mark add:current D add:done
Z add:current add:shift F add:mark|1 add:mark add:current E add:done

K add:current add:shift G add:mark|1 add:mark add:current 0 add:done
L add:current add:shift G add:mark|1 add:mark add:current 1 add:done
M add:current add:shift G add:mark|1 add:mark add:current 2 add:done
N add:current add:shift G add:mark|1 add:mark add:current 3 add:done
O add:current add:shift G add:mark|1 add:mark add:current 4 add:done
P add:current add:shift G add:mark|1 add:mark add:current 5 add:done
Q add:current add:shift G add:mark|1 add:mark add:current 6 add:done
R add:current add:shift G add:mark|1 add:mark add:current 7 add:done
S add:current add:shift G add:mark|1 add:mark add:current 8 add:done
T add:current add:shift G add:mark|1 add:mark add:current 9 add:done
U add:current add:shift G add:mark|1 add:mark add:current A add:done
V add:current add:shift G add:mark|1 add:mark add:current B add:done
W add:current add:shift G add:mark|1 add:mark add:current C add:done
X add:current add:shift G add:mark|1 add:mark add:current D add:done
Y add:current add:shift G add:mark|1 add:mark add:current E add:done
Z add:current add:shift G add:mark|1 add:mark add:current F add:done

J add:current add:shift H add:mark|1 add:mark add:current 0 add:done
K add:current add:shift H add:mark|1 add:mark add:current 1 add:done
L add:current add:shift H add:mark|1 add:mark add:current 2 add:done
M add:current add:shift H add:mark|1 add:mark add:current 3 add:done
N add:current add:shift H add:mark|1 add:mark add:current 4 add:done
O add:current add:shift H add:mark|1 add:mark add:current 5 add:done
P add:current add:shift H add:mark|1 add:mark add:current 6 add:done
Q add:current add:shift H add:mark|1 add:mark add:current 7 add:done
R add:current add:shift H add:mark|1 add:mark add:current 8 add:done
S add:current add:shift H add:mark|1 add:mark add:current 9 add:done
T add:current add:shift H add:mark|1 add:mark add:current A add:done
U add:current add:shift H add:mark|1 add:mark add:current B add:done
V add:current add:shift H add:mark|1 add:mark add:current C add:done
W add:current add:shift H add:mark|1 add:mark add:current D add:done
X add:current add:shift H add:mark|1 add:mark add:current E add:done
Y add:current add:shift H add:mark|1 add:mark add:current F add:done
Z add:current add:shift H add:mark|1 add:mark add:current G add:done

I add:current add:shift I add:mark|1 add:mark add:current 0 add:done
J add:current add:shift I add:mark|1 add:mark add:current 1 add:done
K add:current add:shift I add:mark|1 add:mark add:current 2 add:done
L add:current add:shift I add:mark|1 add:mark add:current 3 add:done
M add:current add:shift I add:mark|1 add:mark add:current 4 add:done
N add:current add:shift I add:mark|1 add:mark add:current 5 add:done
O add:current add:shift I add:mark|1 add:mark add:current 6 add:done
P add:current add:shift I add:mark|1 add:mark add:current 7 add:done
Q add:current add:shift I add:mark|1 add:mark add:current 8 add:done
R add:current add:shift I add:mark|1 add:mark add:current 9 add:done
S add:current add:shift I add:mark|1 add:mark add:current A add:done
T add:current add:shift I add:mark|1 add:mark add:current B add:done
U add:current add:shift I add:mark|1 add:mark add:current C add:done
V add:current add:shift I add:mark|1 add:mark add:current D add:done
W add:current add:shift I add:mark|1 add:mark add:current E add:done
X add:current add:shift I add:mark|1 add:mark add:current F add:done
Y add:current add:shift I add:mark|1 add:mark add:current G add:done
Z add:current add:shift I add:mark|1 add:mark add:current H add:done

H add:current add:shift J add:mark|1 add:mark add:current 0 add:done
I add:current add:shift J add:mark|1 add:mark add:current 1 add:done
J add:current add:shift J add:mark|1 add:mark add:current 2 add:done
K add:current add:shift J add:mark|1 add:mark add:current 3 add:done
L add:current add:shift J add:mark|1 add:mark add:current 4 add:done
M add:current add:shift J add:mark|1 add:mark add:current 5 add:done
N add:current add:shift J add:mark|1 add:mark add:current 6 add:done
O add:current add:shift J add:mark|1 add:mark add:current 7 add:done
P add:current add:shift J add:mark|1 add:mark add:current 8 add:done
Q add:current add:shift J add:mark|1 add:mark add:current 9 add:done
R add:current add:shift J add:mark|1 add:mark add:current A add:done
S add:current add:shift J add:mark|1 add:mark add:current B add:done
T add:current add:shift J add:mark|1 add:mark add:current C add:done
U add:current add:shift J add:mark|1 add:mark add:current D add:done
V add:current add:shift J add:mark|1 add:mark add:current E add:done
W add:current add:shift J add:mark|1 add:mark add:current F add:done
X add:current add:shift J add:mark|1 add:mark add:current G add:done
Y add:current add:shift J add:mark|1 add:mark add:current H add:done
Z add:current add:shift J add:mark|1 add:mark add:current I add:done

G add:current add:shift K add:mark|1 add:mark add:current 0 add:done
H add:current add:shift K add:mark|1 add:mark add:current 1 add:done
I add:current add:shift K add:mark|1 add:mark add:current 2 add:done
J add:current add:shift K add:mark|1 add:mark add:current 3 add:done
K add:current add:shift K add:mark|1 add:mark add:current 4 add:done
L add:current add:shift K add:mark|1 add:mark add:current 5 add:done
M add:current add:shift K add:mark|1 add:mark add:current 6 add:done
N add:current add:shift K add:mark|1 add:mark add:current 7 add:done
O add:current add:shift K add:mark|1 add:mark add:current 8 add:done
P add:current add:shift K add:mark|1 add:mark add:current 9 add:done
Q add:current add:shift K add:mark|1 add:mark add:current A add:done
R add:current add:shift K add:mark|1 add:mark add:current B add:done
S add:current add:shift K add:mark|1 add:mark add:current C add:done
T add:current add:shift K add:mark|1 add:mark add:current D add:done
U add:current add:shift K add:mark|1 add:mark add:current E add:done
V add:current add:shift K add:mark|1 add:mark add:current F add:done
W add:current add:shift K add:mark|1 add:mark add:current G add:done
X add:current add:shift K add:mark|1 add:mark add:current H add:done
Y add:current add:shift K add:mark|1 add:mark add:current I add:done
Z add:current add:shift K add:mark|1 add:mark add:current J add:done

F add:current add:shift L add:mark|1 add:mark add:current 0 add:done
G add:current add:shift L add:mark|1 add:mark add:current 1 add:done
H add:current add:shift L add:mark|1 add:mark add:current 2 add:done
I add:current add:shift L add:mark|1 add:mark add:current 3 add:done
J add:current add:shift L add:mark|1 add:mark add:current 4 add:done
K add:current add:shift L add:mark|1 add:mark add:current 5 add:done
L add:current add:shift L add:mark|1 add:mark add:current 6 add:done
M add:current add:shift L add:mark|1 add:mark add:current 7 add:done
N add:current add:shift L add:mark|1 add:mark add:current 8 add:done
O add:current add:shift L add:mark|1 add:mark add:current 9 add:done
P add:current add:shift L add:mark|1 add:mark add:current A add:done
Q add:current add:shift L add:mark|1 add:mark add:current B add:done
R add:current add:shift L add:mark|1 add:mark add:current C add:done
S add:current add:shift L add:mark|1 add:mark add:current D add:done
T add:current add:shift L add:mark|1 add:mark add:current E add:done
U add:current add:shift L add:mark|1 add:mark add:current F add:done
V add:current add:shift L add:mark|1 add:mark add:current G add:done
W add:current add:shift L add:mark|1 add:mark add:current H add:done
X add:current add:shift L add:mark|1 add:mark add:current I add:done
Y add:current add:shift L add:mark|1 add:mark add:current J add:done
Z add:current add:shift L add:mark|1 add:mark add:current K add:done

E add:current add:shift M add:mark|1 add:mark add:current 0 add:done
F add:current add:shift M add:mark|1 add:mark add:current 1 add:done
G add:current add:shift M add:mark|1 add:mark add:current 2 add:done
H add:current add:shift M add:mark|1 add:mark add:current 3 add:done
I add:current add:shift M add:mark|1 add:mark add:current 4 add:done
J add:current add:shift M add:mark|1 add:mark add:current 5 add:done
K add:current add:shift M add:mark|1 add:mark add:current 6 add:done
L add:current add:shift M add:mark|1 add:mark add:current 7 add:done
M add:current add:shift M add:mark|1 add:mark add:current 8 add:done
N add:current add:shift M add:mark|1 add:mark add:current 9 add:done
O add:current add:shift M add:mark|1 add:mark add:current A add:done
P add:current add:shift M add:mark|1 add:mark add:current B add:done
Q add:current add:shift M add:mark|1 add:mark add:current C add:done
R add:current add:shift M add:mark|1 add:mark add:current D add:done
S add:current add:shift M add:mark|1 add:mark add:current E add:done
T add:current add:shift M add:mark|1 add:mark add:current F add:done
U add:current add:shift M add:mark|1 add:mark add:current G add:done
V add:current add:shift M add:mark|1 add:mark add:current H add:done
W add:current add:shift M add:mark|1 add:mark add:current I add:done
X add:current add:shift M add:mark|1 add:mark add:current J add:done
Y add:current add:shift M add:mark|1 add:mark add:current K add:done
Z add:current add:shift M add:mark|1 add:mark add:current L add:done

D add:current add:shift N add:mark|1 add:mark add:current 0 add:done
E add:current add:shift N add:mark|1 add:mark add:current 1 add:done
F add:current add:shift N add:mark|1 add:mark add:current 2 add:done
G add:current add:shift N add:mark|1 add:mark add:current 3 add:done
H add:current add:shift N add:mark|1 add:mark add:current 4 add:done
I add:current add:shift N add:mark|1 add:mark add:current 5 add:done
J add:current add:shift N add:mark|1 add:mark add:current 6 add:done
K add:current add:shift N add:mark|1 add:mark add:current 7 add:done
L add:current add:shift N add:mark|1 add:mark add:current 8 add:done
M add:current add:shift N add:mark|1 add:mark add:current 9 add:done
N add:current add:shift N add:mark|1 add:mark add:current A add:done
O add:current add:shift N add:mark|1 add:mark add:current B add:done
P add:current add:shift N add:mark|1 add:mark add:current C add:done
Q add:current add:shift N add:mark|1 add:mark add:current D add:done
R add:current add:shift N add:mark|1 add:mark add:current E add:done
S add:current add:shift N add:mark|1 add:mark add:current F add:done
T add:current add:shift N add:mark|1 add:mark add:current G add:done
U add:current add:shift N add:mark|1 add:mark add:current H add:done
V add:current add:shift N add:mark|1 add:mark add:current I add:done
W add:current add:shift N add:mark|1 add:mark add:current J add:done
X add:current add:shift N add:mark|1 add:mark add:current K add:done
Y add:current add:shift N add:mark|1 add:mark add:current L add:done
Z add:current add:shift N add:mark|1 add:mark add:current M add:done

C add:current add:shift O add:mark|1 add:mark add:current 0 add:done
D add:current add:shift O add:mark|1 add:mark add:current 1 add:done
E add:current add:shift O add:mark|1 add:mark add:current 2 add:done
F add:current add:shift O add:mark|1 add:mark add:current 3 add:done
G add:current add:shift O add:mark|1 add:mark add:current 4 add:done
H add:current add:shift O add:mark|1 add:mark add:current 5 add:done
I add:current add:shift O add:mark|1 add:mark add:current 6 add:done
J add:current add:shift O add:mark|1 add:mark add:current 7 add:done
K add:current add:shift O add:mark|1 add:mark add:current 8 add:done
L add:current add:shift O add:mark|1 add:mark add:current 9 add:done
M add:current add:shift O add:mark|1 add:mark add:current A add:done
N add:current add:shift O add:mark|1 add:mark add:current B add:done
O add:current add:shift O add:mark|1 add:mark add:current C add:done
P add:current add:shift O add:mark|1 add:mark add:current D add:done
Q add:current add:shift O add:mark|1 add:mark add:current E add:done
R add:current add:shift O add:mark|1 add:mark add:current F add:done
S add:current add:shift O add:mark|1 add:mark add:current G add:done
T add:current add:shift O add:mark|1 add:mark add:current H add:done
U add:current add:shift O add:mark|1 add:mark add:current I add:done
V add:current add:shift O add:mark|1 add:mark add:current J add:done
W add:current add:shift O add:mark|1 add:mark add:current K add:done
X add:current add:shift O add:mark|1 add:mark add:current L add:done
Y add:current add:shift O add:mark|1 add:mark add:current M add:done
Z add:current add:shift O add:mark|1 add:mark add:current N add:done

B add:current add:shift P add:mark|1 add:mark add:current 0 add:done
C add:current add:shift P add:mark|1 add:mark add:current 1 add:done
D add:current add:shift P add:mark|1 add:mark add:current 2 add:done
E add:current add:shift P add:mark|1 add:mark add:current 3 add:done
F add:current add:shift P add:mark|1 add:mark add:current 4 add:done
G add:current add:shift P add:mark|1 add:mark add:current 5 add:done
H add:current add:shift P add:mark|1 add:mark add:current 6 add:done
I add:current add:shift P add:mark|1 add:mark add:current 7 add:done
J add:current add:shift P add:mark|1 add:mark add:current 8 add:done
K add:current add:shift P add:mark|1 add:mark add:current 9 add:done
L add:current add:shift P add:mark|1 add:mark add:current A add:done
M add:current add:shift P add:mark|1 add:mark add:current B add:done
N add:current add:shift P add:mark|1 add:mark add:current C add:done
O add:current add:shift P add:mark|1 add:mark add:current D add:done
P add:current add:shift P add:mark|1 add:mark add:current E add:done
Q add:current add:shift P add:mark|1 add:mark add:current F add:done
R add:current add:shift P add:mark|1 add:mark add:current G add:done
S add:current add:shift P add:mark|1 add:mark add:current H add:done
T add:current add:shift P add:mark|1 add:mark add:current I add:done
U add:current add:shift P add:mark|1 add:mark add:current J add:done
V add:current add:shift P add:mark|1 add:mark add:current K add:done
W add:current add:shift P add:mark|1 add:mark add:current L add:done
X add:current add:shift P add:mark|1 add:mark add:current M add:done
Y add:current add:shift P add:mark|1 add:mark add:current N add:done
Z add:current add:shift P add:mark|1 add:mark add:current O add:done

A add:current add:shift Q add:mark|1 add:mark add:current 0 add:done
B add:current add:shift Q add:mark|1 add:mark add:current 1 add:done
C add:current add:shift Q add:mark|1 add:mark add:current 2 add:done
D add:current add:shift Q add:mark|1 add:mark add:current 3 add:done
E add:current add:shift Q add:mark|1 add:mark add:current 4 add:done
F add:current add:shift Q add:mark|1 add:mark add:current 5 add:done
G add:current add:shift Q add:mark|1 add:mark add:current 6 add:done
H add:current add:shift Q add:mark|1 add:mark add:current 7 add:done
I add:current add:shift Q add:mark|1 add:mark add:current 8 add:done
J add:current add:shift Q add:mark|1 add:mark add:current 9 add:done
K add:current add:shift Q add:mark|1 add:mark add:current A add:done
L add:current add:shift Q add:mark|1 add:mark add:current B add:done
M add:current add:shift Q add:mark|1 add:mark add:current C add:done
N add:current add:shift Q add:mark|1 add:mark add:current D add:done
O add:current add:shift Q add:mark|1 add:mark add:current E add:done
P add:current add:shift Q add:mark|1 add:mark add:current F add:done
Q add:current add:shift Q add:mark|1 add:mark add:current G add:done
R add:current add:shift Q add:mark|1 add:mark add:current H add:done
S add:current add:shift Q add:mark|1 add:mark add:current I add:done
T add:current add:shift Q add:mark|1 add:mark add:current J add:done
U add:current add:shift Q add:mark|1 add:mark add:current K add:done
V add:current add:shift Q add:mark|1 add:mark add:current L add:done
W add:current add:shift Q add:mark|1 add:mark add:current M add:done
X add:current add:shift Q add:mark|1 add:mark add:current N add:done
Y add:current add:shift Q add:mark|1 add:mark add:current O add:done
Z add:current add:shift Q add:mark|1 add:mark add:current P add:done

9 add:current add:shift R add:mark|1 add:mark add:current 0 add:done
A add:current add:shift R add:mark|1 add:mark add:current 1 add:done
B add:current add:shift R add:mark|1 add:mark add:current 2 add:done
C add:current add:shift R add:mark|1 add:mark add:current 3 add:done
D add:current add:shift R add:mark|1 add:mark add:current 4 add:done
E add:current add:shift R add:mark|1 add:mark add:current 5 add:done
F add:current add:shift R add:mark|1 add:mark add:current 6 add:done
G add:current add:shift R add:mark|1 add:mark add:current 7 add:done
H add:current add:shift R add:mark|1 add:mark add:current 8 add:done
I add:current add:shift R add:mark|1 add:mark add:current 9 add:done
J add:current add:shift R add:mark|1 add:mark add:current A add:done
K add:current add:shift R add:mark|1 add:mark add:current B add:done
L add:current add:shift R add:mark|1 add:mark add:current C add:done
M add:current add:shift R add:mark|1 add:mark add:current D add:done
N add:current add:shift R add:mark|1 add:mark add:current E add:done
O add:current add:shift R add:mark|1 add:mark add:current F add:done
P add:current add:shift R add:mark|1 add:mark add:current G add:done
Q add:current add:shift R add:mark|1 add:mark add:current H add:done
R add:current add:shift R add:mark|1 add:mark add:current I add:done
S add:current add:shift R add:mark|1 add:mark add:current J add:done
T add:current add:shift R add:mark|1 add:mark add:current K add:done
U add:current add:shift R add:mark|1 add:mark add:current L add:done
V add:current add:shift R add:mark|1 add:mark add:current M add:done
W add:current add:shift R add:mark|1 add:mark add:current N add:done
X add:current add:shift R add:mark|1 add:mark add:current O add:done
Y add:current add:shift R add:mark|1 add:mark add:current P add:done
Z add:current add:shift R add:mark|1 add:mark add:current Q add:done

8 add:current add:shift S add:mark|1 add:mark add:current 0 add:done
9 add:current add:shift S add:mark|1 add:mark add:current 1 add:done
A add:current add:shift S add:mark|1 add:mark add:current 2 add:done
B add:current add:shift S add:mark|1 add:mark add:current 3 add:done
C add:current add:shift S add:mark|1 add:mark add:current 4 add:done
D add:current add:shift S add:mark|1 add:mark add:current 5 add:done
E add:current add:shift S add:mark|1 add:mark add:current 6 add:done
F add:current add:shift S add:mark|1 add:mark add:current 7 add:done
G add:current add:shift S add:mark|1 add:mark add:current 8 add:done
H add:current add:shift S add:mark|1 add:mark add:current 9 add:done
I add:current add:shift S add:mark|1 add:mark add:current A add:done
J add:current add:shift S add:mark|1 add:mark add:current B add:done
K add:current add:shift S add:mark|1 add:mark add:current C add:done
L add:current add:shift S add:mark|1 add:mark add:current D add:done
M add:current add:shift S add:mark|1 add:mark add:current E add:done
N add:current add:shift S add:mark|1 add:mark add:current F add:done
O add:current add:shift S add:mark|1 add:mark add:current G add:done
P add:current add:shift S add:mark|1 add:mark add:current H add:done
Q add:current add:shift S add:mark|1 add:mark add:current I add:done
R add:current add:shift S add:mark|1 add:mark add:current J add:done
S add:current add:shift S add:mark|1 add:mark add:current K add:done
T add:current add:shift S add:mark|1 add:mark add:current L add:done
U add:current add:shift S add:mark|1 add:mark add:current M add:done
V add:current add:shift S add:mark|1 add:mark add:current N add:done
W add:current add:shift S add:mark|1 add:mark add:current O add:done
X add:current add:shift S add:mark|1 add:mark add:current P add:done
Y add:current add:shift S add:mark|1 add:mark add:current Q add:done
Z add:current add:shift S add:mark|1 add:mark add:current R add:done

7 add:current add:shift T add:mark|1 add:mark add:current 0 add:done
8 add:current add:shift T add:mark|1 add:mark add:current 1 add:done
9 add:current add:shift T add:mark|1 add:mark add:current 2 add:done
A add:current add:shift T add:mark|1 add:mark add:current 3 add:done
B add:current add:shift T add:mark|1 add:mark add:current 4 add:done
C add:current add:shift T add:mark|1 add:mark add:current 5 add:done
D add:current add:shift T add:mark|1 add:mark add:current 6 add:done
E add:current add:shift T add:mark|1 add:mark add:current 7 add:done
F add:current add:shift T add:mark|1 add:mark add:current 8 add:done
G add:current add:shift T add:mark|1 add:mark add:current 9 add:done
H add:current add:shift T add:mark|1 add:mark add:current A add:done
I add:current add:shift T add:mark|1 add:mark add:current B add:done
J add:current add:shift T add:mark|1 add:mark add:current C add:done
K add:current add:shift T add:mark|1 add:mark add:current D add:done
L add:current add:shift T add:mark|1 add:mark add:current E add:done
M add:current add:shift T add:mark|1 add:mark add:current F add:done
N add:current add:shift T add:mark|1 add:mark add:current G add:done
O add:current add:shift T add:mark|1 add:mark add:current H add:done
P add:current add:shift T add:mark|1 add:mark add:current I add:done
Q add:current add:shift T add:mark|1 add:mark add:current J add:done
R add:current add:shift T add:mark|1 add:mark add:current K add:done
S add:current add:shift T add:mark|1 add:mark add:current L add:done
T add:current add:shift T add:mark|1 add:mark add:current M add:done
U add:current add:shift T add:mark|1 add:mark add:current N add:done
V add:current add:shift T add:mark|1 add:mark add:current O add:done
W add:current add:shift T add:mark|1 add:mark add:current P add:done
X add:current add:shift T add:mark|1 add:mark add:current Q add:done
Y add:current add:shift T add:mark|1 add:mark add:current R add:done
Z add:current add:shift T add:mark|1 add:mark add:current S add:done

6 add:current add:shift U add:mark|1 add:mark add:current 0 add:done
7 add:current add:shift U add:mark|1 add:mark add:current 1 add:done
8 add:current add:shift U add:mark|1 add:mark add:current 2 add:done
9 add:current add:shift U add:mark|1 add:mark add:current 3 add:done
A add:current add:shift U add:mark|1 add:mark add:current 4 add:done
B add:current add:shift U add:mark|1 add:mark add:current 5 add:done
C add:current add:shift U add:mark|1 add:mark add:current 6 add:done
D add:current add:shift U add:mark|1 add:mark add:current 7 add:done
E add:current add:shift U add:mark|1 add:mark add:current 8 add:done
F add:current add:shift U add:mark|1 add:mark add:current 9 add:done
G add:current add:shift U add:mark|1 add:mark add:current A add:done
H add:current add:shift U add:mark|1 add:mark add:current B add:done
I add:current add:shift U add:mark|1 add:mark add:current C add:done
J add:current add:shift U add:mark|1 add:mark add:current D add:done
K add:current add:shift U add:mark|1 add:mark add:current E add:done
L add:current add:shift U add:mark|1 add:mark add:current F add:done
M add:current add:shift U add:mark|1 add:mark add:current G add:done
N add:current add:shift U add:mark|1 add:mark add:current H add:done
O add:current add:shift U add:mark|1 add:mark add:current I add:done
P add:current add:shift U add:mark|1 add:mark add:current J add:done
Q add:current add:shift U add:mark|1 add:mark add:current K add:done
R add:current add:shift U add:mark|1 add:mark add:current L add:done
S add:current add:shift U add:mark|1 add:mark add:current M add:done
T add:current add:shift U add:mark|1 add:mark add:current N add:done
U add:current add:shift U add:mark|1 add:mark add:current O add:done
V add:current add:shift U add:mark|1 add:mark add:current P add:done
W add:current add:shift U add:mark|1 add:mark add:current Q add:done
X add:current add:shift U add:mark|1 add:mark add:current R add:done
Y add:current add:shift U add:mark|1 add:mark add:current S add:done
Z add:current add:shift U add:mark|1 add:mark add:current T add:done

5 add:current add:shift V add:mark|1 add:mark add:current 0 add:done
6 add:current add:shift V add:mark|1 add:mark add:current 1 add:done
7 add:current add:shift V add:mark|1 add:mark add:current 2 add:done
8 add:current add:shift V add:mark|1 add:mark add:current 3 add:done
9 add:current add:shift V add:mark|1 add:mark add:current 4 add:done
A add:current add:shift V add:mark|1 add:mark add:current 5 add:done
B add:current add:shift V add:mark|1 add:mark add:current 6 add:done
C add:current add:shift V add:mark|1 add:mark add:current 7 add:done
D add:current add:shift V add:mark|1 add:mark add:current 8 add:done
E add:current add:shift V add:mark|1 add:mark add:current 9 add:done
F add:current add:shift V add:mark|1 add:mark add:current A add:done
G add:current add:shift V add:mark|1 add:mark add:current B add:done
H add:current add:shift V add:mark|1 add:mark add:current C add:done
I add:current add:shift V add:mark|1 add:mark add:current D add:done
J add:current add:shift V add:mark|1 add:mark add:current E add:done
K add:current add:shift V add:mark|1 add:mark add:current F add:done
L add:current add:shift V add:mark|1 add:mark add:current G add:done
M add:current add:shift V add:mark|1 add:mark add:current H add:done
N add:current add:shift V add:mark|1 add:mark add:current I add:done
O add:current add:shift V add:mark|1 add:mark add:current J add:done
P add:current add:shift V add:mark|1 add:mark add:current K add:done
Q add:current add:shift V add:mark|1 add:mark add:current L add:done
R add:current add:shift V add:mark|1 add:mark add:current M add:done
S add:current add:shift V add:mark|1 add:mark add:current N add:done
T add:current add:shift V add:mark|1 add:mark add:current O add:done
U add:current add:shift V add:mark|1 add:mark add:current P add:done
V add:current add:shift V add:mark|1 add:mark add:current Q add:done
W add:current add:shift V add:mark|1 add:mark add:current R add:done
X add:current add:shift V add:mark|1 add:mark add:current S add:done
Y add:current add:shift V add:mark|1 add:mark add:current T add:done
Z add:current add:shift V add:mark|1 add:mark add:current U add:done

4 add:current add:shift W add:mark|1 add:mark add:current 0 add:done
5 add:current add:shift W add:mark|1 add:mark add:current 1 add:done
6 add:current add:shift W add:mark|1 add:mark add:current 2 add:done
7 add:current add:shift W add:mark|1 add:mark add:current 3 add:done
8 add:current add:shift W add:mark|1 add:mark add:current 4 add:done
9 add:current add:shift W add:mark|1 add:mark add:current 5 add:done
A add:current add:shift W add:mark|1 add:mark add:current 6 add:done
B add:current add:shift W add:mark|1 add:mark add:current 7 add:done
C add:current add:shift W add:mark|1 add:mark add:current 8 add:done
D add:current add:shift W add:mark|1 add:mark add:current 9 add:done
E add:current add:shift W add:mark|1 add:mark add:current A add:done
F add:current add:shift W add:mark|1 add:mark add:current B add:done
G add:current add:shift W add:mark|1 add:mark add:current C add:done
H add:current add:shift W add:mark|1 add:mark add:current D add:done
I add:current add:shift W add:mark|1 add:mark add:current E add:done
J add:current add:shift W add:mark|1 add:mark add:current F add:done
K add:current add:shift W add:mark|1 add:mark add:current G add:done
L add:current add:shift W add:mark|1 add:mark add:current H add:done
M add:current add:shift W add:mark|1 add:mark add:current I add:done
N add:current add:shift W add:mark|1 add:mark add:current J add:done
O add:current add:shift W add:mark|1 add:mark add:current K add:done
P add:current add:shift W add:mark|1 add:mark add:current L add:done
Q add:current add:shift W add:mark|1 add:mark add:current M add:done
R add:current add:shift W add:mark|1 add:mark add:current N add:done
S add:current add:shift W add:mark|1 add:mark add:current O add:done
T add:current add:shift W add:mark|1 add:mark add:current P add:done
U add:current add:shift W add:mark|1 add:mark add:current Q add:done
V add:current add:shift W add:mark|1 add:mark add:current R add:done
W add:current add:shift W add:mark|1 add:mark add:current S add:done
X add:current add:shift W add:mark|1 add:mark add:current T add:done
Y add:current add:shift W add:mark|1 add:mark add:current U add:done
Z add:current add:shift W add:mark|1 add:mark add:current V add:done

3 add:current add:shift X add:mark|1 add:mark add:current 0 add:done
4 add:current add:shift X add:mark|1 add:mark add:current 1 add:done
5 add:current add:shift X add:mark|1 add:mark add:current 2 add:done
6 add:current add:shift X add:mark|1 add:mark add:current 3 add:done
7 add:current add:shift X add:mark|1 add:mark add:current 4 add:done
8 add:current add:shift X add:mark|1 add:mark add:current 5 add:done
9 add:current add:shift X add:mark|1 add:mark add:current 6 add:done
A add:current add:shift X add:mark|1 add:mark add:current 7 add:done
B add:current add:shift X add:mark|1 add:mark add:current 8 add:done
C add:current add:shift X add:mark|1 add:mark add:current 9 add:done
D add:current add:shift X add:mark|1 add:mark add:current A add:done
E add:current add:shift X add:mark|1 add:mark add:current B add:done
F add:current add:shift X add:mark|1 add:mark add:current C add:done
G add:current add:shift X add:mark|1 add:mark add:current D add:done
H add:current add:shift X add:mark|1 add:mark add:current E add:done
I add:current add:shift X add:mark|1 add:mark add:current F add:done
J add:current add:shift X add:mark|1 add:mark add:current G add:done
K add:current add:shift X add:mark|1 add:mark add:current H add:done
L add:current add:shift X add:mark|1 add:mark add:current I add:done
M add:current add:shift X add:mark|1 add:mark add:current J add:done
N add:current add:shift X add:mark|1 add:mark add:current K add:done
O add:current add:shift X add:mark|1 add:mark add:current L add:done
P add:current add:shift X add:mark|1 add:mark add:current M add:done
Q add:current add:shift X add:mark|1 add:mark add:current N add:done
R add:current add:shift X add:mark|1 add:mark add:current O add:done
S add:current add:shift X add:mark|1 add:mark add:current P add:done
T add:current add:shift X add:mark|1 add:mark add:current Q add:done
U add:current add:shift X add:mark|1 add:mark add:current R add:done
V add:current add:shift X add:mark|1 add:mark add:current S add:done
W add:current add:shift X add:mark|1 add:mark add:current T add:done
X add:current add:shift X add:mark|1 add:mark add:current U add:done
Y add:current add:shift X add:mark|1 add:mark add:current V add:done
Z add:current add:shift X add:mark|1 add:mark add:current W add:done

2 add:current add:shift Y add:mark|1 add:mark add:current 0 add:done
3 add:current add:shift Y add:mark|1 add:mark add:current 1 add:done
4 add:current add:shift Y add:mark|1 add:mark add:current 2 add:done
5 add:current add:shift Y add:mark|1 add:mark add:current 3 add:done
6 add:current add:shift Y add:mark|1 add:mark add:current 4 add:done
7 add:current add:shift Y add:mark|1 add:mark add:current 5 add:done
8 add:current add:shift Y add:mark|1 add:mark add:current 6 add:done
9 add:current add:shift Y add:mark|1 add:mark add:current 7 add:done
A add:current add:shift Y add:mark|1 add:mark add:current 8 add:done
B add:current add:shift Y add:mark|1 add:mark add:current 9 add:done
C add:current add:shift Y add:mark|1 add:mark add:current A add:done
D add:current add:shift Y add:mark|1 add:mark add:current B add:done
E add:current add:shift Y add:mark|1 add:mark add:current C add:done
F add:current add:shift Y add:mark|1 add:mark add:current D add:done
G add:current add:shift Y add:mark|1 add:mark add:current E add:done
H add:current add:shift Y add:mark|1 add:mark add:current F add:done
I add:current add:shift Y add:mark|1 add:mark add:current G add:done
J add:current add:shift Y add:mark|1 add:mark add:current H add:done
K add:current add:shift Y add:mark|1 add:mark add:current I add:done
L add:current add:shift Y add:mark|1 add:mark add:current J add:done
M add:current add:shift Y add:mark|1 add:mark add:current K add:done
N add:current add:shift Y add:mark|1 add:mark add:current L add:done
O add:current add:shift Y add:mark|1 add:mark add:current M add:done
P add:current add:shift Y add:mark|1 add:mark add:current N add:done
Q add:current add:shift Y add:mark|1 add:mark add:current O add:done
R add:current add:shift Y add:mark|1 add:mark add:current P add:done
S add:current add:shift Y add:mark|1 add:mark add:current Q add:done
T add:current add:shift Y add:mark|1 add:mark add:current R add:done
U add:current add:shift Y add:mark|1 add:mark add:current S add:done
V add:current add:shift Y add:mark|1 add:mark add:current T add:done
W add:current add:shift Y add:mark|1 add:mark add:current U add:done
X add:current add:shift Y add:mark|1 add:mark add:current V add:done
Y add:current add:shift Y add:mark|1 add:mark add:current W add:done
Z add:current add:shift Y add:mark|1 add:mark add:current X add:done

1 add:current add:shift Z add:mark|1 add:mark add:current 0 add:done
2 add:current add:shift Z add:mark|1 add:mark add:current 1 add:done
3 add:current add:shift Z add:mark|1 add:mark add:current 2 add:done
4 add:current add:shift Z add:mark|1 add:mark add:current 3 add:done
5 add:current add:shift Z add:mark|1 add:mark add:current 4 add:done
6 add:current add:shift Z add:mark|1 add:mark add:current 5 add:done
7 add:current add:shift Z add:mark|1 add:mark add:current 6 add:done
8 add:current add:shift Z add:mark|1 add:mark add:current 7 add:done
9 add:current add:shift Z add:mark|1 add:mark add:current 8 add:done
A add:current add:shift Z add:mark|1 add:mark add:current 9 add:done
B add:current add:shift Z add:mark|1 add:mark add:current A add:done
C add:current add:shift Z add:mark|1 add:mark add:current B add:done
D add:current add:shift Z add:mark|1 add:mark add:current C add:done
E add:current add:shift Z add:mark|1 add:mark add:current D add:done
F add:current add:shift Z add:mark|1 add:mark add:current E add:done
G add:current add:shift Z add:mark|1 add:mark add:current F add:done
H add:current add:shift Z add:mark|1 add:mark add:current G add:done
I add:current add:shift Z add:mark|1 add:mark add:current H add:done
J add:current add:shift Z add:mark|1 add:mark add:current I add:done
K add:current add:shift Z add:mark|1 add:mark add:current J add:done
L add:current add:shift Z add:mark|1 add:mark add:current K add:done
M add:current add:shift Z add:mark|1 add:mark add:current L add:done
N add:current add:shift Z add:mark|1 add:mark add:current M add:done
O add:current add:shift Z add:mark|1 add:mark add:current N add:done
P add:current add:shift Z add:mark|1 add:mark add:current O add:done
Q add:current add:shift Z add:mark|1 add:mark add:current P add:done
R add:current add:shift Z add:mark|1 add:mark add:current Q add:done
S add:current add:shift Z add:mark|1 add:mark add:current R add:done
T add:current add:shift Z add:mark|1 add:mark add:current S add:done
U add:current add:shift Z add:mark|1 add:mark add:current T add:done
V add:current add:shift Z add:mark|1 add:mark add:current U add:done
W add:current add:shift Z add:mark|1 add:mark add:current V add:done
X add:current add:shift Z add:mark|1 add:mark add:current W add:done
Y add:current add:shift Z add:mark|1 add:mark add:current X add:done
Z add:current add:shift Z add:mark|1 add:mark add:current Y add:done


== Apply carried addends
0 0 add:mark|0
1 0 add:mark|1
2 0 add:mark|2
3 0 add:mark|3
4 0 add:mark|4
5 0 add:mark|5
6 0 add:mark|6
7 0 add:mark|7
8 0 add:mark|8
9 0 add:mark|9
A 0 add:mark|A
B 0 add:mark|B
C 0 add:mark|C
D 0 add:mark|D
E 0 add:mark|E
F 0 add:mark|F
G 0 add:mark|G
H 0 add:mark|H
I 0 add:mark|I
J 0 add:mark|J
K 0 add:mark|K
L 0 add:mark|L
M 0 add:mark|M
N 0 add:mark|N
O 0 add:mark|O
P 0 add:mark|P
Q 0 add:mark|Q
R 0 add:mark|R
S 0 add:mark|S
T 0 add:mark|T
U 0 add:mark|U
V 0 add:mark|V
W 0 add:mark|W
X 0 add:mark|X
Y 0 add:mark|Y
Z 0 add:mark|Z

0 1 add:mark|1
1 1 add:mark|2
2 1 add:mark|3
3 1 add:mark|4
4 1 add:mark|5
5 1 add:mark|6
6 1 add:mark|7
7 1 add:mark|8
8 1 add:mark|9
9 1 add:mark|A
A 1 add:mark|B
B 1 add:mark|C
C 1 add:mark|D
D 1 add:mark|E
E 1 add:mark|F
F 1 add:mark|G
G 1 add:mark|H
H 1 add:mark|I
I 1 add:mark|J
J 1 add:mark|K
K 1 add:mark|L
L 1 add:mark|M
M 1 add:mark|N
N 1 add:mark|O
O 1 add:mark|P
P 1 add:mark|Q
Q 1 add:mark|R
R 1 add:mark|S
S 1 add:mark|T
T 1 add:mark|U
U 1 add:mark|V
V 1 add:mark|W
W 1 add:mark|X
X 1 add:mark|Y
Y 1 add:mark|Z

0 2 add:mark|2
1 2 add:mark|3
2 2 add:mark|4
3 2 add:mark|5
4 2 add:mark|6
5 2 add:mark|7
6 2 add:mark|8
7 2 add:mark|9
8 2 add:mark|A
9 2 add:mark|B
A 2 add:mark|C
B 2 add:mark|D
C 2 add:mark|E
D 2 add:mark|F
E 2 add:mark|G
F 2 add:mark|H
G 2 add:mark|I
H 2 add:mark|J
I 2 add:mark|K
J 2 add:mark|L
K 2 add:mark|M
L 2 add:mark|N
M 2 add:mark|O
N 2 add:mark|P
O 2 add:mark|Q
P 2 add:mark|R
Q 2 add:mark|S
R 2 add:mark|T
S 2 add:mark|U
T 2 add:mark|V
U 2 add:mark|W
V 2 add:mark|X
W 2 add:mark|Y
X 2 add:mark|Z

0 3 add:mark|3
1 3 add:mark|4
2 3 add:mark|5
3 3 add:mark|6
4 3 add:mark|7
5 3 add:mark|8
6 3 add:mark|9
7 3 add:mark|A
8 3 add:mark|B
9 3 add:mark|C
A 3 add:mark|D
B 3 add:mark|E
C 3 add:mark|F
D 3 add:mark|G
E 3 add:mark|H
F 3 add:mark|I
G 3 add:mark|J
H 3 add:mark|K
I 3 add:mark|L
J 3 add:mark|M
K 3 add:mark|N
L 3 add:mark|O
M 3 add:mark|P
N 3 add:mark|Q
O 3 add:mark|R
P 3 add:mark|S
Q 3 add:mark|T
R 3 add:mark|U
S 3 add:mark|V
T 3 add:mark|W
U 3 add:mark|X
V 3 add:mark|Y
W 3 add:mark|Z

0 4 add:mark|4
1 4 add:mark|5
2 4 add:mark|6
3 4 add:mark|7
4 4 add:mark|8
5 4 add:mark|9
6 4 add:mark|A
7 4 add:mark|B
8 4 add:mark|C
9 4 add:mark|D
A 4 add:mark|E
B 4 add:mark|F
C 4 add:mark|G
D 4 add:mark|H
E 4 add:mark|I
F 4 add:mark|J
G 4 add:mark|K
H 4 add:mark|L
I 4 add:mark|M
J 4 add:mark|N
K 4 add:mark|O
L 4 add:mark|P
M 4 add:mark|Q
N 4 add:mark|R
O 4 add:mark|S
P 4 add:mark|T
Q 4 add:mark|U
R 4 add:mark|V
S 4 add:mark|W
T 4 add:mark|X
U 4 add:mark|Y
V 4 add:mark|Z

0 5 add:mark|5
1 5 add:mark|6
2 5 add:mark|7
3 5 add:mark|8
4 5 add:mark|9
5 5 add:mark|A
6 5 add:mark|B
7 5 add:mark|C
8 5 add:mark|D
9 5 add:mark|E
A 5 add:mark|F
B 5 add:mark|G
C 5 add:mark|H
D 5 add:mark|I
E 5 add:mark|J
F 5 add:mark|K
G 5 add:mark|L
H 5 add:mark|M
I 5 add:mark|N
J 5 add:mark|O
K 5 add:mark|P
L 5 add:mark|Q
M 5 add:mark|R
N 5 add:mark|S
O 5 add:mark|T
P 5 add:mark|U
Q 5 add:mark|V
R 5 add:mark|W
S 5 add:mark|X
T 5 add:mark|Y
U 5 add:mark|Z

0 6 add:mark|6
1 6 add:mark|7
2 6 add:mark|8
3 6 add:mark|9
4 6 add:mark|A
5 6 add:mark|B
6 6 add:mark|C
7 6 add:mark|D
8 6 add:mark|E
9 6 add:mark|F
A 6 add:mark|G
B 6 add:mark|H
C 6 add:mark|I
D 6 add:mark|J
E 6 add:mark|K
F 6 add:mark|L
G 6 add:mark|M
H 6 add:mark|N
I 6 add:mark|O
J 6 add:mark|P
K 6 add:mark|Q
L 6 add:mark|R
M 6 add:mark|S
N 6 add:mark|T
O 6 add:mark|U
P 6 add:mark|V
Q 6 add:mark|W
R 6 add:mark|X
S 6 add:mark|Y
T 6 add:mark|Z

0 7 add:mark|7
1 7 add:mark|8
2 7 add:mark|9
3 7 add:mark|A
4 7 add:mark|B
5 7 add:mark|C
6 7 add:mark|D
7 7 add:mark|E
8 7 add:mark|F
9 7 add:mark|G
A 7 add:mark|H
B 7 add:mark|I
C 7 add:mark|J
D 7 add:mark|K
E 7 add:mark|L
F 7 add:mark|M
G 7 add:mark|N
H 7 add:mark|O
I 7 add:mark|P
J 7 add:mark|Q
K 7 add:mark|R
L 7 add:mark|S
M 7 add:mark|T
N 7 add:mark|U
O 7 add:mark|V
P 7 add:mark|W
Q 7 add:mark|X
R 7 add:mark|Y
S 7 add:mark|Z

0 8 add:mark|8
1 8 add:mark|9
2 8 add:mark|A
3 8 add:mark|B
4 8 add:mark|C
5 8 add:mark|D
6 8 add:mark|E
7 8 add:mark|F
8 8 add:mark|G
9 8 add:mark|H
A 8 add:mark|I
B 8 add:mark|J
C 8 add:mark|K
D 8 add:mark|L
E 8 add:mark|M
F 8 add:mark|N
G 8 add:mark|O
H 8 add:mark|P
I 8 add:mark|Q
J 8 add:mark|R
K 8 add:mark|S
L 8 add:mark|T
M 8 add:mark|U
N 8 add:mark|V
O 8 add:mark|W
P 8 add:mark|X
Q 8 add:mark|Y
R 8 add:mark|Z

0 9 add:mark|9
1 9 add:mark|A
2 9 add:mark|B
3 9 add:mark|C
4 9 add:mark|D
5 9 add:mark|E
6 9 add:mark|F
7 9 add:mark|G
8 9 add:mark|H
9 9 add:mark|I
A 9 add:mark|J
B 9 add:mark|K
C 9 add:mark|L
D 9 add:mark|M
E 9 add:mark|N
F 9 add:mark|O
G 9 add:mark|P
H 9 add:mark|Q
I 9 add:mark|R
J 9 add:mark|S
K 9 add:mark|T
L 9 add:mark|U
M 9 add:mark|V
N 9 add:mark|W
O 9 add:mark|X
P 9 add:mark|Y
Q 9 add:mark|Z

0 A add:mark|A
1 A add:mark|B
2 A add:mark|C
3 A add:mark|D
4 A add:mark|E
5 A add:mark|F
6 A add:mark|G
7 A add:mark|H
8 A add:mark|I
9 A add:mark|J
A A add:mark|K
B A add:mark|L
C A add:mark|M
D A add:mark|N
E A add:mark|O
F A add:mark|P
G A add:mark|Q
H A add:mark|R
I A add:mark|S
J A add:mark|T
K A add:mark|U
L A add:mark|V
M A add:mark|W
N A add:mark|X
O A add:mark|Y
P A add:mark|Z

0 B add:mark|B
1 B add:mark|C
2 B add:mark|D
3 B add:mark|E
4 B add:mark|F
5 B add:mark|G
6 B add:mark|H
7 B add:mark|I
8 B add:mark|J
9 B add:mark|K
A B add:mark|L
B B add:mark|M
C B add:mark|N
D B add:mark|O
E B add:mark|P
F B add:mark|Q
G B add:mark|R
H B add:mark|S
I B add:mark|T
J B add:mark|U
K B add:mark|V
L B add:mark|W
M B add:mark|X
N B add:mark|Y
O B add:mark|Z

0 C add:mark|C
1 C add:mark|D
2 C add:mark|E
3 C add:mark|F
4 C add:mark|G
5 C add:mark|H
6 C add:mark|I
7 C add:mark|J
8 C add:mark|K
9 C add:mark|L
A C add:mark|M
B C add:mark|N
C C add:mark|O
D C add:mark|P
E C add:mark|Q
F C add:mark|R
G C add:mark|S
H C add:mark|T
I C add:mark|U
J C add:mark|V
K C add:mark|W
L C add:mark|X
M C add:mark|Y
N C add:mark|Z

0 D add:mark|D
1 D add:mark|E
2 D add:mark|F
3 D add:mark|G
4 D add:mark|H
5 D add:mark|I
6 D add:mark|J
7 D add:mark|K
8 D add:mark|L
9 D add:mark|M
A D add:mark|N
B D add:mark|O
C D add:mark|P
D D add:mark|Q
E D add:mark|R
F D add:mark|S
G D add:mark|T
H D add:mark|U
I D add:mark|V
J D add:mark|W
K D add:mark|X
L D add:mark|Y
M D add:mark|Z

0 E add:mark|E
1 E add:mark|F
2 E add:mark|G
3 E add:mark|H
4 E add:mark|I
5 E add:mark|J
6 E add:mark|K
7 E add:mark|L
8 E add:mark|M
9 E add:mark|N
A E add:mark|O
B E add:mark|P
C E add:mark|Q
D E add:mark|R
E E add:mark|S
F E add:mark|T
G E add:mark|U
H E add:mark|V
I E add:mark|W
J E add:mark|X
K E add:mark|Y
L E add:mark|Z

0 F add:mark|F
1 F add:mark|G
2 F add:mark|H
3 F add:mark|I
4 F add:mark|J
5 F add:mark|K
6 F add:mark|L
7 F add:mark|M
8 F add:mark|N
9 F add:mark|O
A F add:mark|P
B F add:mark|Q
C F add:mark|R
D F add:mark|S
E F add:mark|T
F F add:mark|U
G F add:mark|V
H F add:mark|W
I F add:mark|X
J F add:mark|Y
K F add:mark|Z

0 G add:mark|G
1 G add:mark|H
2 G add:mark|I
3 G add:mark|J
4 G add:mark|K
5 G add:mark|L
6 G add:mark|M
7 G add:mark|N
8 G add:mark|O
9 G add:mark|P
A G add:mark|Q
B G add:mark|R
C G add:mark|S
D G add:mark|T
E G add:mark|U
F G add:mark|V
G G add:mark|W
H G add:mark|X
I G add:mark|Y
J G add:mark|Z

0 H add:mark|H
1 H add:mark|I
2 H add:mark|J
3 H add:mark|K
4 H add:mark|L
5 H add:mark|M
6 H add:mark|N
7 H add:mark|O
8 H add:mark|P
9 H add:mark|Q
A H add:mark|R
B H add:mark|S
C H add:mark|T
D H add:mark|U
E H add:mark|V
F H add:mark|W
G H add:mark|X
H H add:mark|Y
I H add:mark|Z

0 I add:mark|I
1 I add:mark|J
2 I add:mark|K
3 I add:mark|L
4 I add:mark|M
5 I add:mark|N
6 I add:mark|O
7 I add:mark|P
8 I add:mark|Q
9 I add:mark|R
A I add:mark|S
B I add:mark|T
C I add:mark|U
D I add:mark|V
E I add:mark|W
F I add:mark|X
G I add:mark|Y
H I add:mark|Z

0 J add:mark|J
1 J add:mark|K
2 J add:mark|L
3 J add:mark|M
4 J add:mark|N
5 J add:mark|O
6 J add:mark|P
7 J add:mark|Q
8 J add:mark|R
9 J add:mark|S
A J add:mark|T
B J add:mark|U
C J add:mark|V
D J add:mark|W
E J add:mark|X
F J add:mark|Y
G J add:mark|Z

0 K add:mark|K
1 K add:mark|L
2 K add:mark|M
3 K add:mark|N
4 K add:mark|O
5 K add:mark|P
6 K add:mark|Q
7 K add:mark|R
8 K add:mark|S
9 K add:mark|T
A K add:mark|U
B K add:mark|V
C K add:mark|W
D K add:mark|X
E K add:mark|Y
F K add:mark|Z

0 L add:mark|L
1 L add:mark|M
2 L add:mark|N
3 L add:mark|O
4 L add:mark|P
5 L add:mark|Q
6 L add:mark|R
7 L add:mark|S
8 L add:mark|T
9 L add:mark|U
A L add:mark|V
B L add:mark|W
C L add:mark|X
D L add:mark|Y
E L add:mark|Z

0 M add:mark|M
1 M add:mark|N
2 M add:mark|O
3 M add:mark|P
4 M add:mark|Q
5 M add:mark|R
6 M add:mark|S
7 M add:mark|T
8 M add:mark|U
9 M add:mark|V
A M add:mark|W
B M add:mark|X
C M add:mark|Y
D M add:mark|Z

0 N add:mark|N
1 N add:mark|O
2 N add:mark|P
3 N add:mark|Q
4 N add:mark|R
5 N add:mark|S
6 N add:mark|T
7 N add:mark|U
8 N add:mark|V
9 N add:mark|W
A N add:mark|X
B N add:mark|Y
C N add:mark|Z

0 O add:mark|O
1 O add:mark|P
2 O add:mark|Q
3 O add:mark|R
4 O add:mark|S
5 O add:mark|T
6 O add:mark|U
7 O add:mark|V
8 O add:mark|W
9 O add:mark|X
A O add:mark|Y
B O add:mark|Z

0 P add:mark|P
1 P add:mark|Q
2 P add:mark|R
3 P add:mark|S
4 P add:mark|T
5 P add:mark|U
6 P add:mark|V
7 P add:mark|W
8 P add:mark|X
9 P add:mark|Y
A P add:mark|Z

0 Q add:mark|Q
1 Q add:mark|R
2 Q add:mark|S
3 Q add:mark|T
4 Q add:mark|U
5 Q add:mark|V
6 Q add:mark|W
7 Q add:mark|X
8 Q add:mark|Y
9 Q add:mark|Z

0 R add:mark|R
1 R add:mark|S
2 R add:mark|T
3 R add:mark|U
4 R add:mark|V
5 R add:mark|W
6 R add:mark|X
7 R add:mark|Y
8 R add:mark|Z

0 S add:mark|S
1 S add:mark|T
2 S add:mark|U
3 S add:mark|V
4 S add:mark|W
5 S add:mark|X
6 S add:mark|Y
7 S add:mark|Z

0 T add:mark|T
1 T add:mark|U
2 T add:mark|V
3 T add:mark|W
4 T add:mark|X
5 T add:mark|Y
6 T add:mark|Z

0 U add:mark|U
1 U add:mark|V
2 U add:mark|W
3 U add:mark|X
4 U add:mark|Y
5 U add:mark|Z

0 V add:mark|V
1 V add:mark|W
2 V add:mark|X
3 V add:mark|Y
4 V add:mark|Z

0 W add:mark|W
1 W add:mark|X
2 W add:mark|Y
3 W add:mark|Z

0 X add:mark|X
1 X add:mark|Y
2 X add:mark|Z

0 Y add:mark|Y
1 Y add:mark|Z

0 Z add:mark|Z


Z 1 add:mark|1 add:mark 0

Y 2 add:mark|1 add:mark 0
Z 2 add:mark|1 add:mark 1

X 3 add:mark|1 add:mark 0
Y 3 add:mark|1 add:mark 1
Z 3 add:mark|1 add:mark 2

W 4 add:mark|1 add:mark 0
X 4 add:mark|1 add:mark 1
Y 4 add:mark|1 add:mark 2
Z 4 add:mark|1 add:mark 3

V 5 add:mark|1 add:mark 0
W 5 add:mark|1 add:mark 1
X 5 add:mark|1 add:mark 2
Y 5 add:mark|1 add:mark 3
Z 5 add:mark|1 add:mark 4

U 6 add:mark|1 add:mark 0
V 6 add:mark|1 add:mark 1
W 6 add:mark|1 add:mark 2
X 6 add:mark|1 add:mark 3
Y 6 add:mark|1 add:mark 4
Z 6 add:mark|1 add:mark 5

T 7 add:mark|1 add:mark 0
U 7 add:mark|1 add:mark 1
V 7 add:mark|1 add:mark 2
W 7 add:mark|1 add:mark 3
X 7 add:mark|1 add:mark 4
Y 7 add:mark|1 add:mark 5
Z 7 add:mark|1 add:mark 6

S 8 add:mark|1 add:mark 0
T 8 add:mark|1 add:mark 1
U 8 add:mark|1 add:mark 2
V 8 add:mark|1 add:mark 3
W 8 add:mark|1 add:mark 4
X 8 add:mark|1 add:mark 5
Y 8 add:mark|1 add:mark 6
Z 8 add:mark|1 add:mark 7

R 9 add:mark|1 add:mark 0
S 9 add:mark|1 add:mark 1
T 9 add:mark|1 add:mark 2
U 9 add:mark|1 add:mark 3
V 9 add:mark|1 add:mark 4
W 9 add:mark|1 add:mark 5
X 9 add:mark|1 add:mark 6
Y 9 add:mark|1 add:mark 7
Z 9 add:mark|1 add:mark 8

Q A add:mark|1 add:mark 0
R A add:mark|1 add:mark 1
S A add:mark|1 add:mark 2
T A add:mark|1 add:mark 3
U A add:mark|1 add:mark 4
V A add:mark|1 add:mark 5
W A add:mark|1 add:mark 6
X A add:mark|1 add:mark 7
Y A add:mark|1 add:mark 8
Z A add:mark|1 add:mark 9

P B add:mark|1 add:mark 0
Q B add:mark|1 add:mark 1
R B add:mark|1 add:mark 2
S B add:mark|1 add:mark 3
T B add:mark|1 add:mark 4
U B add:mark|1 add:mark 5
V B add:mark|1 add:mark 6
W B add:mark|1 add:mark 7
X B add:mark|1 add:mark 8
Y B add:mark|1 add:mark 9
Z B add:mark|1 add:mark A

O C add:mark|1 add:mark 0
P C add:mark|1 add:mark 1
Q C add:mark|1 add:mark 2
R C add:mark|1 add:mark 3
S C add:mark|1 add:mark 4
T C add:mark|1 add:mark 5
U C add:mark|1 add:mark 6
V C add:mark|1 add:mark 7
W C add:mark|1 add:mark 8
X C add:mark|1 add:mark 9
Y C add:mark|1 add:mark A
Z C add:mark|1 add:mark B

N D add:mark|1 add:mark 0
O D add:mark|1 add:mark 1
P D add:mark|1 add:mark 2
Q D add:mark|1 add:mark 3
R D add:mark|1 add:mark 4
S D add:mark|1 add:mark 5
T D add:mark|1 add:mark 6
U D add:mark|1 add:mark 7
V D add:mark|1 add:mark 8
W D add:mark|1 add:mark 9
X D add:mark|1 add:mark A
Y D add:mark|1 add:mark B
Z D add:mark|1 add:mark C

M E add:mark|1 add:mark 0
N E add:mark|1 add:mark 1
O E add:mark|1 add:mark 2
P E add:mark|1 add:mark 3
Q E add:mark|1 add:mark 4
R E add:mark|1 add:mark 5
S E add:mark|1 add:mark 6
T E add:mark|1 add:mark 7
U E add:mark|1 add:mark 8
V E add:mark|1 add:mark 9
W E add:mark|1 add:mark A
X E add:mark|1 add:mark B
Y E add:mark|1 add:mark C
Z E add:mark|1 add:mark D

L F add:mark|1 add:mark 0
M F add:mark|1 add:mark 1
N F add:mark|1 add:mark 2
O F add:mark|1 add:mark 3
P F add:mark|1 add:mark 4
Q F add:mark|1 add:mark 5
R F add:mark|1 add:mark 6
S F add:mark|1 add:mark 7
T F add:mark|1 add:mark 8
U F add:mark|1 add:mark 9
V F add:mark|1 add:mark A
W F add:mark|1 add:mark B
X F add:mark|1 add:mark C
Y F add:mark|1 add:mark D
Z F add:mark|1 add:mark E

K G add:mark|1 add:mark 0
L G add:mark|1 add:mark 1
M G add:mark|1 add:mark 2
N G add:mark|1 add:mark 3
O G add:mark|1 add:mark 4
P G add:mark|1 add:mark 5
Q G add:mark|1 add:mark 6
R G add:mark|1 add:mark 7
S G add:mark|1 add:mark 8
T G add:mark|1 add:mark 9
U G add:mark|1 add:mark A
V G add:mark|1 add:mark B
W G add:mark|1 add:mark C
X G add:mark|1 add:mark D
Y G add:mark|1 add:mark E
Z G add:mark|1 add:mark F

J H add:mark|1 add:mark 0
K H add:mark|1 add:mark 1
L H add:mark|1 add:mark 2
M H add:mark|1 add:mark 3
N H add:mark|1 add:mark 4
O H add:mark|1 add:mark 5
P H add:mark|1 add:mark 6
Q H add:mark|1 add:mark 7
R H add:mark|1 add:mark 8
S H add:mark|1 add:mark 9
T H add:mark|1 add:mark A
U H add:mark|1 add:mark B
V H add:mark|1 add:mark C
W H add:mark|1 add:mark D
X H add:mark|1 add:mark E
Y H add:mark|1 add:mark F
Z H add:mark|1 add:mark G

I I add:mark|1 add:mark 0
J I add:mark|1 add:mark 1
K I add:mark|1 add:mark 2
L I add:mark|1 add:mark 3
M I add:mark|1 add:mark 4
N I add:mark|1 add:mark 5
O I add:mark|1 add:mark 6
P I add:mark|1 add:mark 7
Q I add:mark|1 add:mark 8
R I add:mark|1 add:mark 9
S I add:mark|1 add:mark A
T I add:mark|1 add:mark B
U I add:mark|1 add:mark C
V I add:mark|1 add:mark D
W I add:mark|1 add:mark E
X I add:mark|1 add:mark F
Y I add:mark|1 add:mark G
Z I add:mark|1 add:mark H

H J add:mark|1 add:mark 0
I J add:mark|1 add:mark 1
J J add:mark|1 add:mark 2
K J add:mark|1 add:mark 3
L J add:mark|1 add:mark 4
M J add:mark|1 add:mark 5
N J add:mark|1 add:mark 6
O J add:mark|1 add:mark 7
P J add:mark|1 add:mark 8
Q J add:mark|1 add:mark 9
R J add:mark|1 add:mark A
S J add:mark|1 add:mark B
T J add:mark|1 add:mark C
U J add:mark|1 add:mark D
V J add:mark|1 add:mark E
W J add:mark|1 add:mark F
X J add:mark|1 add:mark G
Y J add:mark|1 add:mark H
Z J add:mark|1 add:mark I

G K add:mark|1 add:mark 0
H K add:mark|1 add:mark 1
I K add:mark|1 add:mark 2
J K add:mark|1 add:mark 3
K K add:mark|1 add:mark 4
L K add:mark|1 add:mark 5
M K add:mark|1 add:mark 6
N K add:mark|1 add:mark 7
O K add:mark|1 add:mark 8
P K add:mark|1 add:mark 9
Q K add:mark|1 add:mark A
R K add:mark|1 add:mark B
S K add:mark|1 add:mark C
T K add:mark|1 add:mark D
U K add:mark|1 add:mark E
V K add:mark|1 add:mark F
W K add:mark|1 add:mark G
X K add:mark|1 add:mark H
Y K add:mark|1 add:mark I
Z K add:mark|1 add:mark J

F L add:mark|1 add:mark 0
G L add:mark|1 add:mark 1
H L add:mark|1 add:mark 2
I L add:mark|1 add:mark 3
J L add:mark|1 add:mark 4
K L add:mark|1 add:mark 5
L L add:mark|1 add:mark 6
M L add:mark|1 add:mark 7
N L add:mark|1 add:mark 8
O L add:mark|1 add:mark 9
P L add:mark|1 add:mark A
Q L add:mark|1 add:mark B
R L add:mark|1 add:mark C
S L add:mark|1 add:mark D
T L add:mark|1 add:mark E
U L add:mark|1 add:mark F
V L add:mark|1 add:mark G
W L add:mark|1 add:mark H
X L add:mark|1 add:mark I
Y L add:mark|1 add:mark J
Z L add:mark|1 add:mark K

E M add:mark|1 add:mark 0
F M add:mark|1 add:mark 1
G M add:mark|1 add:mark 2
H M add:mark|1 add:mark 3
I M add:mark|1 add:mark 4
J M add:mark|1 add:mark 5
K M add:mark|1 add:mark 6
L M add:mark|1 add:mark 7
M M add:mark|1 add:mark 8
N M add:mark|1 add:mark 9
O M add:mark|1 add:mark A
P M add:mark|1 add:mark B
Q M add:mark|1 add:mark C
R M add:mark|1 add:mark D
S M add:mark|1 add:mark E
T M add:mark|1 add:mark F
U M add:mark|1 add:mark G
V M add:mark|1 add:mark H
W M add:mark|1 add:mark I
X M add:mark|1 add:mark J
Y M add:mark|1 add:mark K
Z M add:mark|1 add:mark L

D N add:mark|1 add:mark 0
E N add:mark|1 add:mark 1
F N add:mark|1 add:mark 2
G N add:mark|1 add:mark 3
H N add:mark|1 add:mark 4
I N add:mark|1 add:mark 5
J N add:mark|1 add:mark 6
K N add:mark|1 add:mark 7
L N add:mark|1 add:mark 8
M N add:mark|1 add:mark 9
N N add:mark|1 add:mark A
O N add:mark|1 add:mark B
P N add:mark|1 add:mark C
Q N add:mark|1 add:mark D
R N add:mark|1 add:mark E
S N add:mark|1 add:mark F
T N add:mark|1 add:mark G
U N add:mark|1 add:mark H
V N add:mark|1 add:mark I
W N add:mark|1 add:mark J
X N add:mark|1 add:mark K
Y N add:mark|1 add:mark L
Z N add:mark|1 add:mark M

C O add:mark|1 add:mark 0
D O add:mark|1 add:mark 1
E O add:mark|1 add:mark 2
F O add:mark|1 add:mark 3
G O add:mark|1 add:mark 4
H O add:mark|1 add:mark 5
I O add:mark|1 add:mark 6
J O add:mark|1 add:mark 7
K O add:mark|1 add:mark 8
L O add:mark|1 add:mark 9
M O add:mark|1 add:mark A
N O add:mark|1 add:mark B
O O add:mark|1 add:mark C
P O add:mark|1 add:mark D
Q O add:mark|1 add:mark E
R O add:mark|1 add:mark F
S O add:mark|1 add:mark G
T O add:mark|1 add:mark H
U O add:mark|1 add:mark I
V O add:mark|1 add:mark J
W O add:mark|1 add:mark K
X O add:mark|1 add:mark L
Y O add:mark|1 add:mark M
Z O add:mark|1 add:mark N

B P add:mark|1 add:mark 0
C P add:mark|1 add:mark 1
D P add:mark|1 add:mark 2
E P add:mark|1 add:mark 3
F P add:mark|1 add:mark 4
G P add:mark|1 add:mark 5
H P add:mark|1 add:mark 6
I P add:mark|1 add:mark 7
J P add:mark|1 add:mark 8
K P add:mark|1 add:mark 9
L P add:mark|1 add:mark A
M P add:mark|1 add:mark B
N P add:mark|1 add:mark C
O P add:mark|1 add:mark D
P P add:mark|1 add:mark E
Q P add:mark|1 add:mark F
R P add:mark|1 add:mark G
S P add:mark|1 add:mark H
T P add:mark|1 add:mark I
U P add:mark|1 add:mark J
V P add:mark|1 add:mark K
W P add:mark|1 add:mark L
X P add:mark|1 add:mark M
Y P add:mark|1 add:mark N
Z P add:mark|1 add:mark O

A Q add:mark|1 add:mark 0
B Q add:mark|1 add:mark 1
C Q add:mark|1 add:mark 2
D Q add:mark|1 add:mark 3
E Q add:mark|1 add:mark 4
F Q add:mark|1 add:mark 5
G Q add:mark|1 add:mark 6
H Q add:mark|1 add:mark 7
I Q add:mark|1 add:mark 8
J Q add:mark|1 add:mark 9
K Q add:mark|1 add:mark A
L Q add:mark|1 add:mark B
M Q add:mark|1 add:mark C
N Q add:mark|1 add:mark D
O Q add:mark|1 add:mark E
P Q add:mark|1 add:mark F
Q Q add:mark|1 add:mark G
R Q add:mark|1 add:mark H
S Q add:mark|1 add:mark I
T Q add:mark|1 add:mark J
U Q add:mark|1 add:mark K
V Q add:mark|1 add:mark L
W Q add:mark|1 add:mark M
X Q add:mark|1 add:mark N
Y Q add:mark|1 add:mark O
Z Q add:mark|1 add:mark P

9 R add:mark|1 add:mark 0
A R add:mark|1 add:mark 1
B R add:mark|1 add:mark 2
C R add:mark|1 add:mark 3
D R add:mark|1 add:mark 4
E R add:mark|1 add:mark 5
F R add:mark|1 add:mark 6
G R add:mark|1 add:mark 7
H R add:mark|1 add:mark 8
I R add:mark|1 add:mark 9
J R add:mark|1 add:mark A
K R add:mark|1 add:mark B
L R add:mark|1 add:mark C
M R add:mark|1 add:mark D
N R add:mark|1 add:mark E
O R add:mark|1 add:mark F
P R add:mark|1 add:mark G
Q R add:mark|1 add:mark H
R R add:mark|1 add:mark I
S R add:mark|1 add:mark J
T R add:mark|1 add:mark K
U R add:mark|1 add:mark L
V R add:mark|1 add:mark M
W R add:mark|1 add:mark N
X R add:mark|1 add:mark O
Y R add:mark|1 add:mark P
Z R add:mark|1 add:mark Q

8 S add:mark|1 add:mark 0
9 S add:mark|1 add:mark 1
A S add:mark|1 add:mark 2
B S add:mark|1 add:mark 3
C S add:mark|1 add:mark 4
D S add:mark|1 add:mark 5
E S add:mark|1 add:mark 6
F S add:mark|1 add:mark 7
G S add:mark|1 add:mark 8
H S add:mark|1 add:mark 9
I S add:mark|1 add:mark A
J S add:mark|1 add:mark B
K S add:mark|1 add:mark C
L S add:mark|1 add:mark D
M S add:mark|1 add:mark E
N S add:mark|1 add:mark F
O S add:mark|1 add:mark G
P S add:mark|1 add:mark H
Q S add:mark|1 add:mark I
R S add:mark|1 add:mark J
S S add:mark|1 add:mark K
T S add:mark|1 add:mark L
U S add:mark|1 add:mark M
V S add:mark|1 add:mark N
W S add:mark|1 add:mark O
X S add:mark|1 add:mark P
Y S add:mark|1 add:mark Q
Z S add:mark|1 add:mark R

7 T add:mark|1 add:mark 0
8 T add:mark|1 add:mark 1
9 T add:mark|1 add:mark 2
A T add:mark|1 add:mark 3
B T add:mark|1 add:mark 4
C T add:mark|1 add:mark 5
D T add:mark|1 add:mark 6
E T add:mark|1 add:mark 7
F T add:mark|1 add:mark 8
G T add:mark|1 add:mark 9
H T add:mark|1 add:mark A
I T add:mark|1 add:mark B
J T add:mark|1 add:mark C
K T add:mark|1 add:mark D
L T add:mark|1 add:mark E
M T add:mark|1 add:mark F
N T add:mark|1 add:mark G
O T add:mark|1 add:mark H
P T add:mark|1 add:mark I
Q T add:mark|1 add:mark J
R T add:mark|1 add:mark K
S T add:mark|1 add:mark L
T T add:mark|1 add:mark M
U T add:mark|1 add:mark N
V T add:mark|1 add:mark O
W T add:mark|1 add:mark P
X T add:mark|1 add:mark Q
Y T add:mark|1 add:mark R
Z T add:mark|1 add:mark S

6 U add:mark|1 add:mark 0
7 U add:mark|1 add:mark 1
8 U add:mark|1 add:mark 2
9 U add:mark|1 add:mark 3
A U add:mark|1 add:mark 4
B U add:mark|1 add:mark 5
C U add:mark|1 add:mark 6
D U add:mark|1 add:mark 7
E U add:mark|1 add:mark 8
F U add:mark|1 add:mark 9
G U add:mark|1 add:mark A
H U add:mark|1 add:mark B
I U add:mark|1 add:mark C
J U add:mark|1 add:mark D
K U add:mark|1 add:mark E
L U add:mark|1 add:mark F
M U add:mark|1 add:mark G
N U add:mark|1 add:mark H
O U add:mark|1 add:mark I
P U add:mark|1 add:mark J
Q U add:mark|1 add:mark K
R U add:mark|1 add:mark L
S U add:mark|1 add:mark M
T U add:mark|1 add:mark N
U U add:mark|1 add:mark O
V U add:mark|1 add:mark P
W U add:mark|1 add:mark Q
X U add:mark|1 add:mark R
Y U add:mark|1 add:mark S
Z U add:mark|1 add:mark T

5 V add:mark|1 add:mark 0
6 V add:mark|1 add:mark 1
7 V add:mark|1 add:mark 2
8 V add:mark|1 add:mark 3
9 V add:mark|1 add:mark 4
A V add:mark|1 add:mark 5
B V add:mark|1 add:mark 6
C V add:mark|1 add:mark 7
D V add:mark|1 add:mark 8
E V add:mark|1 add:mark 9
F V add:mark|1 add:mark A
G V add:mark|1 add:mark B
H V add:mark|1 add:mark C
I V add:mark|1 add:mark D
J V add:mark|1 add:mark E
K V add:mark|1 add:mark F
L V add:mark|1 add:mark G
M V add:mark|1 add:mark H
N V add:mark|1 add:mark I
O V add:mark|1 add:mark J
P V add:mark|1 add:mark K
Q V add:mark|1 add:mark L
R V add:mark|1 add:mark M
S V add:mark|1 add:mark N
T V add:mark|1 add:mark O
U V add:mark|1 add:mark P
V V add:mark|1 add:mark Q
W V add:mark|1 add:mark R
X V add:mark|1 add:mark S
Y V add:mark|1 add:mark T
Z V add:mark|1 add:mark U

4 W add:mark|1 add:mark 0
5 W add:mark|1 add:mark 1
6 W add:mark|1 add:mark 2
7 W add:mark|1 add:mark 3
8 W add:mark|1 add:mark 4
9 W add:mark|1 add:mark 5
A W add:mark|1 add:mark 6
B W add:mark|1 add:mark 7
C W add:mark|1 add:mark 8
D W add:mark|1 add:mark 9
E W add:mark|1 add:mark A
F W add:mark|1 add:mark B
G W add:mark|1 add:mark C
H W add:mark|1 add:mark D
I W add:mark|1 add:mark E
J W add:mark|1 add:mark F
K W add:mark|1 add:mark G
L W add:mark|1 add:mark H
M W add:mark|1 add:mark I
N W add:mark|1 add:mark J
O W add:mark|1 add:mark K
P W add:mark|1 add:mark L
Q W add:mark|1 add:mark M
R W add:mark|1 add:mark N
S W add:mark|1 add:mark O
T W add:mark|1 add:mark P
U W add:mark|1 add:mark Q
V W add:mark|1 add:mark R
W W add:mark|1 add:mark S
X W add:mark|1 add:mark T
Y W add:mark|1 add:mark U
Z W add:mark|1 add:mark V

3 X add:mark|1 add:mark 0
4 X add:mark|1 add:mark 1
5 X add:mark|1 add:mark 2
6 X add:mark|1 add:mark 3
7 X add:mark|1 add:mark 4
8 X add:mark|1 add:mark 5
9 X add:mark|1 add:mark 6
A X add:mark|1 add:mark 7
B X add:mark|1 add:mark 8
C X add:mark|1 add:mark 9
D X add:mark|1 add:mark A
E X add:mark|1 add:mark B
F X add:mark|1 add:mark C
G X add:mark|1 add:mark D
H X add:mark|1 add:mark E
I X add:mark|1 add:mark F
J X add:mark|1 add:mark G
K X add:mark|1 add:mark H
L X add:mark|1 add:mark I
M X add:mark|1 add:mark J
N X add:mark|1 add:mark K
O X add:mark|1 add:mark L
P X add:mark|1 add:mark M
Q X add:mark|1 add:mark N
R X add:mark|1 add:mark O
S X add:mark|1 add:mark P
T X add:mark|1 add:mark Q
U X add:mark|1 add:mark R
V X add:mark|1 add:mark S
W X add:mark|1 add:mark T
X X add:mark|1 add:mark U
Y X add:mark|1 add:mark V
Z X add:mark|1 add:mark W

2 Y add:mark|1 add:mark 0
3 Y add:mark|1 add:mark 1
4 Y add:mark|1 add:mark 2
5 Y add:mark|1 add:mark 3
6 Y add:mark|1 add:mark 4
7 Y add:mark|1 add:mark 5
8 Y add:mark|1 add:mark 6
9 Y add:mark|1 add:mark 7
A Y add:mark|1 add:mark 8
B Y add:mark|1 add:mark 9
C Y add:mark|1 add:mark A
D Y add:mark|1 add:mark B
E Y add:mark|1 add:mark C
F Y add:mark|1 add:mark D
G Y add:mark|1 add:mark E
H Y add:mark|1 add:mark F
I Y add:mark|1 add:mark G
J Y add:mark|1 add:mark H
K Y add:mark|1 add:mark I
L Y add:mark|1 add:mark J
M Y add:mark|1 add:mark K
N Y add:mark|1 add:mark L
O Y add:mark|1 add:mark M
P Y add:mark|1 add:mark N
Q Y add:mark|1 add:mark O
R Y add:mark|1 add:mark P
S Y add:mark|1 add:mark Q
T Y add:mark|1 add:mark R
U Y add:mark|1 add:mark S
V Y add:mark|1 add:mark T
W Y add:mark|1 add:mark U
X Y add:mark|1 add:mark V
Y Y add:mark|1 add:mark W
Z Y add:mark|1 add:mark X

1 Z add:mark|1 add:mark 0
2 Z add:mark|1 add:mark 1
3 Z add:mark|1 add:mark 2
4 Z add:mark|1 add:mark 3
5 Z add:mark|1 add:mark 4
6 Z add:mark|1 add:mark 5
7 Z add:mark|1 add:mark 6
8 Z add:mark|1 add:mark 7
9 Z add:mark|1 add:mark 8
A Z add:mark|1 add:mark 9
B Z add:mark|1 add:mark A
C Z add:mark|1 add:mark B
D Z add:mark|1 add:mark C
E Z add:mark|1 add:mark D
F Z add:mark|1 add:mark E
G Z add:mark|1 add:mark F
H Z add:mark|1 add:mark G
I Z add:mark|1 add:mark H
J Z add:mark|1 add:mark I
K Z add:mark|1 add:mark J
L Z add:mark|1 add:mark K
M Z add:mark|1 add:mark L
N Z add:mark|1 add:mark M
O Z add:mark|1 add:mark N
P Z add:mark|1 add:mark O
Q Z add:mark|1 add:mark P
R Z add:mark|1 add:mark Q
S Z add:mark|1 add:mark R
T Z add:mark|1 add:mark S
U Z add:mark|1 add:mark T
V Z add:mark|1 add:mark U
W Z add:mark|1 add:mark V
X Z add:mark|1 add:mark W
Y Z add:mark|1 add:mark X
Z Z add:mark|1 add:mark Y


#
add:start Y 4 W X I 3 Y V 1 O Y V M M M 6 Y 9 J A 7 I F O J A A V U T W F U Z Z J S M S U 0 C 2 D Z 8 C U G 7 W K 5 J K 1 I 4 U 0 1 H 7 D U Q O T R Y B 4 W 3 3 0 4 Y D P A L C L G T 1 9 E 1 C S E B 4 K 6 A A V B Y U 4 O H 5 X A C 1 F V I X 9 H I 1 2 M 6 F 0 P Z E O R 7 J R A Y S 6 J Y O 8 D J B K W H 6 4 B Z 4 5 D 8 K X I 1 Q 9 C A I P 8 O 6 8 0 2 D B 5 B X G 4 Z 6 3 V O 7 D 9 L K C N J R 0 H F 3 A U D add:current Y L 9 2 G 4 4 S Q 7 R 8 U J 8 L R C 8 4 W 6 N E S 4 5 T F 3 Y X 5 9 I M 4 V B 7 A B L V C X M I M A 4 4 P X R 7 K C M D Y D 7 N 4 G X J O 9 T B D Y H B 1 X G 8 P I 3 3 Q 8 G Y E 3 A 0 H 3 E E U O G Q V E 3 O V W 5 V 1 0 V 4 T V T K 2 J 6 Y 6 K Q B N C Y K A H H P A 2 A H Q F Q A U I V L H J I B 8 4 6 V V Y W H N 4 Q 5 I D N W 4 S 8 0 E V T A 4 4 H R P J U F Q Z H 3 0 6 F J 9 K U 3 7 I E F 2 J add:cmd
