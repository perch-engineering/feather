package main

import (
	"errors"
	"feather/core"
	"feather/modules"
	"flag"
	"fmt"
	"io/ioutil"
	"strings"
	"time"
)

func CleanSplit(split []string) []string {
	results := []string{}
	for _, value := range split {
		if value != "" {
			results = append(results, value)
		}
	}
	return results
}

func ParseRules(rules string) core.Module {
	lines := strings.Split(rules, "\n")
	result := modules.CreateTrie(nil)
	for _, value := range lines {
		rule := strings.Split(value, "|")
		if len(rule) != 2 {
			continue
		}
		rule[0] = strings.TrimLeft(rule[0], " \t\n")
		rule[1] = strings.TrimLeft(rule[1], " \t\n")
		rule[0] = strings.TrimRight(rule[0], " \t\n")
		rule[1] = strings.TrimRight(rule[1], " \t\n")
		result.Insert(CleanSplit(strings.Split(rule[0], " ")), CleanSplit(strings.Split(rule[1], " ")))
	}
	return result
}

func ParseInitial(initial string) []string {
	lines := strings.Split(initial, "\n")
	results := []string{}
	for _, value := range lines {
		line := strings.TrimLeft(strings.TrimRight(value, " \t\n"), " \t\n")
		results = append(results, CleanSplit(strings.Split(line, " "))...)
	}
	return results
}

func LoadFile(filename string) (core.Module, []string, error) {
	content, failure := ioutil.ReadFile(filename)
	if failure != nil {
		return nil, nil, failure
	}
	segments := strings.Split(string(content), "#")
	if len(segments) != 2 {
		return nil, nil, errors.New("Too little or too many segments.")
	}
	return ParseRules(segments[0]), ParseInitial(segments[1]), nil
}

func main() {
	file := flag.String("file", "", "a file to evaluate")
	debug := flag.Bool("debug", false, "enable debugging")
	length := flag.Int("queue-length", 65536, "set the queue length")
	flag.Parse()
	if *file == "" {
		fmt.Println("No files to evaluate, terminating.")
		return
	}
	rules, initial, failure := LoadFile(*file)
	if failure != nil {
		fmt.Println("Error:", failure)
		return
	}
	interpreter := core.CreateInterpreter(core.InterpreterOptions{
		Initial: initial,
		Modules: []core.Module{
			rules,
			&modules.SeekModule{},
			&modules.IOModule{},
			&modules.MathModule{},
		},
		Length: *length,
		Debug:  *debug,
	})
	start := time.Now()
	interpreter.Run()
	fmt.Println(time.Since(start))
	interpreter.Queue.Print()
}
