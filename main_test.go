package main

import (
	"feather/core"
	"feather/modules"
	"testing"
)

func BenchmarkParseAndLoad(test *testing.B) {
	rules, initial, _ := LoadFile("add_b36_1024.fth")
	interpreter := core.CreateInterpreter(core.InterpreterOptions{
		Initial: initial,
		Modules: []core.Module{rules, &modules.IOModule{}, &modules.SeekModule{}, &modules.MathModule{}},
		Length:  65536,
	})
	interpreter.Run()
}
