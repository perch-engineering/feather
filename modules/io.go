package modules

import (
	"bufio"
	"feather/core"
	"fmt"
	"os"
)

type IOModule struct{}

func (module *IOModule) Execute(interpreter *core.Interpreter) (bool, error) {
	if interpreter.Queue.Match("print") == true {
		character := rune(0)
		interpreter.Queue.Dequeue(1)
		token := interpreter.Queue.Peek()
		for token == "0" || token == "1" {
			character <<= 1
			if token == "1" {
				character |= 1
			}
			interpreter.Queue.Dequeue(1)
			token = interpreter.Queue.Peek()
		}
		fmt.Printf("%c", character)
		return interpreter.Queue.EnqueueHead("print:done"), nil
	} else if interpreter.Queue.Match("get") == true {
		reader := bufio.NewReader(os.Stdin)
		character, _, _ := reader.ReadRune()
		if character == 0 {
			return interpreter.Queue.Dequeue(1) && interpreter.Queue.EnqueueHead("0"), nil
		} else {
			interpreter.Queue.Dequeue(1)
			for character != 0 {
				if int(character)&1 == 1 {
					interpreter.Queue.EnqueueHead("1")
				} else {
					interpreter.Queue.EnqueueHead("0")
				}
				character >>= 1
			}
		}
		return interpreter.Queue.EnqueueHead("get:done"), nil
	}
	return false, nil
}
