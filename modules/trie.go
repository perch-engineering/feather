package modules

import (
	"feather/core"
)

type Trie struct {
	Nodes   map[string]*Trie
	Content []string
}

func CreateTrie(content []string) *Trie {
	return &Trie{
		Nodes:   make(map[string]*Trie),
		Content: content,
	}
}

func (trie *Trie) Insert(path, value []string) {
	for _, value := range path {
		if _, exists := trie.Nodes[value]; exists == false {
			trie.Nodes[value] = CreateTrie(nil)
		}
		trie = trie.Nodes[value]
	}
	trie.Content = value
}

func (trie *Trie) Search(queue *core.Queue) ([]string, int) {
	length := 0
	index := queue.Tail
	for index != queue.Head {
		next := trie.Nodes[queue.Elements[index]]
		if next == nil {
			break
		}
		trie = next
		length++
		index++
		if index == len(queue.Elements) {
			index = 0
		}
	}
	return trie.Content, length
}

func (trie *Trie) Execute(interpreter *core.Interpreter) (bool, error) {
	replacement, length := trie.Search(interpreter.Queue)
	if replacement == nil {
		return false, nil
	}
	interpreter.Queue.Dequeue(length)
	interpreter.Queue.EnqueueHead(replacement...)
	// Replace this. This works, but doesn't belong here.
	if len(replacement) >= length {
		for count := 0; count < length; count++ {
			if interpreter.Queue.Head == 0 {
				interpreter.Queue.Head = len(interpreter.Queue.Elements)
			}
			interpreter.Queue.Head--
			if interpreter.Queue.Tail == 0 {
				interpreter.Queue.Tail = len(interpreter.Queue.Elements)
			}
			interpreter.Queue.Tail--
			interpreter.Queue.Elements[interpreter.Queue.Tail] = interpreter.Queue.Elements[interpreter.Queue.Head]
		}
	}
	return true, nil
}
