package modules

import (
	"feather/core"
)

type SeekModule struct{}

func (module *SeekModule) Execute(interpreter *core.Interpreter) (bool, error) {
	if interpreter.Queue.Match("seek:right") == true {
		interpreter.Queue.Dequeue(1)
		pattern := []string{}
		payload := []string{}
		token := interpreter.Queue.Peek()
		for token != "seek:payload" {
			pattern = append(pattern, token)
			interpreter.Queue.Dequeue(1)
			token = interpreter.Queue.Peek()
		}
		interpreter.Queue.Dequeue(1)
		token = interpreter.Queue.Peek()
		for token != "seek:end" {
			payload = append(payload, token)
			interpreter.Queue.Dequeue(1)
			token = interpreter.Queue.Peek()
		}
		interpreter.Queue.Dequeue(1)
		steps := 0
		for steps != interpreter.Queue.Length {
			if interpreter.Queue.Match(pattern...) {
				interpreter.Queue.EnqueueHead(payload...)
				for count := 0; count < len(pattern); count++ {
					if interpreter.Queue.Head == 0 {
						interpreter.Queue.Head = len(interpreter.Queue.Elements)
					}
					interpreter.Queue.Head--
					if interpreter.Queue.Tail == 0 {
						interpreter.Queue.Tail = len(interpreter.Queue.Elements)
					}
					interpreter.Queue.Tail--
					interpreter.Queue.Elements[interpreter.Queue.Tail] = interpreter.Queue.Elements[interpreter.Queue.Head]
				}
				return true, nil
			}
			interpreter.Queue.Roll()
			steps++
		}
	} else if interpreter.Queue.Match("seek:left") == true {
		interpreter.Queue.Dequeue(1)
		pattern := []string{}
		payload := []string{}
		token := interpreter.Queue.Peek()
		for token != "seek:payload" {
			pattern = append(pattern, token)
			interpreter.Queue.Dequeue(1)
			token = interpreter.Queue.Peek()
		}
		interpreter.Queue.Dequeue(1)
		token = interpreter.Queue.Peek()
		for token != "seek:end" {
			payload = append(payload, token)
			interpreter.Queue.Dequeue(1)
			token = interpreter.Queue.Peek()
		}
		interpreter.Queue.Dequeue(1)
		steps := 0
		for steps != interpreter.Queue.Length {
			if interpreter.Queue.Match(pattern...) {
				interpreter.Queue.EnqueueHead(payload...)
				return true, nil
			}
			if interpreter.Queue.Head == 0 {
				interpreter.Queue.Head = len(interpreter.Queue.Elements)
			}
			interpreter.Queue.Head--
			if interpreter.Queue.Tail == 0 {
				interpreter.Queue.Tail = len(interpreter.Queue.Elements)
			}
			interpreter.Queue.Tail--
			interpreter.Queue.Elements[interpreter.Queue.Tail] = interpreter.Queue.Elements[interpreter.Queue.Head]
			steps++
		}
	}
	return false, nil
}
