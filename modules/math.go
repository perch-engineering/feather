package modules

import (
	"feather/core"
	"math/big"
	"strings"
)

type MathModule struct{}

func (module *MathModule) Execute(interpreter *core.Interpreter) (bool, error) {
	if interpreter.Queue.Match("add") == true || interpreter.Queue.Match("sub") {
		method := interpreter.Queue.Peek()
		interpreter.Queue.Dequeue(1)
		token := interpreter.Queue.Peek()
		first := ""
		second := ""
		for token == "0" || token == "1" {
			first += token
			interpreter.Queue.Dequeue(1)
			token = interpreter.Queue.Peek()
		}
		interpreter.Queue.Dequeue(1)
		token = interpreter.Queue.Peek()
		for token == "0" || token == "1" {
			second += token
			interpreter.Queue.Dequeue(1)
			token = interpreter.Queue.Peek()
		}
		result := new(big.Int)
		a := new(big.Int)
		b := new(big.Int)
		a.SetString(first, 2)
		b.SetString(second, 2)
		if method == "add" {
			result.Add(a, b)
		} else if method == "sub" {
			result.Sub(a, b)
		}
		interpreter.Queue.EnqueueHead(strings.Split(result.Text(2), "")...)
		// Replace this. It works, but it doesn't belong here, and is a copy.
		length := len(result.Text(2))
		for count := 0; count < length; count++ {
			if interpreter.Queue.Head == 0 {
				interpreter.Queue.Head = len(interpreter.Queue.Elements)
			}
			interpreter.Queue.Head--
			if interpreter.Queue.Tail == 0 {
				interpreter.Queue.Tail = len(interpreter.Queue.Elements)
			}
			interpreter.Queue.Tail--
			interpreter.Queue.Elements[interpreter.Queue.Tail] = interpreter.Queue.Elements[interpreter.Queue.Head]
		}
		return true, nil
	} else if interpreter.Queue.Match("add:16") == true || interpreter.Queue.Match("sub:16") {
		method := interpreter.Queue.Peek()
		interpreter.Queue.Dequeue(1)
		token := interpreter.Queue.Peek()
		first := ""
		second := ""
		for token == "0" || token == "1" ||
			token == "2" || token == "3" ||
			token == "4" || token == "5" ||
			token == "6" || token == "7" ||
			token == "8" || token == "9" ||
			token == "a" || token == "b" ||
			token == "c" || token == "d" ||
			token == "e" || token == "f" {
			first += token
			interpreter.Queue.Dequeue(1)
			token = interpreter.Queue.Peek()
		}
		interpreter.Queue.Dequeue(1)
		token = interpreter.Queue.Peek()
		for token == "0" || token == "1" ||
			token == "2" || token == "3" ||
			token == "4" || token == "5" ||
			token == "6" || token == "7" ||
			token == "8" || token == "9" ||
			token == "a" || token == "b" ||
			token == "c" || token == "d" ||
			token == "e" || token == "f" {
			second += token
			interpreter.Queue.Dequeue(1)
			token = interpreter.Queue.Peek()
		}
		result := new(big.Int)
		a := new(big.Int)
		b := new(big.Int)
		a.SetString(first, 16)
		b.SetString(second, 16)
		if method == "add:16" {
			result.Add(a, b)
		} else if method == "sub:16" {
			result.Sub(a, b)
		}
		interpreter.Queue.EnqueueHead(strings.Split(result.Text(16), "")...)
		// Replace this. It works, but it doesn't belong here, and is a copy.
		length := len(result.Text(16))
		for count := 0; count < length; count++ {
			if interpreter.Queue.Head == 0 {
				interpreter.Queue.Head = len(interpreter.Queue.Elements)
			}
			interpreter.Queue.Head--
			if interpreter.Queue.Tail == 0 {
				interpreter.Queue.Tail = len(interpreter.Queue.Elements)
			}
			interpreter.Queue.Tail--
			interpreter.Queue.Elements[interpreter.Queue.Tail] = interpreter.Queue.Elements[interpreter.Queue.Head]
		}

	}
	return false, nil
}
