package core

type Module interface {
	Execute(*Interpreter) (bool, error)
}
