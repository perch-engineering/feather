package core

import (
	"fmt"
)

type Queue struct {
	Elements []string
	Head     int
	Tail     int
	Length   int
}

func (queue *Queue) Match(pattern ...string) bool {
	if len(pattern) > queue.Length {
		return false
	}
	second := queue.Tail
	for first := 0; first < len(pattern); first++ {
		if pattern[first] != queue.Elements[second] {
			return false
		}
		second++
		if second == len(queue.Elements) {
			second = 0
		}
	}
	return true
}

func (queue *Queue) Peek() string {
	return queue.Elements[queue.Tail]
}

func (queue *Queue) Enqueue(elements ...string) bool {
	if len(elements)+queue.Length > len(queue.Elements) {
		return false
	}
	for _, value := range elements {
		queue.Elements[queue.Head] = value
		queue.Head++
		if queue.Head == len(queue.Elements) {
			queue.Head = 0
		}
	}
	queue.Length += len(elements)
	return true
}

func (queue *Queue) EnqueueHead(elements ...string) bool {
	if len(elements)+queue.Length > len(queue.Elements) {
		return false
	}
	for index := len(elements) - 1; index >= 0; index-- {
		if queue.Tail == 0 {
			queue.Tail = len(queue.Elements)
		}
		queue.Tail--
		queue.Elements[queue.Tail] = elements[index]
	}
	queue.Length += len(elements)
	return true
}

func (queue *Queue) Dequeue(length int) bool {
	if length > len(queue.Elements) {
		return false
	}
	queue.Length -= length
	for length != 0 {
		queue.Tail++
		if queue.Tail == len(queue.Elements) {
			queue.Tail = 0
		}
		length--
	}
	return true
}

func (queue *Queue) Roll() bool {
	element := queue.Elements[queue.Tail]
	return queue.Dequeue(1) && queue.Enqueue(element)
}

func (queue *Queue) Print() {
	index := queue.Tail
	for index != queue.Head {
		fmt.Printf("%s ", queue.Elements[index])
		index++
		if index == len(queue.Elements) {
			index = 0
		}
	}
	fmt.Print("\n")
}
