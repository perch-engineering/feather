package core

type Interpreter struct {
	Modules []Module
	Queue   *Queue
	Debug   bool
}

type InterpreterOptions struct {
	Initial []string
	Modules []Module
	Length  int
	Debug   bool
}

func CreateInterpreter(options InterpreterOptions) *Interpreter {
	if options.Length == 0 {
		options.Length = len(options.Initial)
	}
	initial := append(options.Initial, make([]string, options.Length-len(options.Initial))...)
	return &Interpreter{
		Queue: &Queue{
			Head:     len(options.Initial),
			Tail:     0,
			Elements: initial,
			Length:   len(options.Initial),
		},
		Modules: options.Modules,
		Debug:   options.Debug,
	}
}

func (interpreter *Interpreter) Step() (bool, error) {
	for _, module := range interpreter.Modules {
		match, failure := module.Execute(interpreter)
		if failure != nil {
			return false, failure
		}
		if match == true {
			return true, nil
		}
	}
	return false, nil
}

func (interpreter *Interpreter) Run() error {
	steps := 0
	for steps != interpreter.Queue.Length {
		if interpreter.Debug == true {
			interpreter.Queue.Print()
		}
		match, failure := interpreter.Step()
		if failure != nil {
			return failure
		}
		if match == true {
			steps = 0
		} else {
			interpreter.Queue.Roll()
			steps++
		}
	}
	return nil
}
